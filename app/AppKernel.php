<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            new Acme\UserBundle\AcmeUserBundle(),
            new Acme\TaskBundle\AcmeTaskBundle(),
            new Acme\AccountBundle\AcmeAccountBundle(),
            new Acme\CatalogBundle\AcmeCatalogBundle(),
            
            new Acme\GoodsBundle\AcmeGoodsBundle(),
            new Acme\ServicesBundle\AcmeServicesBundle(),
            new Acme\TechnologyBundle\AcmeTechnologyBundle(),
            new Acme\InvestmentBundle\AcmeInvestmentBundle(),
            new Acme\DiscussionBundle\AcmeDiscussionBundle(),

            new Acme\InfoBundle\AcmeInfoBundle(),
            new Acme\FileBundle\AcmeFileBundle(),
            new Acme\ProductBundle\AcmeProductBundle(),
            new Acme\FormBundle\AcmeFormBundle(),
            new Acme\TicketsBundle\AcmeTicketsBundle(),
            new Acme\OrganizationBundle\AcmeOrganizationBundle(),
            new Acme\NewsBundle\AcmeNewsBundle(),
            
//            new Sylius\Bundle\ResourceBundle\SyliusResourceBundle(),
//            new Sylius\Bundle\MoneyBundle\SyliusMoneyBundle(),
//            new Sylius\Bundle\OrderBundle\SyliusOrderBundle(),
//            new Sylius\Bundle\CartBundle\SyliusCartBundle(),
//            new Sylius\Bundle\TranslationBundle\SyliusTranslationBundle(),

            new App\AppBundle\AppAppBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'))) {
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
