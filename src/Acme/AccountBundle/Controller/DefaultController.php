<?php

namespace Acme\AccountBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($id)
    { 
            return $this->render('AcmeAccountBundle:Default:index.html.twig', array('id' => $id));
    }
}
