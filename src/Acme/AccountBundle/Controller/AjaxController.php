<?php

namespace Acme\AccountBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AjaxController extends Controller
{
    public function indexAction()
    {
        return $this->render('AcmeAccountBundle:Default:ajax.html.twig');
    }
}
