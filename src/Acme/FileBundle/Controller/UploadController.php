<?php

namespace Acme\FileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\FileBundle\Entity\Document;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;



class UploadController extends Controller
{
    public function uploadAction(Request $request)
    {
        $document = new Document();
        $form = $this->createFormBuilder($document)
                	->add('name')
                        ->add('file')
                        ->getForm();

        if ($request->getMethod() == 'POST')
        {
            $form->submit($request);
            if ($form->isValid()) {
        	$em = $this->getDoctrine()->getManager();
        	$document->upload();
        	$em->persist($document);
        	$em->flush();
        	return $this->redirect($this->generateUrl('acme_file_homepage'));
            }
        }
        
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT files FROM AcmeFileBundle:Document files ORDER BY files.id');
        $files = $query->getArrayResult();

        return $this->render('AcmeFileBundle:Default:upload.html.twig',array('form' => $form->createView(), 'files' => $files));
    }
}
