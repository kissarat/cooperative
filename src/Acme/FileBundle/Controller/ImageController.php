<?php

namespace Acme\FileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;

use Acme\FileBundle\Entity\Document;

class ImageController extends Controller
{
    public function indexAction($id)
    {
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery('SELECT files FROM AcmeFileBundle:Document files WHERE files.id = :id'
	                    	    )->setParameters(array(
	                            	    'id' => $id,
	                                    ));
        $files = $query->getArrayResult();
        $file = $files[0]['path'];

	if ($file != ''){
	    $guesser = MimeTypeGuesser::getInstance();
	    $mimetype =  $guesser->guess("uploads/documents/$file");

    	    $headers = array('Content-Type' => '$mimetype', 'Content-Disposition' => 'inline; filename="$file"'); // mimetype = 'image/jpg'
    	    return new Response(file_get_contents("uploads/documents/$file"), 200, $headers);
        }
    }
}
