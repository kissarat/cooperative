<?php

namespace Acme\FileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;

use Acme\FileBundle\Entity\Document;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DocumentViewController extends Controller
{
    public function indexAction()
    {
	$guesser = MimeTypeGuesser::getInstance();
	$mimetype = $guesser->guess('uploads/documents/WDSF ID-Card for Serhii Maslovskyi.pdf');

//        $headers = array('Content-Type' => 'application/pdf', 'Content-Disposition' => 'inline; filename="WDSF ID-Card for Serhii Maslovskyi.pdf"');
        $headers = array('Content-Type' => '$mimetype', 'Content-Disposition' => 'inline; filename="WDSF ID-Card for Serhii Maslovskyi.pdf"');
        return new Response(file_get_contents('uploads/documents/WDSF ID-Card for Serhii Maslovskyi.pdf'), 200, $headers); 
    }
}
