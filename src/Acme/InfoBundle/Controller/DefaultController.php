<?php

namespace Acme\InfoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Finder;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
	$data = file_get_contents("info/$name.html");
	preg_match_all("/<title>(.*)<\/title>(.*)/ims",$data,$ARR);
	$name = $ARR[1][0];
	$data = $ARR[2][0];
        return $this->render('AcmeInfoBundle:Default:index.html.twig', array('name' => $name, 'data' => $data));
    }
}
