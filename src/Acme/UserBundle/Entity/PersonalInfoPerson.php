<?php
// src/Acme/UserBundle/Entity/PersonalInfoPerson.php
namespace Acme\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="personalinfo.person")
*/

class PersonalInfoPerson
{
    /**
    * @ORM\Id
    * @ORM\Column(name="person_id", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $person_id;

    /**
    * @ORM\Column(name="user_id", type="integer")
    */    
    protected $user_id;
    
    /**
    * @ORM\Column(name="lastname", type="string", length=64)
    */    
    protected $lastname;

    /**
    * @ORM\Column(name="firstname", type="string", length=32)
    */    
    protected $firstname;
    
    /**
    * @ORM\Column(name="middlename", type="string", length=32)
    */
    protected $middlename;

    /**
    * @ORM\Column(name="prefix", type="string", length=8)
    */
    protected $prefix;

    /**
    * @ORM\Column(name="title", type="string", length=8)
    */
    protected $title;

    /**
    * @ORM\Column(name="suffix", type="string", length=8)
    */
    protected $suffix;

    /**
    * @ORM\Column(name="preferredname", type="string", length=32)
    */
    protected $preferredname;

    /**
    * @ORM\Column(name="sex", type="integer", nullable=true)
    */
    protected $sex;

    /**
    * @ORM\Column(name="birthday", type="date", nullable=false)
    */
    protected $birthday;



    /**
     * Get person_id
     *
     * @return integer 
     */
    public function getPersonId()
    {
        return $this->person_id;
    }

    /**
     * Set user_id
     *
     * @param integer $userId
     * @return PersonalInfoPerson
     */
    public function setUserId($userId)
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * Set lastname
     *
     * @param \string $lastname
     * @return PersonalInfoPerson
     */
    public function setLastname(\string $lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return \string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param \string $firstname
     * @return PersonalInfoPerson
     */
    public function setFirstname(\string $firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return \string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set middlename
     *
     * @param \string $middlename
     * @return PersonalInfoPerson
     */
    public function setMiddlename(\string $middlename)
    {
        $this->middlename = $middlename;

        return $this;
    }

    /**
     * Get middlename
     *
     * @return \string 
     */
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     * Set prefix
     *
     * @param \string $prefix
     * @return PersonalInfoPerson
     */
    public function setPrefix(\string $prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    /**
     * Get prefix
     *
     * @return \string 
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * Set title
     *
     * @param \string $title
     * @return PersonalInfoPerson
     */
    public function setTitle(\string $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return \string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set suffix
     *
     * @param \string $suffix
     * @return PersonalInfoPerson
     */
    public function setSuffix(\string $suffix)
    {
        $this->suffix = $suffix;

        return $this;
    }

    /**
     * Get suffix
     *
     * @return \string 
     */
    public function getSuffix()
    {
        return $this->suffix;
    }

    /**
     * Set preferredname
     *
     * @param \string $preferredname
     * @return PersonalInfoPerson
     */
    public function setPreferredname(\string $preferredname)
    {
        $this->preferredname = $preferredname;

        return $this;
    }

    /**
     * Get preferredname
     *
     * @return \string 
     */
    public function getPreferredname()
    {
        return $this->preferredname;
    }

    /**
     * Set sex
     *
     * @param integer $sex
     * @return PersonalInfoPerson
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return integer 
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return PersonalInfoPerson
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }
}
