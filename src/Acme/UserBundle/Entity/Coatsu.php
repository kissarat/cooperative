<?php
// src/Acme/UserBundle/Entity/Country.php
namespace Acme\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="common.coatsu")
*/

class Coatsu
{
    /**
    * @ORM\Id
    * @ORM\Column(name="administrative_unit_id", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $administrative_unit_id;

    /**
    * @ORM\Column(name="country_id", type="string", length=3)
    */    
    protected $country_id;
    
    /**
    * @ORM\Column(name="parent_id", type="string", length=2)
    */    
    protected $parent_id;

    /**
    * @ORM\Column(name="code", type="string", length=3)
    */    
    protected $code;
    
    /**
    * @ORM\Column(name="code_level", type="string", length=128)
    */
    protected $code_level;

    /**
    * @ORM\Column(name="code_l2sign", type="string")
    */
    protected $code_l2sign;

    /**
    * @ORM\Column(name="code_l3sign", type="string")
    */
    protected $code_l3sign;

    /**
    * @ORM\Column(name="unit_type", type="boolean")
    */
    protected $unit_type;

    /**
    * @ORM\Column(name="unit_name", type="text")
    */
    protected $unit_name;

    /**
    * @ORM\Column(name="modified", type="text")
    */
    protected $modified;

    /**
    * @ORM\Column(name="comment", type="text")
    */
    protected $comment;


    /**
     * Get administrative_unit_id
     *
     * @return integer 
     */
    public function getAdministrativeUnitId()
    {
        return $this->administrative_unit_id;
    }

    /**
     * Set country_id
     *
     * @param string $countryId
     * @return Coatsu
     */
    public function setCountryId($countryId)
    {
        $this->country_id = $countryId;

        return $this;
    }

    /**
     * Get country_id
     *
     * @return string 
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * Set parent_id
     *
     * @param string $parentId
     * @return Coatsu
     */
    public function setParentId($parentId)
    {
        $this->parent_id = $parentId;

        return $this;
    }

    /**
     * Get parent_id
     *
     * @return string 
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Coatsu
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set code_level
     *
     * @param string $codeLevel
     * @return Coatsu
     */
    public function setCodeLevel($codeLevel)
    {
        $this->code_level = $codeLevel;

        return $this;
    }

    /**
     * Get code_level
     *
     * @return string 
     */
    public function getCodeLevel()
    {
        return $this->code_level;
    }

    /**
     * Set code_l2sign
     *
     * @param string $codeL2sign
     * @return Coatsu
     */
    public function setCodeL2sign($codeL2sign)
    {
        $this->code_l2sign = $codeL2sign;

        return $this;
    }

    /**
     * Get code_l2sign
     *
     * @return string 
     */
    public function getCodeL2sign()
    {
        return $this->code_l2sign;
    }

    /**
     * Set code_l3sign
     *
     * @param \DateTime $codeL3sign
     * @return Coatsu
     */
    public function setCodeL3sign($codeL3sign)
    {
        $this->code_l3sign = $codeL3sign;

        return $this;
    }

    /**
     * Get code_l3sign
     *
     * @return \DateTime 
     */
    public function getCodeL3sign()
    {
        return $this->code_l3sign;
    }

    /**
     * Set unit_type
     *
     * @param boolean $unitType
     * @return Coatsu
     */
    public function setUnitType($unitType)
    {
        $this->unit_type = $unitType;

        return $this;
    }

    /**
     * Get unit_type
     *
     * @return boolean 
     */
    public function getUnitType()
    {
        return $this->unit_type;
    }

    /**
     * Set unit_name
     *
     * @param string $unitName
     * @return Coatsu
     */
    public function setUnitName($unitName)
    {
        $this->unit_name = $unitName;

        return $this;
    }

    /**
     * Get unit_name
     *
     * @return string 
     */
    public function getUnitName()
    {
        return $this->unit_name;
    }

    /**
     * Set modified
     *
     * @param string $modified
     * @return Coatsu
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return string 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Coatsu
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}
