<?php

namespace Acme\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="personalinfo_passport")
*/

class PersonalInfoPassport
{
    /**
     * @ORM\Id
     * @ORM\Column(name="passport_id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $passport_id;

    /**
     * @ORM\Column(name="person_id", type="integer")
     */
    private $person_id;

    /**
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @ORM\Column(name="passport", type="string")
     */
    private $passport;

    /**
     * @ORM\Column(name="begin_date", type="datetime")
     */
    private $begin_date;

    /**
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $end_date;

    /**
     * @ORM\Column(name="issuer", type="string")
     */
    private $issuer;

    /**
     * @ORM\Column(name="creation", type="datetime")
     */
    private $creation;


    /**
     * Get passport_id
     *
     * @return integer 
     */
    public function getPassportId()
    {
        return $this->passport_id;
    }

    /**
     * Set person_id
     *
     * @param integer $personId
     * @return PersonalInfoPassport
     */
    public function setPersonId($personId)
    {
        $this->person_id = $personId;

        return $this;
    }

    /**
     * Get person_id
     *
     * @return integer 
     */
    public function getPersonId()
    {
        return $this->person_id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return PersonalInfoPassport
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set passport
     *
     * @param string $passport
     * @return PersonalInfoPassport
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;

        return $this;
    }

    /**
     * Get passport
     *
     * @return string 
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * Set begin_date
     *
     * @param \DateTime $beginDate
     * @return PersonalInfoPassport
     */
    public function setBeginDate($beginDate)
    {
        $this->begin_date = $beginDate;

        return $this;
    }

    /**
     * Get begin_date
     *
     * @return \DateTime 
     */
    public function getBeginDate()
    {
        return $this->begin_date;
    }

    /**
     * Set end_date
     *
     * @param \DateTime $endDate
     * @return PersonalInfoPassport
     */
    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;

        return $this;
    }

    /**
     * Get end_date
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * Set issuer
     *
     * @param integer $issuer
     * @return PersonalInfoPassport
     */
    public function setIssuer($issuer)
    {
        $this->issuer = $issuer;

        return $this;
    }

    /**
     * Get issuer
     *
     * @return integer 
     */
    public function getIssuer()
    {
        return $this->issuer;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return PersonalInfoPassport
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }
}
