<?php
namespace Acme\UserBundle\Entity;

class General
{
    protected $id;
    protected $lastname;
    protected $firstname;
    protected $middlename;
    protected $prefix;
    protected $title;
    protected $suffix;
    protected $preferredname;
    protected $sex;
    protected $birthday;

    public function getId()
        {
        return $this->id;
        }
    public function setId($id)
        {
        $this->id = $id;
        }

    public function getLastname()
        {
        return $this->lastname;
        }
    public function setLastname($lastname)
        {
        $this->lastname = $lastname;
        }

    public function getFirstname()
        {
        return $this->firstname;
        }
    public function setFirstname($firstname)
        {
        $this->firstname = $firstname;
        }

    public function getMiddlename()
        {
        return $this->middlename;
        }
    public function setMiddlename($middlename)
        {
        $this->middlename = $middlename;
        }
    
    public function getPrefix()
        {
        return $this->prefix;
        }
    public function setPrefix($prefix)
        {
        $this->prefix = $prefix;
        }

    public function getTitle()
        {
        return $this->title;
        }
    public function setTitle($title)
        {
        $this->title = $title;
        }

    public function getSuffix()
        {
        return $this->suffix;
        }
    public function setSuffix($suffix)
        {
        $this->suffix = $suffix;
        }

    public function getPreferredname()
        {
        return $this->preferredname;
        }
    public function setPreferredname($preferredname)
        {
        $this->preferredname = $preferredname;
        }

    public function getSex()
        {
        return $this->sex;
        }
    public function setSex($sex)
        {
        $this->sex = $sex;
        }

    public function getBirthday()
        {
        return $this->birthday;
        }
    public function setBirthday($birthday)
        {
        $this->birthday = $birthday;
        }

}