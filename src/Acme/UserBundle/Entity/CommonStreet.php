<?php
// src/Acme/UserBundle/Entity/CommonStreet.php
namespace Acme\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="common.street")
*/

class CommonStreet
{
    /**
    * @ORM\Id
    * @ORM\Column(name="street_id", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $street_id;

    /**
    * @ORM\Column(name="country_id", type="integer")
    */    
    protected $country_id;
    
    /**
    * @ORM\Column(name="administrative_unit_id", type="integer")
    */    
    protected $administrative_unit_id;

    /**
    * @ORM\Column(name="kind_type", type="integer")
    */    
    protected $kind_type;
    
    /**
    * @ORM\Column(name="kind_name", type="string", length=32)
    */
    protected $kind_name;

    /**
    * @ORM\Column(name="title", type="string", length=128)
    */
    protected $title;


    /**
     * Get street_id
     *
     * @return integer 
     */
    public function getStreetId()
    {
        return $this->street_id;
    }

    /**
     * Set country_id
     *
     * @param integer $countryId
     * @return CommonStreet
     */
    public function setCountryId($countryId)
    {
        $this->country_id = $countryId;

        return $this;
    }

    /**
     * Get country_id
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * Set administrative_unit_id
     *
     * @param integer $administrativeUnitId
     * @return CommonStreet
     */
    public function setAdministrativeUnitId($administrativeUnitId)
    {
        $this->administrative_unit_id = $administrativeUnitId;

        return $this;
    }

    /**
     * Get administrative_unit_id
     *
     * @return integer 
     */
    public function getAdministrativeUnitId()
    {
        return $this->administrative_unit_id;
    }

    /**
     * Set kind_type
     *
     * @param integer $kindType
     * @return CommonStreet
     */
    public function setKindType($kindType)
    {
        $this->kind_type = $kindType;

        return $this;
    }

    /**
     * Get kind_type
     *
     * @return integer 
     */
    public function getKindType()
    {
        return $this->kind_type;
    }

    /**
     * Set kind_name
     *
     * @param \varchar $kindName
     * @return CommonStreet
     */
    public function setKindName(\varchar $kindName)
    {
        $this->kind_name = $kindName;

        return $this;
    }

    /**
     * Get kind_name
     *
     * @return \varchar 
     */
    public function getKindName()
    {
        return $this->kind_name;
    }

    /**
     * Set title
     *
     * @param \varchar $title
     * @return CommonStreet
     */
    public function setTitle(\varchar $title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return \varchar 
     */
    public function getTitle()
    {
        return $this->title;
    }
}
