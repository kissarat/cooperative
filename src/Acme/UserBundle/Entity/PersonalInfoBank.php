<?php

namespace Acme\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="personalinfo_bank")
*/

class PersonalInfoBank
{
    /**
     * @ORM\Id
     * @ORM\Column(name="bank_id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $bank_id;

    /**
     * @ORM\Column(name="person_id", type="integer")
     */
    private $person_id;

    /**
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @ORM\Column(name="mfo", type="string")
     */
    private $mfo;

    /**
     * @ORM\Column(name="code", type="string")
     */
    private $code;

    /**
     * @ORM\Column(name="rr", type="string")
     */
    private $rr;

    /**
     * @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
    * @ORM\Column(name="creation", type="datetime")
    */
    protected $creation;

    /**
     * Get bank_id
     *
     * @return integer 
     */
    public function getBankId()
    {
        return $this->bank_id;
    }

    /**
     * Set person_id
     *
     * @param integer $personId
     * @return PersonalInfoBank
     */
    public function setPersonId($personId)
    {
        $this->person_id = $personId;

        return $this;
    }

    /**
     * Get person_id
     *
     * @return integer 
     */
    public function getPersonId()
    {
        return $this->person_id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return PersonalInfoBank
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set mfo
     *
     * @param string $mfo
     * @return PersonalInfoBank
     */
    public function setMfo($mfo)
    {
        $this->mfo = $mfo;

        return $this;
    }

    /**
     * Get mfo
     *
     * @return string 
     */
    public function getMfo()
    {
        return $this->mfo;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return PersonalInfoBank
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set rr
     *
     * @param string $rr
     * @return PersonalInfoBank
     */
    public function setRr($rr)
    {
        $this->rr = $rr;

        return $this;
    }

    /**
     * Get rr
     *
     * @return string 
     */
    public function getRr()
    {
        return $this->rr;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PersonalInfoBank
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return PersonalInfoBank
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }
}
