<?php
// src/Acme/UserBundle/Entity/CommonBuilding.php
namespace Acme\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="common.building")
*/

class CommonBuilding
{
    /**
    * @ORM\Id
    * @ORM\Column(name="building_id", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $building_id;

    /**
    * @ORM\Column(name="country_id", type="integer")
    */    
    protected $country_id;
    
    /**
    * @ORM\Column(name="street_id", type="integer")
    */    
    protected $street_id;

    /**
    * @ORM\Column(name="building_no", type="string", length=32)
    */    
    protected $building_no;
    
    /**
    * @ORM\Column(name="postal_code", type="string", length=16)
    */
    protected $postal_code;

    /**
    * @ORM\Column(name="latitude", type="decimal", precision=24, scale=16, nullable=true)
    */
    protected $latitude;

 /**
    * @ORM\Column(name="longitude", type="decimal", precision=24, scale=16, nullable=true)
    */
    protected $longitude;

    /**
     * Get building_id
     *
     * @return integer 
     */
    public function getBuildingId()
    {
        return $this->building_id;
    }

    /**
     * Set country_id
     *
     * @param integer $countryId
     * @return CommonBuilding
     */
    public function setCountryId($countryId)
    {
        $this->country_id = $countryId;

        return $this;
    }

    /**
     * Get country_id
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * Set street_id
     *
     * @param integer $streetId
     * @return CommonBuilding
     */
    public function setStreetId($streetId)
    {
        $this->street_id = $streetId;

        return $this;
    }

    /**
     * Get street_id
     *
     * @return integer 
     */
    public function getStreetId()
    {
        return $this->street_id;
    }

    /**
     * Set building_no
     *
     * @param \varchar $buildingNo
     * @return CommonBuilding
     */
    public function setBuildingNo(\varchar $buildingNo)
    {
        $this->building_no = $buildingNo;

        return $this;
    }

    /**
     * Get building_no
     *
     * @return \varchar 
     */
    public function getBuildingNo()
    {
        return $this->building_no;
    }

    /**
     * Set postal_code
     *
     * @param \varchar $postalCode
     * @return CommonBuilding
     */
    public function setPostalCode(\varchar $postalCode)
    {
        $this->postal_code = $postalCode;

        return $this;
    }

    /**
     * Get postal_code
     *
     * @return \varchar 
     */
    public function getPostalCode()
    {
        return $this->postal_code;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return CommonBuilding
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return CommonBuilding
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
}
