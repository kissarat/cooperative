<?php
namespace Acme\UserBundle\Entity;

class PersonCountry
{
    protected $countryid;
    protected $code;
    protected $codealpha2;
    protected $codealpha3;
    protected $title;
    protected $officialname;
    protected $modified;
    protected $canmodify;
    protected $comment;

    public function getId()
        {
        return $this->id;
        }
    public function setId($id)
        {
        $this->id = $id;
        }

    public function getCountryid()
        {
        return $this->countryid;
        }
    public function setCountryid($countryid)
        {
        $this->countryid = $countryid;
        }

    public function getCode()
        {
        return $this->code;
        }
    public function setCode($code)
        {
        $this->code = $code;
        }

    public function getCodealpha2()
        {
        return $this->codealpha2;
        }
    public function setCodealpha2($codealpha2)
        {
        $this->codealpha2 = $codealpha2;
        }

    public function getCodealpha3()
        {
        return $this->codealpha3;
        }
    public function setCodealpha3($codealpha3)
        {
        $this->codealpha3 = $codealpha3;
        }
    
    public function getTitle()
        {
        return $this->title;
        }
    public function setTitle($title)
        {
        $this->title = $title;
        }

    public function getOfficialname()
        {
        return $this->officialname;
        }
    public function setOfficialname($officialname)
        {
        $this->officialname = $officialname;
        }

    public function getModified()
        {
        return $this->modified;
        }
    public function setModified($modified)
        {
        $this->modified = $modified;
        }

    public function getCanmodify()
        {
        return $this->canmodify;
        }
    public function setCanmodify($canmodify)
        {
        $this->canmodify = $canmodify;
        }

    public function getComment()
        {
        return $this->comment;
        }
    public function setComment($comment)
        {
        $this->comment = $comment;
        }

}