<?php
// src/Acme/UserBundle/Entity/PersonalInfoContact.php
namespace Acme\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="personalinfo.contact")
*/

class PersonalInfoContact
{
    /**
    * @ORM\Id
    * @ORM\Column(name="contact_id", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $contact_id;

    /**
    * @ORM\Column(name="person_id", type="integer")
    */    
    protected $person_id;
    
    /**
    * @ORM\Column(name="kind", type="integer")
    */    
    protected $kind;

    /**
    * @ORM\Column(name="data", type="string", length=256)
    */    
    protected $data;
    
    /**
    * @ORM\Column(name="tag", type="integer")
    */
    protected $tag;


    /**
     * Get contact_id
     *
     * @return integer 
     */
    public function getContactId()
    {
        return $this->contact_id;
    }

    /**
     * Set person_id
     *
     * @param integer $personId
     * @return PersonalInfoContact
     */
    public function setPersonId($personId)
    {
        $this->person_id = $personId;

        return $this;
    }

    /**
     * Get person_id
     *
     * @return integer 
     */
    public function getPersonId()
    {
        return $this->person_id;
    }

    /**
     * Set kind
     *
     * @param integer $kind
     * @return PersonalInfoContact
     */
    public function setKind($kind)
    {
        $this->kind = $kind;

        return $this;
    }

    /**
     * Get kind
     *
     * @return integer 
     */
    public function getKind()
    {
        return $this->kind;
    }

    /**
     * Set data
     *
     * @param \varchar $data
     * @return PersonalInfoContact
     */
    public function setData(\varchar $data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return \varchar 
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set tag
     *
     * @param integer $tag
     * @return PersonalInfoContact
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return integer 
     */
    public function getTag()
    {
        return $this->tag;
    }
}
