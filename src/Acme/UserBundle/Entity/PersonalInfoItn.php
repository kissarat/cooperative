<?php

namespace Acme\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="personalinfo_itn")
*/

class PersonalInfoItn
{
    /**
     * @ORM\Id
     * @ORM\Column(name="itn_id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $itn_id;

    /**
     * @ORM\Column(name="person_id", type="integer")
     */
    private $person_id;

    /**
     * @ORM\Column(name="itn", type="integer")
     */
    private $itn;

    /**
     * @ORM\Column(name="creation", type="datetime")
     */
    private $creation;


    /**
     * Get itn_id
     *
     * @return integer 
     */
    public function getItnId()
    {
        return $this->itn_id;
    }

    /**
     * Set person_id
     *
     * @param integer $personId
     * @return PersonalInfoItn
     */
    public function setPersonId($personId)
    {
        $this->person_id = $personId;

        return $this;
    }

    /**
     * Get person_id
     *
     * @return integer 
     */
    public function getPersonId()
    {
        return $this->person_id;
    }

    /**
     * Set itn
     *
     * @param integer $itn
     * @return PersonalInfoItn
     */
    public function setItn($itn)
    {
        $this->itn = $itn;

        return $this;
    }

    /**
     * Get itn
     *
     * @return integer 
     */
    public function getItn()
    {
        return $this->itn;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return PersonalInfoItn
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }
}
