<?php

namespace Acme\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="personalinfo.address")
*/

class PersonalInfoAddress
{
    /**
     * @ORM\Id
     * @ORM\Column(name="address_id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $address_id;

    /**
     * @ORM\Column(name="person_id", type="integer")
     */
    private $person_id;

    /**
     * @ORM\Column(name="address_type", type="integer")
     */
    private $address_type;

    /**
     * @ORM\Column(name="country_id", type="integer")
     */
    private $country_id;

    /**
     * @ORM\Column(name="administrative_unit_id", type="integer")
     */
    private $administrative_unit_id;

    /**
     * @ORM\Column(name="street_id", type="integer")
     */
    private $street_id;

    /**
     * @ORM\Column(name="building_id", type="integer")
     */
    private $building_id;

    /**
     * @ORM\Column(name="address", type="string")
     */
    private $address;


    /**
     * Get address_id
     *
     * @return integer 
     */
    public function getAddressId()
    {
        return $this->address_id;
    }

    /**
     * Set person_id
     *
     * @param integer $personId
     * @return PersonalInfoAddress
     */
    public function setPersonId($personId)
    {
        $this->person_id = $personId;

        return $this;
    }

    /**
     * Get person_id
     *
     * @return integer 
     */
    public function getPersonId()
    {
        return $this->person_id;
    }

    /**
     * Set address_type
     *
     * @param integer $addressType
     * @return PersonalInfoAddress
     */
    public function setAddressType($addressType)
    {
        $this->address_type = $addressType;

        return $this;
    }

    /**
     * Get address_type
     *
     * @return integer 
     */
    public function getAddressType()
    {
        return $this->address_type;
    }

    /**
     * Set country_id
     *
     * @param integer $countryId
     * @return PersonalInfoAddress
     */
    public function setCountryId($countryId)
    {
        $this->country_id = $countryId;

        return $this;
    }

    /**
     * Get country_id
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * Set administrative_unit_id
     *
     * @param integer $administrativeUnitId
     * @return PersonalInfoAddress
     */
    public function setAdministrativeUnitId($administrativeUnitId)
    {
        $this->administrative_unit_id = $administrativeUnitId;

        return $this;
    }

    /**
     * Get administrative_unit_id
     *
     * @return integer 
     */
    public function getAdministrativeUnitId()
    {
        return $this->administrative_unit_id;
    }

    /**
     * Set street_id
     *
     * @param integer $streetId
     * @return PersonalInfoAddress
     */
    public function setStreetId($streetId)
    {
        $this->street_id = $streetId;

        return $this;
    }

    /**
     * Get street_id
     *
     * @return integer 
     */
    public function getStreetId()
    {
        return $this->street_id;
    }

    /**
     * Set building_id
     *
     * @param integer $buildingId
     * @return PersonalInfoAddress
     */
    public function setBuildingId($buildingId)
    {
        $this->building_id = $buildingId;

        return $this;
    }

    /**
     * Get building_id
     *
     * @return integer 
     */
    public function getBuildingId()
    {
        return $this->building_id;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return PersonalInfoAddress
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }
}
