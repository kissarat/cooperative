<?php
// src/Acme/UserBundle/Entity/Country.php
namespace Acme\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="common.country")
*/

class Country
{
    /**
    * @ORM\Id
    * @ORM\Column(name="country_id", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $country_id;

    /**
    * @ORM\Column(name="code", type="string", length=3)
    */    
    protected $code;
    
    /**
    * @ORM\Column(name="code_alpha2", type="string", length=2)
    */    
    protected $code_alpha2;

    /**
    * @ORM\Column(name="code_alpha3", type="string", length=3)
    */    
    protected $code_alpha3;
    
    /**
    * @ORM\Column(name="title", type="string", length=128)
    */
    protected $title;

    /**
    * @ORM\Column(name="official_name", type="string")
    */
    protected $official_name;

    /**
    * @ORM\Column(name="modified", type="date")
    */
    protected $modified;

    /**
    * @ORM\Column(name="can_modify", type="boolean")
    */
    protected $can_modify;

    /**
    * @ORM\Column(name="comment", type="text")
    */
    protected $comment;


    /**
     * Get country_id
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Country
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set code_alpha2
     *
     * @param string $codeAlpha2
     * @return Country
     */
    public function setCodeAlpha2($codeAlpha2)
    {
        $this->code_alpha2 = $codeAlpha2;

        return $this;
    }

    /**
     * Get code_alpha2
     *
     * @return string 
     */
    public function getCodeAlpha2()
    {
        return $this->code_alpha2;
    }

    /**
     * Set code_alpha3
     *
     * @param string $codeAlpha3
     * @return Country
     */
    public function setCodeAlpha3($codeAlpha3)
    {
        $this->code_alpha3 = $codeAlpha3;

        return $this;
    }

    /**
     * Get code_alpha3
     *
     * @return string 
     */
    public function getCodeAlpha3()
    {
        return $this->code_alpha3;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Country
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set official_name
     *
     * @param string $officialName
     * @return Country
     */
    public function setOfficialName($officialName)
    {
        $this->official_name = $officialName;

        return $this;
    }

    /**
     * Get official_name
     *
     * @return string 
     */
    public function getOfficialName()
    {
        return $this->official_name;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return Country
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set can_modify
     *
     * @param boolean $canModify
     * @return Country
     */
    public function setCanModify($canModify)
    {
        $this->can_modify = $canModify;

        return $this;
    }

    /**
     * Get can_modify
     *
     * @return boolean 
     */
    public function getCanModify()
    {
        return $this->can_modify;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Country
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}
