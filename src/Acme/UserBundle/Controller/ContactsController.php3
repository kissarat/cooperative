<?php
namespace Acme\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\UserBundle\Entity\Person;
use Acme\UserBundle\Entity\PersonalInfoAddress;
use Acme\UserBundle\Entity\Country;
use Acme\UserBundle\Entity\Coatsu;

use Acme\UserBundle\Entity\PersonAddress;

class ContactsController extends Controller
{
    public function viewAction(Request $request, $id)
    {

	$em = $this->getDoctrine()->getManager();

	// get person Id
	$query = $em->createQuery('SELECT person FROM AcmeUserBundle:Person person WHERE person.user_id = :user_id'
					)->setParameters(array(
	                    			'user_id' => $id,
	                                        ));
	$person = $query->getArrayResult();
	$person_id = $person[0]['person_id'];

	// get Count address for person
	$query = $em->createQuery('SELECT address FROM AcmeUserBundle:PersonalInfoAddress address WHERE address.person_id = :person_id'
					)->setParameters(array(
	                    			'person_id' => $person_id,
	                                        ));
	$address = $query->getArrayResult();
	$cnt = count($address);
	$cnt = 0;
	if($cnt > 0){
		echo "> 0";
		$pAddress = new PersonAddress();
		$pAddress->setCountryid("233");

		$form = $this->createFormBuilder($pAddress)
				->add('countryid', 'text')
				->getForm();

		$form->handleRequest($request);
		return $this->render('AcmeUserBundle:Default:contacts.html.twig', array('form' => $form->createView(),'id' => $id,));

	} else {
		    
	    $pAddress = new PersonAddress();

	    $form = $this->createFormBuilder($pAddress)->getForm();
	    $form->handleRequest($request);

	    if ($request->getMethod() == 'POST') {
		if ($form->isValid()) {
			
		    $country_id = $request->request->get('country');

		    // find last id where value not null
		    $cnt = count($request->request->get('sub_category'))-1;
		    while($request->request->get('sub_category')[$cnt] == ''){
			$cnt--;
			$unit_id = $request->request->get('sub_category')[$cnt];
		    }

		    $street_id = $request->request->get('street');
		    $building_id = $request->request->get('building');
		    $address = $request->request->get('address');

    		    // INSERT
                    $PersonalInfoAddress = new PersonalInfoAddress();

                    $PersonalInfoAddress->setPersonId($person_id);
                    $PersonalInfoAddress->setAddressType(NULL);
                    $PersonalInfoAddress->setCountryId($country_id);
                    $PersonalInfoAddress->setAdministrativeUnitId($unit_id);
                    $PersonalInfoAddress->setStreetId($street_id);
                    $PersonalInfoAddress->setBuildingId($building_id);
                    $PersonalInfoAddress->setAddress($address);

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($PersonalInfoAddress);
                    $em->flush();
		    
		    return $this->redirect($this->generateUrl('profile_contacts')."/$id");
		    
		}
	    }
		    
	    return $this->render('AcmeUserBundle:Default:contacts.html.twig', array('form' => $form->createView(),'id' => $id,));

	}
    }
}