<?php
namespace Acme\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\UserBundle\Entity\PersonalInfoPassport;
use Acme\UserBundle\Entity\PersonalInfoItn;
use Acme\UserBundle\Entity\PersonalInfoBank;

class BusinessController extends Controller
{
    public function viewAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        // person_id
	$query = $em->createQuery("SELECT p.person_id FROM AcmeUserBundle:Person p WHERE p.user_id=:user_id"
	                                                )->setParameters(array(
	                                                            'user_id' => $id
	                                                            ));
	$person_id = $query->getSingleScalarResult();
	// select passport
	$query = $em->createQuery("SELECT passport FROM AcmeUserBundle:PersonalInfoPassport passport WHERE passport.person_id=:person_id"
	                                                )->setParameters(array(
	                                                            'person_id' => $person_id
	                                                            ));
	$passport = $query->getArrayResult();
	// select itn
	$query = $em->createQuery("SELECT itn FROM AcmeUserBundle:PersonalInfoItn itn WHERE itn.person_id=:person_id"
	                                                )->setParameters(array(
	                                                            'person_id' => $person_id
	                                                            ));
	$itn = $query->getArrayResult();
	// select bank
	$query = $em->createQuery("SELECT bank FROM AcmeUserBundle:PersonalInfoBank bank WHERE bank.person_id=:person_id"
	                                                )->setParameters(array(
	                                                            'person_id' => $person_id
	                                                            ));
	$bank = $query->getArrayResult();
	                                                                                                                                                                                   
	// passport
	$newPassport = new PersonalInfoPassport();
	if(count($passport) > 0){
	    $newPassport->setType($passport[0]['type']);
	    $newPassport->setPassport($passport[0]['passport']);
	    $newPassport->setBeginDate($passport[0]['begin_date']);
	    $newPassport->setEndDate($passport[0]['end_date']);
	    $newPassport->setIssuer($passport[0]['issuer']);
	    }
	$formPassport = $this->createFormBuilder($newPassport)
		->add('type', 'choice', array('choices' => array(0 => 'Український', 1=>'Закордонний') ))
		->add('passport', 'text')
		->add('begin_date', 'date', array('widget' => 'single_text','format' => 'yyyy-MM-dd' ))
		->add('issuer', 'text')
		->add('end_date', 'date', array('widget' => 'single_text','format' => 'yyyy-MM-dd' ))
		->getForm();
	// itn
	$newItn = new PersonalInfoItn();
	if(count($itn) > 0){
	    $newItn->setItn($itn[0]['itn']);
	    }
	$formItn = $this->createFormBuilder($newItn)
		->add('itn', 'number')
		->getForm();
	// bank
	$newBank = new PersonalInfoBank();
	$formBank = $this->createFormBuilder($newBank)
            ->add('title', 'text', array('required' => true))
            ->add('mfo', 'text', array('required' => true))
            ->add('code', 'text', array('required' => true))
            ->add('rr', 'text', array('required' => true))
            ->add('description', 'text', array('required' => true))
            ->getForm();
                                                                        
	if ($request->getMethod() == 'POST') {
	    $formPassport->submit($request);
	    if ($formPassport->isValid()) {
		if(count($passport) == 0){
		    $newPassport = new PersonalInfoPassport();
		    $newPassport->setPersonId($person_id);
		    $newPassport->setType($formPassport->get('type')->getData());
		    $newPassport->setPassport($formPassport->get('passport')->getData());
		    $newPassport->setBeginDate($formPassport->get('begin_date')->getData());
		    $newPassport->setEndDate($formPassport->get('end_date')->getData());
		    $newPassport->setIssuer($formPassport->get('issuer')->getData());
		    $newPassport->setCreation(new \DateTime("now"));
		    $em->persist($newPassport);
		    $em->flush();
		    }
		else{
		    $newPassport = new PersonalInfoPassport();
		    $newPassport = $em->createQuery("UPDATE AcmeUserBundle:PersonalInfoPassport passport SET
							passport.type = :type,
							passport.passport = :passport,
							passport.begin_date = :begin_date,
							passport.end_date = :end_date,
							passport.issuer = :issuer,
							passport.creation = :creation
								WHERE
								    passport.person_id = :person_id"
									    )->setParameters(array(
										    'type' => $formPassport->get('type')->getData(),
										    'passport' => $formPassport->get('passport')->getData(),
										    'begin_date' => $formPassport->get('begin_date')->getData(),
										    'end_date' => $formPassport->get('end_date')->getData(),
										    'issuer' => $formPassport->get('issuer')->getData(),
										    'creation' => new \DateTime("now"),
										    'person_id' => $person_id
												));
		    $newPassport->getResult();
		    $em->flush();
    		    }
		}
	    $formItn->submit($request);
	    if ($formItn->isValid()) {
		if(count($itn) == 0){
		    $newItn = new PersonalInfoItn();
		    $newItn->setPersonId($person_id);
		    $newItn->setItn($formItn->get('itn')->getData());
		    $newItn->setCreation(new \DateTime("now"));
		    $em->persist($newItn);
		    $em->flush();
		    }
		else {
		    $newItn = new PersonalInfoItn();
		    $newItn = $em->createQuery("UPDATE AcmeUserBundle:PersonalInfoItn itn SET
							itn.itn = :itn,
							itn.creation = :creation
								WHERE
								    itn.person_id = :person_id"
									    )->setParameters(array(
										    'itn' => $formItn->get('itn')->getData(),
										    'creation' => new \DateTime("now"),
										    'person_id' => $person_id
												));
		    $newItn->getResult();
		    $em->flush();
		    }
		}
	    $formBank->submit($request);
	    if ($formBank->isValid()) {
		$newBank = new PersonalInfoBank();
		$newBank->setPersonId($person_id);
		$newBank->setTitle($formBank->get('title')->getData());
		$newBank->setMfo($formBank->get('mfo')->getData());
		$newBank->setCode($formBank->get('code')->getData());
		$newBank->setRr($formBank->get('rr')->getData());
		$newBank->setDescription($formBank->get('description')->getData());
		$newBank->setCreation(new \DateTime("now"));
		$em->persist($newBank);
		$em->flush();
		}
	    return $this->redirect($this->generateUrl('acme_profile_business', array('id'=> $id)));
	    }
    return $this->render('AcmeUserBundle:Default:business.html.twig', array('formPassport' => $formPassport->createView(), 'formItn' => $formItn->createView(), 'formBank' => $formBank->createView(), 'bank'=>$bank, 'id' => $id));
    }

    public function deleteBankAction($id,$bankId)
    {
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
			"DELETE FROM AcmeUserBundle:PersonalInfoBank bank WHERE bank.bank_id=:bankId"
					)->setParameters(array(
						'bankId' => $bankId
						));
	$query->getResult();
	return $this->redirect($this->generateUrl('acme_profile_business', array('id'=> $id)));
    }

}