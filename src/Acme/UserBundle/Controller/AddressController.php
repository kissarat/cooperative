<?php
namespace Acme\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\UserBundle\Entity\PersonalInfoAddress;
use Acme\UserBundle\Entity\Country;
use Acme\UserBundle\Entity\Coatsu;

use Acme\UserBundle\Entity\PersonAddress;

class AddressController extends Controller
{
    public function viewAction(Request $request,$id)
        {
                $em = $this->getDoctrine()->getManager();

		$query = $em->createQuery('SELECT address FROM AcmeUserBundle:PersonalInfoAddress address WHERE address.person_id = :person_id'
						)->setParameters(array(
		                        			'person_id' => $id,
		                                                ));
		$address = $query->getArrayResult();

		if(count($address) > 0){
		    echo "> 0";
		} else {
		    echo "0";
		  }



/*		
		$Address = new PersonAddress();

		$Address->setCountryid($country[0]['country_id']);
		$Address->setTitle($country[0]['title']);

		$arrayCountry = array();
		for($i=0;$i<count($country);$i++){
		    $arrayCountry[$country[$i]['country_id']] = $country[$i]['title'];
		    }

		$query = $em->createQuery('SELECT coatsu FROM AcmeUserBundle:Coatsu coatsu ORDER BY coatsu.unit_name');
		$coatsu = $query->getArrayResult();
		
		$Address->setCountryid($coatsu[0]['country_id']);
		$Address->setTitle($country[0]['title']);
		$arrayCoatsu = array();
		for($i=0;$i<count($coatsu);$i++){
		    $arrayCoatsu[$coatsu[$i]['administrative_unit_id']] = $coatsu[$i]['unit_name'];
		    }

		$form = $this->createFormBuilder($Address)
				    ->add('title', 'choice', array('choices' => $arrayCountry, 'preferred_choices' => array(233),))
				    ->add('unitname', 'choice', array('choices' => $arrayCoatsu))
				    ->getForm();
*/










/*
		if ($request->getMethod() == 'POST') {
		    $form->submit($request);
		    if ($form->isValid()) {
			$id = $form->get('id')->getData();
                	$personalinfo = $em->createQuery(
			                    'SELECT info FROM AcmeUserBundle:Person info WHERE info.user_id = :user_id'
			                    )->setParameters(array(
			                	'user_id' => $id,
			                        ));
			if (!$personalinfo) { throw $this->createNotFoundException('No user found for id='.$id); }
			else {
                	    $personalinfo = $em->createQuery(
                				    "UPDATE AcmeUserBundle:Person person SET
                					 person.lastname = :lastname,
                					 person.firstname = :firstname,
                					 person.middlename = :middlename,
                					 person.prefix = :prefix,
                					 person.title = :title,
                					 person.suffix = :suffix,
                					 person.preferredname = :preferredname,
                					 person.sex = :sex,
                					 person.birthday = :birthday
                					     WHERE
                					         person.user_id = :user_id"
                					         )->setParameters(array(
                					            'lastname' => $form->get('lastname')->getData(),
                					            'firstname' => $form->get('firstname')->getData(),
                					            'middlename' => $form->get('middlename')->getData(),
                					            'prefix' => $form->get('prefix')->getData(),
                					            'title' => $form->get('title')->getData(),
                					            'suffix' => $form->get('suffix')->getData(),
                					            'preferredname' => $form->get('preferredname')->getData(),
                					            'sex' => $form->get('sex')->getData(),
                					            'birthday' => $form->get('birthday')->getData(),
                					            'user_id' => $id,
                					         ));

			    $personalinfo->getResult();
			    $em->flush();
			}
			return $this->render('AcmeUserBundle:Default:contacts.html.twig', array('form' => $form->createView(),'id' => $id));
		    }
		}
*/
		return $this->render('AcmeUserBundle:Default:address.html.twig', array('id' => $id));
        }
}