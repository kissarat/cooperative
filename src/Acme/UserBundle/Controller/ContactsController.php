<?php
namespace Acme\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\UserBundle\Entity\Country;
use Acme\UserBundle\Entity\Coatsu;
use Acme\UserBundle\Entity\CommonStreet;
use Acme\UserBundle\Entity\CommonBuilding;

use Acme\UserBundle\Entity\Person;
use Acme\UserBundle\Entity\PersonalInfoAddress;

class ContactsController extends Controller
{
    public function viewAction(Request $request,$id)
    {

	$em = $this->getDoctrine()->getManager();
    	$query = $em->createQuery(
                            "SELECT p.person_id FROM AcmeUserBundle:Person p WHERE p.user_id=:user_id"
                                                    )->setParameters(array(
                                                                'user_id' => $id
                                                                ));
        $person_id = $query->getSingleScalarResult();

    	$query = $em->createQuery(
                            "SELECT a.address_id,country.title,coatsu.unit_name,street.title as stitle,building.building_no,a.address FROM AcmeUserBundle:PersonalInfoAddress a 
                        	JOIN AcmeUserBundle:Country country WITH a.country_id=country.country_id
                        	JOIN AcmeUserBundle:Coatsu coatsu WITH a.administrative_unit_id=coatsu.administrative_unit_id
                        	JOIN AcmeUserBundle:CommonStreet street WITH a.street_id=street.street_id
                        	JOIN AcmeUserBundle:CommonBuilding building WITH a.building_id=building.building_id
                        	    WHERE
                        		a.person_id=:person_id"
                                                    )->setParameters(array(
                                                                'person_id' => $person_id
                                                                ));
        $address = $query->getResult();

	$pAddress = new PersonalInfoAddress();
	$form = $this->createFormBuilder($pAddress)
//			->add('country_id', 'choice', array('required' => true))
//			->add('administrative_unit_id', 'choice', array('required' => false))
//			->add('street_id', 'choice', array('required' => false))
//			->add('building_id', 'choice', array('required' => false))
//			->add('address', 'text', array('required' => false))
			->getForm();
	
	if ($request->getMethod() == 'POST')
	{
	    $form->submit($request);
	    if ($form->isValid())
	    {
		$country_id = $request->request->get('country_id');
		$administrative_unit_id = $request->request->get('administrative_unit_id');
		$street_id = $request->request->get('street_id');
		$building_id = $request->request->get('building_id');
		$address = $request->request->get('address');
		
		$pAddress = new PersonalInfoAddress();
		$pAddress->setPersonId($person_id);
		$pAddress->setCountryId($country_id);
		$pAddress->setAdministrativeUnitId($administrative_unit_id);
		$pAddress->setStreetId($street_id);
		$pAddress->setBuildingId($building_id);
		$pAddress->setAddress($address);
		
	    
	        $em = $this->getDoctrine()->getManager();
	        $em->persist($pAddress);
		$em->flush();
	        
	        return $this->redirect($this->generateUrl('acme_profile_contacts', array('id'=> $id)));
	    }
	}
	return $this->render('AcmeUserBundle:Default:contacts.html.twig', array('form' => $form->createView(),'id' => $id, 'address' => $address));
    }

    public function addressDeleteAction($id)
    {
	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$em = $this->getDoctrine()->getManager();
    	$query = $em->createQuery(
                            "DELETE FROM AcmeUserBundle:PersonalInfoAddress a WHERE a.address_id=:id"
                                                    )->setParameters(array(
                                                                'id' => $id
                                                                ));
        $query->getResult();

	return $this->redirect($this->generateUrl('acme_profile_contacts', array('id'=> $userId)));
    }

    // getCountry
    public function countryAction(Request $request)
    {

	$country = new Country();
	$q = $request->request->get('data');
	$q = mb_strtoupper($q['q'], 'UTF-8');

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT country FROM AcmeUserBundle:Country country WHERE country.title LIKE :title'
                                    	)->setParameters(array(
                                                'title' => '%'.$q.'%'
                                                ));
        $country = $query->getArrayResult();

        $results = array();
	foreach ($country as $c)
	{
	    $results[] = array('id' => $c['country_id'], 'text' => $c['title']);
	}

	return $this->render('AcmeUserBundle:Default:country.html.twig', array('array' => json_encode(array('q' => $q, 'results' => $results)) ));

    }

    // getCoatsu
    public function coatsuAction($id, Request $request)
    {

	$coatsu = new Coatsu();
	$q = $request->request->get('data');
	$q = mb_strtoupper($q['q'], 'UTF-8');

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT coatsu FROM AcmeUserBundle:Coatsu coatsu WHERE coatsu.country_id=:id AND coatsu.unit_name LIKE :title'
                                    	)->setParameters(array(
                                    		'id' => $id,
                                                'title' => '%'.$q.'%',
                                                ));
        $coatsu = $query->getResult();

        $results = array();
	foreach ($coatsu as $c)
	{
	    $results[] = array('id' => $c->getAdministrativeUnitId(), 'text' => $c->getUnitName());
	}

	return $this->render('AcmeUserBundle:Default:coatsu.html.twig', array('array' => json_encode(array('q' => $q, 'results' => $results)) ));

    }

    // getStreet
    public function streetAction($id, Request $request)
    {

	$street = new CommonStreet();
	$q = $request->request->get('data');
	$q = mb_strtoupper($q['q'], 'UTF-8');

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT street FROM AcmeUserBundle:CommonStreet street WHERE street.administrative_unit_id=:id AND street.title LIKE :title'
                                    	)->setParameters(array(
                                    		'id' => $id,
                                                'title' => '%'.$q.'%',
                                                ));
        $street = $query->getResult();

        $results = array();
	foreach ($street as $s)
	{
	    $results[] = array('id' => $s->getStreetId(), 'text' => $s->getTitle());
	}

	return $this->render('AcmeUserBundle:Default:street.html.twig', array('array' => json_encode(array('q' => $q, 'results' => $results)) ));

    }

    // getBuilding
    public function buildingAction($id, Request $request)
    {

	$building = new CommonBuilding();
	$q = $request->request->get('data');
	$q = $q['q'];

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT building FROM AcmeUserBundle:CommonBuilding building WHERE building.street_id=:id AND building.building_no LIKE :num'
                                    	)->setParameters(array(
                                                'id' => $id,
                                                'num' => '%'.$q.'%'
                                                ));
        $building = $query->getResult();

        $results = array();
	foreach ($building as $b)
	{
	    $results[] = array('id' => $b->getBuildingId(), 'text' => $b->getBuildingNo());
	}

	return $this->render('AcmeUserBundle:Default:building.html.twig', array('array' => json_encode(array('q' => $q, 'results' => $results)) ));

    }
        
}