<?php
namespace Acme\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\UserBundle\Entity\Country;
use Acme\UserBundle\Entity\Coatsu;
use Acme\UserBundle\Entity\CommonStreet;
use Acme\UserBundle\Entity\CommonBuilding;

use Acme\UserBundle\Entity\User;
use Acme\UserBundle\Entity\Person;
use Acme\UserBundle\Entity\PersonalInfonPerson;
use Acme\UserBundle\Entity\PersonalInfoAddress;

class UsersController extends Controller
{
    public function viewAction()
    {

	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$em = $this->getDoctrine()->getManager();
    	$query = $em->createQuery(
                            "SELECT u.id,inf.lastname,inf.firstname,country.title,coatsu.unit_name,street.title as stitle,building.building_no,a.address FROM AcmeUserBundle:User u
                        	JOIN AcmeUserBundle:Person p WITH u.id=p.user_id
                        	JOIN AcmeUserBundle:PersonalInfoPerson inf WITH p.person_id=inf.person_id
                        	JOIN AcmeUserBundle:PersonalInfoAddress a WITH p.person_id=a.person_id
                        	JOIN AcmeUserBundle:Country country WITH a.country_id=country.country_id
                        	JOIN AcmeUserBundle:Coatsu coatsu WITH a.administrative_unit_id=coatsu.administrative_unit_id
                        	JOIN AcmeUserBundle:CommonStreet street WITH a.street_id=street.street_id
                        	JOIN AcmeUserBundle:CommonBuilding building WITH a.building_id=building.building_id

                        	");
	$users = $query->getResult();
/*
    	$query = $em->createQuery(
                            "SELECT p.person_id FROM AcmeUserBundle:Person p WHERE p.user_id=:user_id"
                                                    )->setParameters(array(
                                                                'user_id' => $id
                                                                ));
        $person_id = $query->getSingleScalarResult();

    	$query = $em->createQuery(
                            "SELECT a.address_id,country.title,coatsu.unit_name,street.title as stitle,building.building_no,a.address FROM AcmeUserBundle:PersonalInfoAddress a 
                        	JOIN AcmeUserBundle:Country country WITH a.country_id=country.country_id
                        	JOIN AcmeUserBundle:Coatsu coatsu WITH a.administrative_unit_id=coatsu.administrative_unit_id
                        	JOIN AcmeUserBundle:CommonStreet street WITH a.street_id=street.street_id
                        	JOIN AcmeUserBundle:CommonBuilding building WITH a.building_id=building.building_id
                        	    WHERE
                        		a.person_id=:person_id"
                                                    )->setParameters(array(
                                                                'person_id' => $person_id
                                                                ));
        $address = $query->getResult();
*/
	return $this->render('AcmeUserBundle:Default:users.html.twig', array('id' => $userId, 'users' => $users));
    }

}