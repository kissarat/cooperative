<?php
namespace Acme\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\UserBundle\Entity\General;
use Acme\UserBundle\Entity\Person;

class ViewController extends Controller
{
    public function viewAction(Request $request,$id)
        {
                $em = $this->getDoctrine()->getManager();
		$query = $em->createQuery(
			'SELECT person FROM AcmeUserBundle:Person person WHERE person.user_id = :user_id'
			)->setParameters(array(
			    'user_id' => $id,
			    ));
		$person = $query->getArrayResult();

		// заглушка, перевірити!!!
		if(count($person) == 0){
		    $p = new Person();
		    $p->setUserId($id);
		    $p->setLastname("");
		    $p->setFirstname("");
		    $p->setBirthday(new \DateTime("now"));

		    $em = $this->getDoctrine()->getManager();
		    $em->persist($p);
		    $em->flush();
		    
		    return $this->redirect($this->generateUrl('acme_profile_view/$id'));
		}
		
		$general = new General();
		
		$general->setLastname($person[0]['lastname']);
		$general->setFirstname($person[0]['firstname']);
		$general->setMiddlename($person[0]['middlename']);
		$general->setPrefix($person[0]['prefix']);
		$general->setTitle($person[0]['title']);
		$general->setSuffix($person[0]['suffix']);
		$general->setPreferredname($person[0]['preferredname']);
		$general->setSex($person[0]['sex']);
		$general->setBirthday($person[0]['birthday']);

		$form = $this->createFormBuilder($general)
				    ->add('lastname', 'text')
				    ->add('firstname', 'text')
				    ->add('middlename', 'text', array('required' => false,))
				    ->add('prefix', 'hidden', array('required' => false,))
				    ->add('title', 'hidden', array('required' => false,))
				    ->add('suffix', 'hidden', array('required' => false,))
				    ->add('preferredname', 'hidden', array('required' => false,))
				    ->add('sex', 'choice', array(
					'choices' => array(''=>'- вибрати -',0=>'жіноча',1=>'чоловіча'),
					'required' => false,
					))
				    ->add('birthday', 'date', array('widget' => 'single_text','format' => 'yyyy-MM-dd' ))
				    ->add('id', 'hidden', array('data' => $id))
				    //->add('save', 'submit')
				    ->getForm();

		if ($request->getMethod() == 'POST') {
		    $form->submit($request);
		    if ($form->isValid()) {
			$id = $form->get('id')->getData();
//			$personalinfo = $this->getDoctrine()->getRepository('AcmeUserBundle:Person')->findByUser_id($id);
                	$personalinfo = $em->createQuery(
			                    'SELECT info FROM AcmeUserBundle:Person info WHERE info.user_id = :user_id'
			                    )->setParameters(array(
			                	'user_id' => $id,
			                        ));
			if (!$personalinfo) { throw $this->createNotFoundException('No user found for id='.$id); }
			else {
//			    $lastname = $form->get('lastname')->getData();
//			    $personalinfo->setLastname($lastname);
//                	    $personalinfo = $em->createQuery("UPDATE AcmeUserBundle:Person person SET person.lastname='$lastname' WHERE person.user_id = $id");
                	    $personalinfo = $em->createQuery(
                				    "UPDATE AcmeUserBundle:Person person SET
                					 person.lastname = :lastname,
                					 person.firstname = :firstname,
                					 person.middlename = :middlename,
                					 person.prefix = :prefix,
                					 person.title = :title,
                					 person.suffix = :suffix,
                					 person.preferredname = :preferredname,
                					 person.sex = :sex,
                					 person.birthday = :birthday
                					     WHERE
                					         person.user_id = :user_id"
                					         )->setParameters(array(
                					            'lastname' => $form->get('lastname')->getData(),
                					            'firstname' => $form->get('firstname')->getData(),
                					            'middlename' => $form->get('middlename')->getData(),
                					            'prefix' => $form->get('prefix')->getData(),
                					            'title' => $form->get('title')->getData(),
                					            'suffix' => $form->get('suffix')->getData(),
                					            'preferredname' => $form->get('preferredname')->getData(),
                					            'sex' => $form->get('sex')->getData(),
                					            'birthday' => $form->get('birthday')->getData(),
                					            'user_id' => $id,
                					         ));

			    $personalinfo->getResult();
			    $em->flush();
			}
			return $this->render('AcmeUserBundle:Default:view.html.twig', array('form' => $form->createView(),'id' => $id));
		    }
		}
		return $this->render('AcmeUserBundle:Default:view.html.twig', array('form' => $form->createView(),'id' => $id));
        }

}