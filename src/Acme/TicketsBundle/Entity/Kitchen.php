<?php 

namespace Acme\TicketsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
* @ORM\Table(name="kitchen")
 */
 class Kitchen
 {
/**
* @ORM\Id
 * @ORM\Column(type="integer")
 * @ORM\GeneratedValue(strategy="AUTO")
 */
protected $id;

 /**
* @ORM\Column(type="string", length=100)
* @Assert\NotBlank()
*/
 protected $name;
 
/**
* @ORM\Column(type="text")
* @Assert\NotBlank()
 */
protected $description;

 /**
 * @Assert\File(maxSize="6000000")
 * @Assert\Image(
*minWidth = 100,
*maxWidth = 2800,
*minHeight = 100,
*maxHeight = 2800
* )
 */
protected $mainImage;

 /**
 * @ORM\Column(type="string", length=255, nullable=true)
 */
protected $mainImagePath;

 /**
* @Assert\Type(type="Acme\TicketsBundle\Entity\KitchenSubImage")
* @ORM\OneToMany(targetEntity="KitchenSubImage", mappedBy="kitchen", cascade={"persist","remove"})
*/
 protected $subImages;
 
public function __construct()
{
$this->subImages = new ArrayCollection();
}

 public function getName()
{
return $this->name;
 }
 
public function setName($name)
 {
 $this->name = $name;
}

 public function getDescription()
{
return $this->description;
 }
 
public function setDescription($description)
 {
 $this->description = $description;
}

 public function getMainImage()
{
return $this->mainImage;
 }
 
public function setMainImage(UploadedFile $mainImage = null)
 {
 $this->mainImage = $mainImage;
}

 public function getSubImages()
{
return $this->subImages;
 }
 
//public function setSubImage(KitchenSubImage $subImage = null)
//{
//$this->subImage = $subImage;
//}

 /**
 * Get id
*
* @return integer 
*/
 public function getId()
{
return $this->id;
 }
 
/**
* Set mainImagePath
*
 * @param string $mainImagePath
 * @return Kitchen
 */
public function setMainImagePath($mainImagePath)
 {
 $this->mainImagePath = $mainImagePath;
 
 return $this;
}

 /**
* Get mainImagePath
*
* @return string 
*/
 public function getMainImagePath()
{
return $this->mainImagePath;
 }
 
public function getAbsolutePath()
 {
 return null === $this->mainImagePath
? null
 : $this->getUploadRootDir().'/'.$this->mainImagePath;
}

 public function getWebPath()
{
return null === $this->mainImagePath
 ? null
: $this->getUploadDir().'/'.$this->mainImagePath;
 }
 
public function getUploadRootDir()
 {
 return __DIR__.'/../../../../web/'.$this->getUploadDir();
}

 public function getUploadDir()
{
return 'uploads/our-work';
 }
 
public function upload()
 {
 if (null === $this->getMainImage()) {return;}

    $this->getMainImage()->move(
	$this->getUploadRootDir(),
	$this->getMainImage()->getClientOriginalName()
	);

    $this->mainImagePath = $this->getMainImage()->getClientOriginalName();

    $this->mainImage = null;
 }
 
/**
* Add subImage
 *
 * @param \Acme\TicketsBundle\Entity\KitchenSubImage $subImage
 * @return Kitchen
*/
public function addSubImage(\Acme\TicketsBundle\Entity\KitchenSubImage $subImages)
{
$this->subImages[] = $subImages;
$subImages->setKitchen($this); # used for persisting
return $this;
}

/**
* Remove subImage
*
* @param \Acme\TicketsBundle\Entity\KitchenSubImage $subImage
*/
public function removeSubImage(\Acme\TicketsBundle\Entity\KitchenSubImage $subImage)
{
$this->subImage->removeElement($subImage);
}
}
