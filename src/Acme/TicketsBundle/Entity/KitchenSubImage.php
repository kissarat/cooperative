<?php

namespace Acme\TicketsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
* @ORM\Table(name="kitchenImages")
 */
 class KitchenSubImage 
 {
 /**
* @ORM\Id
 * @ORM\Column(type="integer")
* @ORM\GeneratedValue(strategy="AUTO")
 */
 protected $id;
 
 /**
* @Assert\Image(
 * minWidth = 100,
* maxWidth = 2800,
 * minHeight = 100,
* maxHeight = 2800
 * )
*/
public $image;

/**
 * @ORM\Column(type="string", length=255, nullable=true)
*/
public $imagePath;

/**
* @ORM\ManyToOne(targetEntity="Kitchen", inversedBy="subImages")
* @ORM\JoinColumn(name="kitchen_id", referencedColumnName="id")
**/
 protected $kitchen;
 
 public function getImage()
 {
 return $this->image;
 }
 
 public function setImage(UploadedFile $image = null)
 {
 $this->image = $image;
 }
 
 public function getAbsolutePath()
 {
 return null === $this->imagePath
 ? null
 : $this->getUploadRootDir().'/'.$this->imagePath;
 }
 
 public function getWebPath()
 {
 return null === $this->imagePath
 ? null
 : $this->getUploadDir().'/'.$this->imagePath;
 }
 
 public function getUploadRootDir()
 {
 return __DIR__.'/../../../../web/'.$this->getUploadDir();
 }
 
 public function getUploadDir()
 {
 return 'uploads/our-work';
 }
 
 public function upload()
 {
 if (null === $this->getImage()) {
 print"NUll";
 return;
 }
 
 $this->getImage()->move(
 $this->getUploadRootDir(),
 $this->getImage()->getClientOriginalName()
 );
 
 $this->mainImagePath = $this->getImage()->getClientOriginalName();
 
 $this->mainImage = null;
 }
 
 /**
* Set imagePath
 *
* @param string $imagePath
 * @return KitchenSubImage
*/
public function setImagePath($imagePath)
{
$this->imagePath = $imagePath;

return $this;
}

/**
 * Get imagePath
*
 * @return string 
*/
public function getImagePath()
{
return $this->imagePath;
}

/**
 * Set kitchen
*
 * @param \Acme\TicketsBundle\Entity\Kitchen $kitchen
* @return KitchenSubImage
 */
 public function setKitchen(\Acme\TicketsBundle\Entity\Kitchen $kitchen = null)
 {
 $this->kitchen = $kitchen;
 
 return $this;
 }
 
 /**
* Get kitchen
 *
* @return \Acme\TicketsBundle\Entity\Kitchen 
 */
 public function getKitchen()
 {
 return $this->kitchen;
 }
 
 /**
* Get id
 *
* @return integer 
 */
 public function getId()
 {
 return $this->id;
 }
 }
