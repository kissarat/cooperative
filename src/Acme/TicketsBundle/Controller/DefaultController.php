<?php

namespace Acme\TicketsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\TicketsBundle\Entity\Kitchen;
use Acme\TicketsBundle\Form\KitchenType;

use Acme\TicketsBundle\Entity\KitchenSubImage;
use Acme\TicketsBundle\Form\KitchenSubImageType;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
    
	$kitchen = new Kitchen();
	$image = new KitchenSubImage();

	$kitchen->addSubImage($image);

	$form = $this->createForm(new KitchenType(), $kitchen);

	$form->handleRequest($request);

        if ($request->getMethod() == 'POST') {
//	    if ($form->isValid()) {
		$kitchen->upload();
		foreach($kitchen->getSubImages() as $subImage){ $subImage->upload();}
		$em = $this->getDoctrine()->getManager();
        	$em->persist($kitchen);
		$em->flush();	    
//	    }
//	    else{print"Ne valid";}
	}
                                                                                                            
	return $this->render('AcmeTicketsBundle:Default:index.html.twig', array(
	    'form' => $form->createView(),
	    ));

    }
}