<?php 

namespace Acme\TicketsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class KitchenType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name');
        $builder->add('description', 'textarea');
        $builder->add('mainImage', 'file');
        $builder->add('subImages', 'collection', array(
                'type' => new KitchenSubImageType(), 
                'label' => false,
                'allow_add' => true, 
                'by_reference' => false,
                ));
        $builder->add('submit', 'submit');
    }
                                                                                                            
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\TicketsBundle\Entity\Kitchen',
            'cascade_validation' => true,
            ));
    }
                                                                                                                                                                
    public function getName()
    {
        return 'kitchen';
    }
}