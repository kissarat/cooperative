<?php

namespace Acme\FormBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
* Acme\ProductBundle\Entity\Telephone
*
* @ORM\Table(name="telephone")
* @ORM\Entity
*/
class Telephone
{

/**
* @ORM\Column(name="phone_number", type="string")
*/

private $path;

/**
* @var integer
*
* @ORM\Column(name="id", type="integer")
* @ORM\Id
* @ORM\GeneratedValue(strategy="IDENTITY")
*/
 private $id;
 
/**
* @var \Acme\FormBundle\Entity\User1
*
* @ORM\ManyToOne(targetEntity="Acme\FormBundle\Entity\User1", inversedBy="telephones", cascade={"persist"})
* @ORM\JoinColumns({
* @ORM\JoinColumn(name="id_user", referencedColumnName="id")
* })
*/
private $user;

/**
* Set user
*
* @param \Acme\FormBundle\Entity\User1 $user
* @return Telephone
*/
public function setUser(\Acme\FormBundle\Entity\User1 $user = null)
{
$this->user = $user;
return $this;
}
 
/**
* Get user
*
* @return \Acme\FormBundle\Entity\User1 
*/
public function getUser()
{
return $this->user;
}

/**
* Get id
*
* @return integer 
*/
public function getId()
{
return $this->id;
}
/* --------------------------------------------------------------------------- */
public function getAbsolutePath(){ return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path; }
public function getWebPath(){ return null === $this->path ? null : $this->getUploadDir().'/'.$this->path; }
protected function getUploadRootDir(){ return __DIR__.'/../../../../web/'.$this->getUploadDir(); }
protected function getUploadDir(){ return 'uploads/documents'; }

/**
* @Assert\File(
*   maxSize="10M",
*   mimeTypes = {
*          "image/png",
*          "image/jpeg",
*          "image/jpg",
*          "image/gif",
*          "application/pdf",
*          "application/x-pdf"
*          }
*  )
*/
private $phoneNumber;
/**
* Set phoneNumber
* @param UploadedFile $phoneNumber
* @return Telephone
*/
public function setPhoneNumber(UploadedFile $phoneNumber = null)
{
    $this->phoneNumber = $phoneNumber;
}
/**
* Get phoneNumber
* @return UploadedFile
*/
public function getPhoneNumber()
{
return $this->phoneNumber;
}
public function upload()
{
// the file property can be empty if the field is not required
if (null === $this->getPhoneNumber()) {
return;
}
// use the original file name here but you should
// sanitize it at least to avoid any security issues
// move takes the target directory and then the
// target filename to move to
$this->getPhoneNumber()->move(
$this->getUploadRootDir(),
$this->getPhoneNumber()->getClientOriginalName()
);
// set the path property to the filename where you've saved the file
$this->path = $this->getPhoneNumber()->getClientOriginalName();
// clean up the file property as you won't need it anymore
$this->phoneNumber = null;
}

/* -------------------------------------------------------------------------- */

/**
* Set path
*
* @param string $path
* @return Telephone
*/
public function setPath($path)
{
$this->path = $path;
return $this;
}

/**
* Get path
*
* @return string 
*/
public function getPath()
{
return $this->path;
}

}
