<?php

namespace Acme\FormBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acme\ProductBundle\Entity\User1
 *
 * @ORM\Table(name="user1")
 * @ORM\Entity
 */

class User1
{

 /**
 * @ORM\Column(name="name", type="string")
 */
 
private $name;

 /**
 * @ORM\Column(name="email", type="string")
 */
 
private $email;

/**
 * @var integer
 *
 * @ORM\Column(name="id", type="integer")
 * @ORM\Id
 * @ORM\GeneratedValue(strategy="IDENTITY")
 */
 private $id;
 
 /** 
 * @var \Acme\FormBundle\Entity\Telephone
 *
 * @ORM\OneToMany(targetEntity="Acme\FormBundle\Entity\Telephone", mappedBy="user", cascade={"ALL"})
 */
 private $telephones;
 
 /**
 * Constructor
 */
 public function __construct()
 {
 $this->telephones = new \Doctrine\Common\Collections\ArrayCollection();
 }
 
 /**
 * Add telephones
 *
 * @param \Acme\FormBundle\Entity\Telephone $telephones
 * @return User
 */
 public function addTelephone(\Acme\FormBundle\Entity\Telephone $telephones)
 {
 $this->telephones[] = $telephones;
 $telephones->setUser($this);
 return $this;
 }
 /**
 * Remove telephones
 *
 * @param \Acme\FormBundle\Entity\Telephone $telephones
 */
 public function removeTelephone(\Acme\FormBundle\Entity\Telephone $telephones)
 {
 $this->telephones->removeElement($telephones);
 }
 
 /**
 * Get telephones
 *
 * @return \Doctrine\Common\Collections\Collection 
 */
 public function getTelephones()
 {
 return $this->telephones;
 }
 
 /**
 * Get id
 *
 * @return integer 
 */
 public function getId()
 {
 return $this->id;
 }

 /**
 * Set name
 *
 * @param string $name
 * @return User1
 */
 public function setName($name)
 {
 $this->name = $name;

 return $this;
 }

 /**
 * Get name
 *
 * @return string 
 */
 public function getName()
 {
 return $this->name;
 }

 /**
 * Set email
 *
 * @param string $email
 * @return User1
 */
 public function setEmail($email)
 {
 $this->email = $email;

 return $this;
 }

 /**
 * Get email
 *
 * @return string 
 */
 public function getEmail()
 {
 return $this->email;
 }
}
