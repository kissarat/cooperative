<?php

namespace Acme\FormBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Acme\FormBundle\Entity\Telephone;

class UserType extends AbstractType
{
public function buildForm(FormBuilderInterface $builder, array $options)
{
    $builder->add('name')
            ->add('email')
                    ->add('telephones', 'collection', array('type' => new TelephoneType()))
                            ->add('save it', 'submit');
                            }
                            
                            public function setDefaultOptions(OptionsResolverInterface $resolver)
                            {
                                $resolver->setDefaults(array(
                                        'data_class' => 'Acme\FormBundle\Entity\User1',
                                                ));
                                                }
                                                
                                                public function getName()
                                                {
                                                    return 'user';
                                                    }
                                                    }