<?php

namespace Acme\FormBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;

class ImageController extends Controller
{
    public function indexAction($id)
    {
	$guesser = MimeTypeGuesser::getInstance();
	$mimetype =  $guesser->guess("uploads/documents/$id");

    	$headers = array('Content-Type' => '$mimetype', 'Content-Disposition' => 'inline; filename="$id"'); // mimetype = 'image/jpg'
    	return new Response(file_get_contents("uploads/documents/$id"), 200, $headers);


    }
}
