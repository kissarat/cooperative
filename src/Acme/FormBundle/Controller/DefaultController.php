<?php

namespace Acme\FormBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\FormBundle\Entity\User1;
use Acme\FormBundle\Form\UserType;

use Acme\FormBundle\Entity\Telephone;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
	$user = new User1();
	$Telephone1 = new Telephone();
	$Telephone2 = new Telephone();

	$user->addTelephone($Telephone1);
	$user->addTelephone($Telephone2);
            
	$form = $this->createForm(new UserType(), $user);
	$form->handleRequest($request);
	
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery('SELECT users.name,users.email,t.path,t.id FROM AcmeFormBundle:User1 users JOIN users.telephones t');
	$result = $query->getResult();


	if ($form->isValid()) {
    	    $em = $this->getDoctrine()->getManager();
    	    $Telephone1->upload();
    	    $Telephone2->upload();
    	    $em->persist($user);
    	    $em->flush();
    	    }
                                                    
	return $this->render('AcmeFormBundle:Default:index.html.twig', array('form' => $form->createView(),'result'=>$result));
    }
}