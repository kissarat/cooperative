<?php

namespace Acme\CatalogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {

	if($name != 'goods' && $name != 'services' && $name != 'technolgy' && $name != 'investment'){$parent = '';}

	if($name == 'goods'){$id=3;$parent=NULL;}
	if($name == 'services'){$id=0;$parent=5;}
	if($name == 'technology'){$id=0;$parent=1213;}
	if($name == 'investment'){$id=0;$parent=1214;}
	if($name == 'users'){$id=0;$parent=6943;}

        return $this->render('AcmeCatalogBundle:Default:index.html.twig', array('name' => $name, 'id' => $id, 'parent' => $parent));
    }
}
