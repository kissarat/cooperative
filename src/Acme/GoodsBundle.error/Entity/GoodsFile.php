<?php

namespace Acme\GoodsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="goods_file")
 */
class GoodsFile
{
    /**
     * @var integer $id
     *
     * @ORM\Id
     * @ORM\Column(name="id_file", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id_file;

    /**
     * @var string $path
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=false)
     */
    private $path;

    /**
     * @ORM\ManyToOne(targetEntity="Goods", inversedBy="file")
     * @ORM\JoinColumn(name="id_goods", referencedColumnName="id")
     */
    protected $file;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
	return $this->id;
    }

    /**
     * Set path
     *
     * @param string $path
     */
    public function setPath($path)
    {
	$this->path = $path;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
	return $this->path;
    }

    /**
     * Set goods
     *
     * @param Acme\GoodsBundle\Entity\Goods $goods
     */
    public function setGoods(\Acme\GoodsBundle\Entity\Goods $goods)
    {
	$this->goods = $goods;
    }

    /**
     * Get goods
     *
     * @return Acme\GoodsBundle\Entity\Goods
     */
    public function getGoods()
    {
	return $this->goods;
    }

    private $noFile = 1;

    public function getNoFile()
    {
	return $this->noFile;
    }

    public function setNoFile($noFile)
    {
	$this->noFile = $noFile;
    }

    public function __set($fieldName, $value)
    {

    }

    public function __get($fieldName)
    {

    }

    /**
     * Get id_file
     *
     * @return integer 
     */
    public function getIdFile()
    {
        return $this->id_file;
    }
}
