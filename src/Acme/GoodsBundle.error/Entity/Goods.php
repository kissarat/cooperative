<?php
// src/Acme/UserBundle/Entity/Goods.php
namespace Acme\GoodsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="goods")
*/

class Goods
{
    /**
    * @ORM\Id
    * @ORM\Column(name="id_goods", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id_goods;
    
    /**
    * @ORM\Column(name="id_u", type="integer")
    */
    protected $id_u;
    
    /**
    * @ORM\Column(name="type", type="integer")
    */
    protected $type;
    
    /**
    * @ORM\Column(name="title", type="string")
    */
    protected $title;
    
    /**
    * @ORM\Column(name="ua_code", type="integer")
    */
    protected $ua_code;

    /**
    * @ORM\Column(name="category", type="integer")
    */
    protected $category;

    /**
    * @ORM\Column(name="avail", type="integer")
    */
    protected $avail;
    
    /**
    * @ORM\Column(name="volume", type="integer")
    */
    protected $volume;
    
    /**
    * @ORM\Column(name="price_min", type="integer")
    */
    protected $price_min;                            
    
    /**
    * @ORM\Column(name="price_max", type="integer")
    */
    protected $price_max;
    
    /**
    * @ORM\Column(name="price_currency", type="string")
    */
    protected $price_currency;
                                    
    /**
    * @ORM\Column(name="price_unit", type="string")
    */
    protected $price_unit;
                                                    
    /**
    * @ORM\Column(name="return", type="integer")
    */
    protected $return;

    /**
    * @ORM\Column(name="return_currency", type="string")
    */
    protected $return_currency;
                    
    /**
    * @ORM\Column(name="return_unit", type="string")
    */
    protected $return_unit;
    
    /**
    * @ORM\Column(name="contribution", type="integer")
    */
    protected $contribution;
    
    /**
    * @ORM\Column(name="contribution_currency", type="string")
    */
    protected $contribution_currency;
                    
    /**
    * @ORM\Column(name="contribution_unit", type="string")
    */
    protected $contribution_unit;                                                                                                        

    /**
    * @ORM\Column(name="contribution_account", type="string")
    */
    protected $contribution_account; 

    /**
    * @ORM\Column(name="descriptions", type="text")
    */
    protected $descriptions;
    
    /**
    * @ORM\Column(name="country", type="string")
    */
    protected $country;
    
    /**
    * @ORM\Column(name="region", type="string")
    */
    protected $region;
    
    /**
    * @ORM\Column(name="district", type="string")
    */
    protected $district;
    
    /**
    * @ORM\Column(name="settlement", type="string")
    */
    protected $settlement;
    
    /**
    * @ORM\Column(name="filter", type="text")
    */
    protected $filter;
    
    /**
    * @ORM\Column(name="brand", type="text")
    */
    protected $brand;
    
    /**
    * @ORM\Column(name="brand_country", type="text")
    */
    protected $brand_country;
    
    /**
    * @ORM\Column(name="goods_code", type="string")
    */
    protected $goods_code;
   
    /**
    * @ORM\Column(name="goods_url", type="string")
    */
    protected $goods_url;
    
    /**
    * @ORM\Column(name="frequancy_volume", type="text")
    */
    protected $frequancy_volume;
    
    /**
    * @ORM\Column(name="single", type="integer")
    */
    protected $single;
    
    /**
    * @ORM\Column(name="pickup", type="text")
    */
    protected $pickup;
    
    /**
    * @ORM\Column(name="courier", type="integer")
    */
    protected $courier;
    
    /**
    * @ORM\Column(name="transportation", type="text")
    */
    protected $transportation;

    /**
    * @ORM\Column(name="status", type="integer")
    */
    protected $status;
    
    /**
    * @ORM\Column(name="creation", type="date")
    */
    protected $creation;

    /**
     * @ORM\OneToMany(targetEntity="GoodsFile")
     */
    private $file;

    /**
     * Get id_goods
     *
     * @return integer 
     */
    public function getIdGoods()
    {
        return $this->id_goods;
    }

    /**
     * Set id_u
     *
     * @param integer $idU
     * @return Goods
     */
    public function setIdU($idU)
    {
        $this->id_u = $idU;

        return $this;
    }

    /**
     * Get id_u
     *
     * @return integer 
     */
    public function getIdU()
    {
        return $this->id_u;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Goods
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Goods
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set ua_code
     *
     * @param integer $uaCode
     * @return Goods
     */
    public function setUaCode($uaCode)
    {
        $this->ua_code = $uaCode;

        return $this;
    }

    /**
     * Get ua_code
     *
     * @return integer 
     */
    public function getUaCode()
    {
        return $this->ua_code;
    }

    /**
     * Set category
     *
     * @param integer $category
     * @return Goods
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return integer 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set avail
     *
     * @param integer $avail
     * @return Goods
     */
    public function setAvail($avail)
    {
        $this->avail = $avail;

        return $this;
    }

    /**
     * Get avail
     *
     * @return integer 
     */
    public function getAvail()
    {
        return $this->avail;
    }

    /**
     * Set volume
     *
     * @param integer $volume
     * @return Goods
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume
     *
     * @return integer 
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set price_min
     *
     * @param integer $priceMin
     * @return Goods
     */
    public function setPriceMin($priceMin)
    {
        $this->price_min = $priceMin;

        return $this;
    }

    /**
     * Get price_min
     *
     * @return integer 
     */
    public function getPriceMin()
    {
        return $this->price_min;
    }

    /**
     * Set price_max
     *
     * @param integer $priceMax
     * @return Goods
     */
    public function setPriceMax($priceMax)
    {
        $this->price_max = $priceMax;

        return $this;
    }

    /**
     * Get price_max
     *
     * @return integer 
     */
    public function getPriceMax()
    {
        return $this->price_max;
    }

    /**
     * Set price_currency
     *
     * @param string $priceCurrency
     * @return Goods
     */
    public function setPriceCurrency($priceCurrency)
    {
        $this->price_currency = $priceCurrency;

        return $this;
    }

    /**
     * Get price_currency
     *
     * @return string 
     */
    public function getPriceCurrency()
    {
        return $this->price_currency;
    }

    /**
     * Set price_unit
     *
     * @param string $priceUnit
     * @return Goods
     */
    public function setPriceUnit($priceUnit)
    {
        $this->price_unit = $priceUnit;

        return $this;
    }

    /**
     * Get price_unit
     *
     * @return string 
     */
    public function getPriceUnit()
    {
        return $this->price_unit;
    }

    /**
     * Set return
     *
     * @param integer $return
     * @return Goods
     */
    public function setReturn($return)
    {
        $this->return = $return;

        return $this;
    }

    /**
     * Get return
     *
     * @return integer 
     */
    public function getReturn()
    {
        return $this->return;
    }

    /**
     * Set return_currency
     *
     * @param string $returnCurrency
     * @return Goods
     */
    public function setReturnCurrency($returnCurrency)
    {
        $this->return_currency = $returnCurrency;

        return $this;
    }

    /**
     * Get return_currency
     *
     * @return string 
     */
    public function getReturnCurrency()
    {
        return $this->return_currency;
    }

    /**
     * Set return_unit
     *
     * @param string $returnUnit
     * @return Goods
     */
    public function setReturnUnit($returnUnit)
    {
        $this->return_unit = $returnUnit;

        return $this;
    }

    /**
     * Get return_unit
     *
     * @return string 
     */
    public function getReturnUnit()
    {
        return $this->return_unit;
    }

    /**
     * Set contribution
     *
     * @param integer $contribution
     * @return Goods
     */
    public function setContribution($contribution)
    {
        $this->contribution = $contribution;

        return $this;
    }

    /**
     * Get contribution
     *
     * @return integer 
     */
    public function getContribution()
    {
        return $this->contribution;
    }

    /**
     * Set contribution_currency
     *
     * @param string $contributionCurrency
     * @return Goods
     */
    public function setContributionCurrency($contributionCurrency)
    {
        $this->contribution_currency = $contributionCurrency;

        return $this;
    }

    /**
     * Get contribution_currency
     *
     * @return string 
     */
    public function getContributionCurrency()
    {
        return $this->contribution_currency;
    }

    /**
     * Set contribution_unit
     *
     * @param string $contributionUnit
     * @return Goods
     */
    public function setContributionUnit($contributionUnit)
    {
        $this->contribution_unit = $contributionUnit;

        return $this;
    }

    /**
     * Get contribution_unit
     *
     * @return string 
     */
    public function getContributionUnit()
    {
        return $this->contribution_unit;
    }

    /**
     * Set contribution_account
     *
     * @param string $contributionAccount
     * @return Goods
     */
    public function setContributionAccount($contributionAccount)
    {
        $this->contribution_account = $contributionAccount;

        return $this;
    }

    /**
     * Get contribution_account
     *
     * @return string 
     */
    public function getContributionAccount()
    {
        return $this->contribution_account;
    }

    /**
     * Set descriptions
     *
     * @param string $descriptions
     * @return Goods
     */
    public function setDescriptions($descriptions)
    {
        $this->descriptions = $descriptions;

        return $this;
    }

    /**
     * Get descriptions
     *
     * @return string 
     */
    public function getDescriptions()
    {
        return $this->descriptions;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Goods
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Goods
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set district
     *
     * @param string $district
     * @return Goods
     */
    public function setDistrict($district)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return string 
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set settlement
     *
     * @param string $settlement
     * @return Goods
     */
    public function setSettlement($settlement)
    {
        $this->settlement = $settlement;

        return $this;
    }

    /**
     * Get settlement
     *
     * @return string 
     */
    public function getSettlement()
    {
        return $this->settlement;
    }

    /**
     * Set filter
     *
     * @param string $filter
     * @return Goods
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * Get filter
     *
     * @return string 
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * Set brand
     *
     * @param string $brand
     * @return Goods
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return string 
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set brand_country
     *
     * @param string $brandCountry
     * @return Goods
     */
    public function setBrandCountry($brandCountry)
    {
        $this->brand_country = $brandCountry;

        return $this;
    }

    /**
     * Get brand_country
     *
     * @return string 
     */
    public function getBrandCountry()
    {
        return $this->brand_country;
    }

    /**
     * Set goods_code
     *
     * @param string $goodsCode
     * @return Goods
     */
    public function setGoodsCode($goodsCode)
    {
        $this->goods_code = $goodsCode;

        return $this;
    }

    /**
     * Get goods_code
     *
     * @return string 
     */
    public function getGoodsCode()
    {
        return $this->goods_code;
    }

    /**
     * Set goods_url
     *
     * @param string $goodsUrl
     * @return Goods
     */
    public function setGoodsUrl($goodsUrl)
    {
        $this->goods_url = $goodsUrl;

        return $this;
    }

    /**
     * Get goods_url
     *
     * @return string 
     */
    public function getGoodsUrl()
    {
        return $this->goods_url;
    }

    /**
     * Set frequancy_volume
     *
     * @param string $frequancyVolume
     * @return Goods
     */
    public function setFrequancyVolume($frequancyVolume)
    {
        $this->frequancy_volume = $frequancyVolume;

        return $this;
    }

    /**
     * Get frequancy_volume
     *
     * @return string 
     */
    public function getFrequancyVolume()
    {
        return $this->frequancy_volume;
    }

    /**
     * Set single
     *
     * @param integer $single
     * @return Goods
     */
    public function setSingle($single)
    {
        $this->single = $single;

        return $this;
    }

    /**
     * Get single
     *
     * @return integer 
     */
    public function getSingle()
    {
        return $this->single;
    }

    /**
     * Set pickup
     *
     * @param string $pickup
     * @return Goods
     */
    public function setPickup($pickup)
    {
        $this->pickup = $pickup;

        return $this;
    }

    /**
     * Get pickup
     *
     * @return string 
     */
    public function getPickup()
    {
        return $this->pickup;
    }

    /**
     * Set courier
     *
     * @param integer $courier
     * @return Goods
     */
    public function setCourier($courier)
    {
        $this->courier = $courier;

        return $this;
    }

    /**
     * Get courier
     *
     * @return integer 
     */
    public function getCourier()
    {
        return $this->courier;
    }

    /**
     * Set transportation
     *
     * @param string $transportation
     * @return Goods
     */
    public function setTransportation($transportation)
    {
        $this->transportation = $transportation;

        return $this;
    }

    /**
     * Get transportation
     *
     * @return string 
     */
    public function getTransportation()
    {
        return $this->transportation;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Goods
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return Goods
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }


    /**
     * @var Acme\GoodsBundle\Entity\GoodsFile GoodsFile
     */
    private $goodsFile;
    /**
     * Get goodsFile
     *
     * @return Acme\GoodsBundle\Entity\GoodsFile
     */
    public function getGoodsFile()
    {
	return $this->goodsFile;
    }
    /**
     * Set goodsFile
     *
     * @param Acme\GoodsBundle\Entity\GoodsFile
     */
    public function setGoodsFile(\Acme\GoodsBundle\Entity\GoodsFile $goodsFile)
    {
	$this->goodsFile = $goodsFile;
    }

    public function __toString()
    {
	return (string) $this->id;
    }
}
