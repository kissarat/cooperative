<?php

namespace Acme\GoodsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GoodsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id_u')
            ->add('type')
            ->add('title')
            ->add('ua_code')
            ->add('category')
            ->add('avail')
            ->add('volume')
            ->add('price_min')
            ->add('price_max')
            ->add('price_currency')
            ->add('price_unit')
            ->add('return')
            ->add('return_currency')
            ->add('return_unit')
            ->add('contribution')
            ->add('contribution_currency')
            ->add('contribution_unit')
            ->add('contribution_account')
            ->add('descriptions')
            ->add('country')
            ->add('region')
            ->add('district')
            ->add('settlement')
            ->add('filter')
            ->add('brand')
            ->add('brand_country')
            ->add('goods_code')
            ->add('goods_url')
            ->add('frequancy_volume')
            ->add('single')
            ->add('pickup')
            ->add('courier')
            ->add('transportation')
            ->add('status')
            ->add('creation')
            ->add('file','collection', array('type' => new GoodsFileType(), 'allow_add' => true))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\GoodsBundle\Entity\Goods'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'acme_goodsbundle_goods';
    }
}
