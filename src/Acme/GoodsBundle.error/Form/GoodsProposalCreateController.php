<?php
namespace Acme\GoodsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\GoodsBundle\Entity\Goods;
use Acme\GoodsBundle\Entity\GoodsFile;
use Acme\GoodsBundle\Form\GoodsType;

class GoodsProposalCreateController extends Controller
{

    private $GoodsFile;

    public function __construct()
    {
	$this->GoodsFile = new \Acme\GoodsBundle\Entity\GoodsFile();
	$this->GoodsFile->setNoFile(10);
    }

    public function createAction(Request $request)
    {
    
	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$entity  = new Goods();
	$request = $this->getRequest();
	$form    = $this->createForm(new GoodsType($this->GoodsFile->getNoFile()), $entity);
//	$form->bindRequest($request);

	return $this->render('AcmeGoodsBundle:Default:goods_proposal_create.html.twig', array('entity' => $entity, 'form' => $form->createView(),'id'=>$userId,));

    }
}