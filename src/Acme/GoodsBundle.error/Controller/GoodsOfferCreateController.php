<?php
namespace Acme\GoodsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\GoodsBundle\Entity\Goods;
#use Acme\GoodsBundle\Entity\GoodsForm;

class GoodsOfferCreateController extends Controller
{
    public function createAction(Request $request)
    {

	$newGoods = new Goods();
	$newGoods->setTitle("");
	/*
	$newGoods->setUa_code("");
	$newGoods->setCategory("");
	$newGoods->setAvail("");
	$newGoods->setVolume("");
	$newGoods->setPrice_min("");
	$newGoods->setPrice_max("");
	$newGoods->setPrice_currency("");
	$newGoods->setPrice_unit("");
	$newGoods->setReturn("");
	$newGoods->setReturn_currency("");
	$newGoods->setReturn_unit("");
	$newGoods->setContribution("");
	$newGoods->setContribution_currency("");
	$newGoods->setContribution_unit("");
	$newGoods->setContribution_account("");
	$newGoods->setDescriptions("");
	$newGoods->setCountry("");
	$newGoods->setRegion("");
	$newGoods->setDistrict("");
	$newGoods->setSettlement("");
	$newGoods->setPhoto("");
	$newGoods->setAddition("");
	$newGoods->setFilter("");
	$newGoods->setBrand("");
	$newGoods->setBrand_country("");
	$newGoods->setGoods_code("");
	$newGoods->setGoods_url("");
	$newGoods->setFrequancy_volume("");
	$newGoods->setSingle("");
	$newGoods->setPickup("");
	$newGoods->setCourier("");
	$newGoods->setTransportation("");
	$newGoods->setStatus("");
	*/


	$form = $this->createFormBuilder($newGoods)
			->add('title', 'text')
			->add('ua_code', 'text')
			->add('category', 'hidden')
			->add('avail', 'choice', array('choices' => array('0' => 'в наявності','1' => 'немає в наявності','2' => 'під замовлення')))
			->add('volume', 'choice', array('choices' => array('0' => 'в роздріб','1' => 'оптом та в роздріб','2' => 'оптом')))
			->add('price_min', 'text')
			->add('price_max', 'text')
			->add('price_currency', 'choice', array('choices' => array('0' => 'ГРН','1' => 'USD'),'label' => false))
			->add('price_unit', 'choice', array('choices' => array('0' => 'ШТ','1' => 'ОД','2' => 'Л','3' => 'УП'),'label' => false))
			->add('return', 'text')
			->add('return_currency', 'choice', array('choices' => array('0' => 'ГРН','1' => 'USD'),'label' => false))
			->add('return_unit', 'choice', array('choices' => array('0' => 'ШТ','1' => 'ОД','2' => 'Л','3' => 'УП'),'label' => false))
			->add('contribution', 'text')
			->add('contribution_currency', 'choice', array('choices' => array('0' => 'ГРН','1' => 'USD'),'label' => false))
			->add('contribution_unit', 'choice', array('choices' => array('0' => 'ШТ','1' => 'ОД','2' => 'Л','3' => 'УП'),'label' => false))
			->add('contribution_account', 'textarea')
			->add('descriptions', 'textarea')
			->add('country', 'country')
			->add('region', 'text')
			->add('district', 'text')
			->add('settlement', 'text')
			->add('filter', 'text')
			->add('brand', 'text')
			->add('brand_country', 'country')
			->add('goods_code', 'text')
			->add('goods_url', 'url')
			->add('frequancy_volume', 'text')
			->add('single', 'text')
			->add('pickup', 'text')
			->add('courier', 'text')
			->add('transportation', 'text')
			->add('status', 'text')
			->getForm();

	$form->handleRequest($request);
	return $this->render('AcmeGoodsBundle:Default:goods_offer_create.html.twig', array('form' => $form->createView(),));

    }
}