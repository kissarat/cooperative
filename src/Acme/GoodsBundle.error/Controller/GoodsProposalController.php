<?php

namespace Acme\GoodsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\GoodsBundle\Entity\Catalog;
use Acme\GoodsBundle\Entity\Goods;

class GoodsProposalController extends Controller
{
    public function indexAction($id=null)
    {
	// SELECT PATH
	$em = $this->getDoctrine()->getManager();
	$conn = $em->getConnection();
	$tree = $conn->prepare("SELECT * FROM getTree($id)");
	$tree->execute();
	$path = $tree->fetchAll();

	// SELECT GOODS CATEGORY
	$Catalog = new Catalog();
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
		    'SELECT catalog FROM AcmeGoodsBundle:Catalog catalog WHERE catalog.pid = :pid'
			)->setParameters(array(
				'pid' => $id,
				));
	$catalog = $query->getResult();
	
	// SELECT GOODS
	$goods = array();
	$Goods = new Goods();
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
		    'SELECT goods FROM AcmeGoodsBundle:Goods goods WHERE goods.category =:category AND goods.type = :type'
			)->setParameters(array(
				'category' => $id,
				'type' => 1,
				));
	$goods = $query->getResult();

	
	//if (!$goods) {throw $this->createNotFoundException('No users found!');}
	//else {
	    return $this->render('AcmeGoodsBundle:Default:goods_proposal_view.html.twig', array('id'=>$id,'path' => $path, 'catalog' => $catalog, 'goods' => $goods));
	//    }

    }
}
