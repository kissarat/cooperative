<?php

namespace Acme\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\OrganizationBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="discussion_question_rating")
*/

class QuestionRating
{
    /**
    * @ORM\Id
    * @ORM\Column(name="id_dscqr", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id_dscqr;

    /**
    * @ORM\Column(name="id_dscq", type="integer")
    */
    protected $id_dscq;
    
    /**
    * @ORM\Column(name="id_u", type="integer")
    */
    protected $id_u;

    /**
    * @ORM\Column(name="rating", type="integer")
    */
    protected $rating;

    /**
    * @ORM\Column(name="creation", type="datetime")
    */
    protected $creation;


    /**
     * Get id_dscqr
     *
     * @return integer 
     */
    public function getIdDscqr()
    {
        return $this->id_dscqr;
    }

    /**
     * Set id_dscq
     *
     * @param integer $idDscq
     * @return QuestionRating
     */
    public function setIdDscq($idDscq)
    {
        $this->id_dscq = $idDscq;

        return $this;
    }

    /**
     * Get id_dscq
     *
     * @return integer 
     */
    public function getIdDscq()
    {
        return $this->id_dscq;
    }

    /**
     * Set id_u
     *
     * @param integer $idU
     * @return QuestionRating
     */
    public function setIdU($idU)
    {
        $this->id_u = $idU;

        return $this;
    }

    /**
     * Get id_u
     *
     * @return integer 
     */
    public function getIdU()
    {
        return $this->id_u;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return QuestionRating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return QuestionRating
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }
}
