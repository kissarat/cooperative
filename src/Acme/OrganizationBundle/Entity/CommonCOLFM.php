<?php

namespace Acme\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
* Acme\OrganizationBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="common.colfm")
*/

class CommonCOLFM
{
    /**
    * @ORM\Id
    * @ORM\Column(name="colfm_id", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $colfm_id;

    /**
    * @ORM\Column(name="code", type="string")
    */
    protected $code;
    
    /**
    * @ORM\Column(name="title", type="string")
    */
    protected $title;

    /**
    * @ORM\Column(name="description", type="text")
    */
    protected $description;

    /**
    * @ORM\Column(name="can_modify", type="boolean")
    */
    protected $can_modify;


    /**
     * Get colfm_id
     *
     * @return integer 
     */
    public function getColfmId()
    {
        return $this->colfm_id;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return CommonCOLFM
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return CommonCOLFM
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return CommonCOLFM
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set can_modify
     *
     * @param boolean $canModify
     * @return CommonCOLFM
     */
    public function setCanModify($canModify)
    {
        $this->can_modify = $canModify;

        return $this;
    }

    /**
     * Get can_modify
     *
     * @return boolean 
     */
    public function getCanModify()
    {
        return $this->can_modify;
    }
}
