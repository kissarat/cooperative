<?php

namespace Acme\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
* Acme\OrganizationBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="discussion")
*/

class Discussion
{
    /**
    * @ORM\Id
    * @ORM\Column(name="id_dsc", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id_dsc;

    /**
    * @ORM\Column(name="id_org", type="integer")
    */
    protected $id_org;
    
    /**
    * @ORM\Column(name="id_u", type="integer")
    */
    protected $id_u;

    /**
    * @ORM\Column(name="type", type="integer")
    */
    protected $type;

    /**
    * @ORM\Column(name="title", type="string")
    */
    protected $title;

    /**
    * @ORM\Column(name="description", type="text")
    */
    protected $description;

    /**
    * @ORM\Column(name="begin_date", type="datetime")
    */
    protected $begin_date;
    
    /**
    * @ORM\Column(name="end_date", type="datetime")
    */
    protected $end_date;

    /**
    * @ORM\Column(name="status", type="integer")
    */
    protected $status;

    /**
    * @ORM\Column(name="visibility", type="integer")
    */
    protected $visibility;
    
    /**
    * @ORM\Column(name="creation", type="datetime")
    */
    protected $creation;


    /**
     * Get id_dsc
     *
     * @return integer 
     */
    public function getIdDsc()
    {
        return $this->id_dsc;
    }

    /**
     * Set id_org
     *
     * @param integer $idOrg
     * @return Discussion
     */
    public function setIdOrg($idOrg)
    {
        $this->id_org = $idOrg;

        return $this;
    }

    /**
     * Get id_org
     *
     * @return integer 
     */
    public function getIdOrg()
    {
        return $this->id_org;
    }

    /**
     * Set id_u
     *
     * @param integer $idU
     * @return Discussion
     */
    public function setIdU($idU)
    {
        $this->id_u = $idU;

        return $this;
    }

    /**
     * Get id_u
     *
     * @return integer 
     */
    public function getIdU()
    {
        return $this->id_u;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Discussion
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Discussion
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Discussion
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set begin_date
     *
     * @param \DateTime $beginDate
     * @return Discussion
     */
    public function setBeginDate($beginDate)
    {
        $this->begin_date = $beginDate;

        return $this;
    }

    /**
     * Get begin_date
     *
     * @return \DateTime 
     */
    public function getBeginDate()
    {
        return $this->begin_date;
    }

    /**
     * Set end_date
     *
     * @param \DateTime $endDate
     * @return Discussion
     */
    public function setEndDate($endDate)
    {
        $this->end_date = $endDate;

        return $this;
    }

    /**
     * Get end_date
     *
     * @return \DateTime 
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Discussion
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set visibility
     *
     * @param integer $visibility
     * @return Discussion
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * Get visibility
     *
     * @return integer 
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return Discussion
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }
}
