<?php

namespace Acme\OrganizationBundle\Entity;

class ApplicationLegalForm
{
    protected $legal;
    public function setLegal($legal)
    {
        $this->legal = $legal;

        return $this;
    }
    public function getLegal()
    {
        return $this->legal;
    }

}
