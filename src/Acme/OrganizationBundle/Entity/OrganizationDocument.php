<?php
namespace Acme\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
* Acme\OrganizationBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="organization_document")
*/

class OrganizationDocument
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id_org_document", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id_org_document;

    /**
     * @ORM\Column(name="id_org", type="integer")
     */
    public $id_org;
                              
    /**
     * @ORM\Column(name="description", type="string", length=255)
     * @Assert\NotBlank
     */
    public $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $path;
                                                                       
    /**
     * Get id_org_document
     *
     * @return integer 
     */
    public function getIdOrgDocument()
    {
        return $this->id_org_document;
    }

    /**
     * Set id_org
     *
     * @param integer $idOrg
     * @return OrganizationDocument
     */
    public function setIdOrg($idOrg)
    {
        $this->id_org = $idOrg;

        return $this;
    }

    /**
     * Get id_org
     *
     * @return integer 
     */
    public function getIdOrg()
    {
        return $this->id_org;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return OrganizationDocument
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**************************************************************************************/
    public function getAbsolutePath()
    {
	return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
	return null === $this->path ? null : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
	// the absolute directory path where uploaded
	// documents should be saved
	return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
	// get rid of the __DIR__ so it doesn't screw up
	// when displaying uploaded doc/image in the view.
	return 'uploads/organization_documents/'.$this->getIdorg();
    }

    /**
     * @Assert\File(
     *   maxSize="10M",
     *   mimeTypes = {
     *          "image/png",
     *          "image/jpeg",
     *          "image/jpg",
     *          "image/gif",
     *          "application/pdf",
     *          "application/x-pdf",
     *		"application/msword",
     *		"application/vnd.openxmlformats-officedocument.wordprocessingml.document"
     *          }
     *  )
     */

    private $file;
    /**
     * Sets file.
     * @param UploadedFile $file
     */

    public function setFile(UploadedFile $file = null)
    {
	$this->file = $file;
    }

    /**
     * Get file.
     * @return UploadedFile
     */

    public function getFile()
    {
	return $this->file;
    }

    public function upload($id_org)
    {
	// the file property can be empty if the field is not required
	if (null === $this->getFile())
	{
	    return;
	}
	// use the original file name here but you should
	// sanitize it at least to avoid any security issues
	// move takes the target directory and then the
	// target filename to move to
	$this->getFile()->move(
		$this->getUploadRootDir().'/'.$id_org,
		$this->getFile()->getClientOriginalName()
		);
	// set the path property to the filename where you've saved the file
	$this->path = $this->getFile()->getClientOriginalName();
	// clean up the file property as you won't need it anymore
	$this->file = null;
    }


    /**
     * Set path
     *
     * @param string $path
     * @return OrganizationDocument
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string 
     */
    public function getPath()
    {
        return $this->path;
    }
}
