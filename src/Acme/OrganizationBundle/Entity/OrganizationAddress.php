<?php

namespace Acme\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="organization_address")
*/

class OrganizationAddress
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id_org_address", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_org_address;

    /**
     * @ORM\Column(name="id_org", type="integer")
     */
    private $id_org;

    /**
     * @ORM\Column(name="address_type", type="integer")
     */
    private $address_type;

    /**
     * @ORM\Column(name="country_id", type="integer")
     */
    private $country_id;

    /**
     * @ORM\Column(name="administrative_unit_id", type="integer")
     */
    private $administrative_unit_id;

    /**
     * @ORM\Column(name="street_id", type="integer")
     */
    private $street_id;

    /**
     * @ORM\Column(name="building_id", type="integer")
     */
    private $building_id;

    /**
     * @ORM\Column(name="address", type="string")
     */
    private $address;


    /**
     * Get id_org_address
     *
     * @return integer 
     */
    public function getIdOrgAddress()
    {
        return $this->id_org_address;
    }

    /**
     * Set id_org
     *
     * @param integer $idOrg
     * @return OrganizationAddress
     */
    public function setIdOrg($idOrg)
    {
        $this->id_org = $idOrg;

        return $this;
    }

    /**
     * Get id_org
     *
     * @return integer 
     */
    public function getIdOrg()
    {
        return $this->id_org;
    }

    /**
     * Set address_type
     *
     * @param integer $addressType
     * @return OrganizationAddress
     */
    public function setAddressType($addressType)
    {
        $this->address_type = $addressType;

        return $this;
    }

    /**
     * Get address_type
     *
     * @return integer 
     */
    public function getAddressType()
    {
        return $this->address_type;
    }

    /**
     * Set country_id
     *
     * @param integer $countryId
     * @return OrganizationAddress
     */
    public function setCountryId($countryId)
    {
        $this->country_id = $countryId;

        return $this;
    }

    /**
     * Get country_id
     *
     * @return integer 
     */
    public function getCountryId()
    {
        return $this->country_id;
    }

    /**
     * Set administrative_unit_id
     *
     * @param integer $administrativeUnitId
     * @return OrganizationAddress
     */
    public function setAdministrativeUnitId($administrativeUnitId)
    {
        $this->administrative_unit_id = $administrativeUnitId;

        return $this;
    }

    /**
     * Get administrative_unit_id
     *
     * @return integer 
     */
    public function getAdministrativeUnitId()
    {
        return $this->administrative_unit_id;
    }

    /**
     * Set street_id
     *
     * @param integer $streetId
     * @return OrganizationAddress
     */
    public function setStreetId($streetId)
    {
        $this->street_id = $streetId;

        return $this;
    }

    /**
     * Get street_id
     *
     * @return integer 
     */
    public function getStreetId()
    {
        return $this->street_id;
    }

    /**
     * Set building_id
     *
     * @param integer $buildingId
     * @return OrganizationAddress
     */
    public function setBuildingId($buildingId)
    {
        $this->building_id = $buildingId;

        return $this;
    }

    /**
     * Get building_id
     *
     * @return integer 
     */
    public function getBuildingId()
    {
        return $this->building_id;
    }

    /**
     * Set address
     *
     * @param string $address
     * @return OrganizationAddress
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string 
     */
    public function getAddress()
    {
        return $this->address;
    }
}
