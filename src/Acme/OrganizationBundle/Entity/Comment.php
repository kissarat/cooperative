<?php

namespace Acme\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* Acme\OrganizationBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="discussion_comment")
*/

class Comment
{
    /**
    * @ORM\Id
    * @ORM\Column(name="id_dscc", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id_dscc;

    /**
    * @ORM\Column(name="id_dscq", type="integer")
    */
    protected $id_dscq;
    
    /**
    * @ORM\Column(name="id_u", type="integer")
    */
    protected $id_u;

    /**
    * @ORM\Column(name="comment", type="text", nullable=FALSE)
    */
    protected $comment;

    /**
    * @ORM\Column(name="pid", type="integer")
    */
    protected $pid;

    /**
    * @ORM\Column(name="visibility", type="integer")
    */
    protected $visibility;
    
    /**
    * @ORM\Column(name="creation", type="datetime")
    */
    protected $creation;


    /**
     * Get id_dscc
     *
     * @return integer 
     */
    public function getIdDscc()
    {
        return $this->id_dscc;
    }

    /**
     * Set id_dscq
     *
     * @param integer $idDscq
     * @return Comment
     */
    public function setIdDscq($idDscq)
    {
        $this->id_dscq = $idDscq;

        return $this;
    }

    /**
     * Get id_dscq
     *
     * @return integer 
     */
    public function getIdDscq()
    {
        return $this->id_dscq;
    }

    /**
     * Set id_u
     *
     * @param integer $idU
     * @return Comment
     */
    public function setIdU($idU)
    {
        $this->id_u = $idU;

        return $this;
    }

    /**
     * Get id_u
     *
     * @return integer 
     */
    public function getIdU()
    {
        return $this->id_u;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set pid
     *
     * @param integer $pid
     * @return Comment
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Get pid
     *
     * @return integer 
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Set visibility
     *
     * @param integer $visibility
     * @return Comment
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * Get visibility
     *
     * @return integer 
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return Comment
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }
}
