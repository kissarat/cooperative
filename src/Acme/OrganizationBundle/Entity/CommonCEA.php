<?php

namespace Acme\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
* Acme\OrganizationBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="common.cea")
*/

class CommonCEA
{
    /**
    * @ORM\Id
    * @ORM\Column(name="cea_id", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $cea_id;

    /**
    * @ORM\Column(name="parent_id", type="integer")
    */
    protected $parent_id;
    
    /**
    * @ORM\Column(name="section", type="string")
    */
    protected $section;

    /**
    * @ORM\Column(name="code", type="string")
    */
    protected $code;

    /**
    * @ORM\Column(name="title", type="text")
    */
    protected $title;

    /**
    * @ORM\Column(name="modified", type="date")
    */
    protected $modified;

    /**
    * @ORM\Column(name="can_modify", type="boolean")
    */
    protected $can_modify;

    /**
    * @ORM\Column(name="comment", type="text")
    */
    protected $comment;


    /**
     * Get cea_id
     *
     * @return integer 
     */
    public function getCeaId()
    {
        return $this->cea_id;
    }

    /**
     * Set parent_id
     *
     * @param integer $parentId
     * @return CommonCEA
     */
    public function setParentId($parentId)
    {
        $this->parent_id = $parentId;

        return $this;
    }

    /**
     * Get parent_id
     *
     * @return integer 
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Set section
     *
     * @param string $section
     * @return CommonCEA
     */
    public function setSection($section)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return string 
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return CommonCEA
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return CommonCEA
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return CommonCEA
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime 
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set can_modify
     *
     * @param boolean $canModify
     * @return CommonCEA
     */
    public function setCanModify($canModify)
    {
        $this->can_modify = $canModify;

        return $this;
    }

    /**
     * Get can_modify
     *
     * @return boolean 
     */
    public function getCanModify()
    {
        return $this->can_modify;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return CommonCEA
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }
}
