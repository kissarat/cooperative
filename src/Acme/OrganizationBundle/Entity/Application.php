<?php

namespace Acme\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
* Acme\OrganizationBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="organization_application")
*/

class Application
{
    /**
    * @ORM\Id
    * @ORM\Column(name="id_oa", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id_oa;

    /**
    * @ORM\Column(name="id_org", type="integer")
    */
    protected $id_org;
    
    /**
    * @ORM\Column(name="entry_id_u", type="integer")
    */
    protected $entry_id_u;

    /**
    * @ORM\Column(name="entry_id_org", type="integer")
    */
    protected $entry_id_org;

    /**
    * @ORM\Column(name="from_user", type="integer")
    */
    protected $from_user;

    /**
    * @ORM\Column(name="valid", type="integer")
    */
    protected $valid;

    /**
    * @ORM\Column(name="associated", type="integer")
    */
    protected $associated;

    /**
    * @ORM\Column(name="bank", type="integer")
    */
    protected $bank;

    /**
    * @ORM\Column(name="answer", type="string")
    */
    protected $answer;

    /**
    * @ORM\Column(name="viewed", type="integer")
    */
    protected $viewed;

    /**
    * @ORM\Column(name="confirmation", type="integer")
    */
    protected $confirmation;

    /**
    * @ORM\Column(name="creation", type="datetime")
    */
    protected $creation;

    /**
    * @ORM\Column(name="last_modify", type="datetime")
    */
    protected $last_modify;


    /**
     * Get id_oa
     *
     * @return integer 
     */
    public function getIdOa()
    {
        return $this->id_oa;
    }

    /**
     * Set id_org
     *
     * @param integer $idOrg
     * @return Application
     */
    public function setIdOrg($idOrg)
    {
        $this->id_org = $idOrg;

        return $this;
    }

    /**
     * Get id_org
     *
     * @return integer 
     */
    public function getIdOrg()
    {
        return $this->id_org;
    }

    /**
     * Set entry_id_u
     *
     * @param integer $entryIdU
     * @return Application
     */
    public function setEntryIdU($entryIdU)
    {
        $this->entry_id_u = $entryIdU;

        return $this;
    }

    /**
     * Get entry_id_u
     *
     * @return integer 
     */
    public function getEntryIdU()
    {
        return $this->entry_id_u;
    }

    /**
     * Set entry_id_org
     *
     * @param integer $entryIdOrg
     * @return Application
     */
    public function setEntryIdOrg($entryIdOrg)
    {
        $this->entry_id_org = $entryIdOrg;

        return $this;
    }

    /**
     * Get entry_id_org
     *
     * @return integer 
     */
    public function getEntryIdOrg()
    {
        return $this->entry_id_org;
    }

    /**
     * Set from_user
     *
     * @param integer $fromUser
     * @return Application
     */
    public function setFromUser($fromUser)
    {
        $this->from_user = $fromUser;

        return $this;
    }

    /**
     * Get from_user
     *
     * @return integer 
     */
    public function getFromUser()
    {
        return $this->from_user;
    }

    /**
     * Set valid
     *
     * @param integer $valid
     * @return Application
     */
    public function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * Get valid
     *
     * @return integer 
     */
    public function getValid()
    {
        return $this->valid;
    }

    /**
     * Set associated
     *
     * @param integer $associated
     * @return Application
     */
    public function setAssociated($associated)
    {
        $this->associated = $associated;

        return $this;
    }

    /**
     * Get associated
     *
     * @return integer 
     */
    public function getAssociated()
    {
        return $this->associated;
    }

    /**
     * Set bank
     *
     * @param integer $bank
     * @return Application
     */
    public function setBank($bank)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get bank
     *
     * @return integer 
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return Application
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set viewed
     *
     * @param integer $viewed
     * @return Application
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;

        return $this;
    }

    /**
     * Get viewed
     *
     * @return integer 
     */
    public function getViewed()
    {
        return $this->viewed;
    }

    /**
     * Set confirmation
     *
     * @param integer $confirmation
     * @return Application
     */
    public function setConfirmation($confirmation)
    {
        $this->confirmation = $confirmation;

        return $this;
    }

    /**
     * Get confirmation
     *
     * @return integer 
     */
    public function getConfirmation()
    {
        return $this->confirmation;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return Application
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }

    /**
     * Set last_modify
     *
     * @param \DateTime $lastModify
     * @return Application
     */
    public function setLastModify($lastModify)
    {
        $this->last_modify = $lastModify;

        return $this;
    }

    /**
     * Get last_modify
     *
     * @return \DateTime 
     */
    public function getLastModify()
    {
        return $this->last_modify;
    }


    protected $org;
    public function setOrg($org)
    {
        $this->org = $org;
        return $this;
    }
    public function getOrg()
    {
        return $this->org;
    }
                                                    
}
