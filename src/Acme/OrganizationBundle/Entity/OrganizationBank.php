<?php

namespace Acme\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="organization_bank")
*/

class OrganizationBank
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id_org_bank", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_org_bank;

    /**
     * @ORM\Column(name="id_org", type="integer")
     */
    private $id_org;

    /**
     * @ORM\Column(name="title", type="string")
     */
    private $title;

    /**
     * @ORM\Column(name="mfo", type="string")
     */
    private $mfo;

    /**
     * @ORM\Column(name="code", type="string")
     */
    private $code;

    /**
     * @ORM\Column(name="rr", type="string")
     */
    private $rr;

    /**
     * @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
    * @ORM\Column(name="creation", type="datetime")
    */
    protected $creation;

    /**
     * Get id_org_bank
     *
     * @return integer 
     */
    public function getIdOrgBank()
    {
        return $this->id_org_bank;
    }

    /**
     * Set id_org
     *
     * @param integer $idOrg
     * @return OrganizationBank
     */
    public function setIdOrg($idOrg)
    {
        $this->id_org = $idOrg;

        return $this;
    }

    /**
     * Get id_org
     *
     * @return integer 
     */
    public function getIdOrg()
    {
        return $this->id_org;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return OrganizationBank
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set mfo
     *
     * @param string $mfo
     * @return OrganizationBank
     */
    public function setMfo($mfo)
    {
        $this->mfo = $mfo;

        return $this;
    }

    /**
     * Get mfo
     *
     * @return string 
     */
    public function getMfo()
    {
        return $this->mfo;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return OrganizationBank
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set rr
     *
     * @param string $rr
     * @return OrganizationBank
     */
    public function setRr($rr)
    {
        $this->rr = $rr;

        return $this;
    }

    /**
     * Get rr
     *
     * @return string 
     */
    public function getRr()
    {
        return $this->rr;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return OrganizationBank
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return OrganizationBank
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }
}
