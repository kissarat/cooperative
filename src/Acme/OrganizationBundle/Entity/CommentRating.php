<?php

namespace Acme\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* Acme\OrganizationBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="discussion_comment_rating")
*/

class CommentRating
{
    /**
    * @ORM\Id
    * @ORM\Column(name="id_dsccr", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id_dsccr;

    /**
    * @ORM\Column(name="id_dscc", type="integer")
    */
    protected $id_dscc;
    
    /**
    * @ORM\Column(name="id_u", type="integer")
    */
    protected $id_u;

    /**
    * @ORM\Column(name="rating", type="integer")
    */
    protected $rating;

    /**
    * @ORM\Column(name="creation", type="datetime")
    */
    protected $creation;


    /**
     * Get id_dsccr
     *
     * @return integer 
     */
    public function getIdDsccr()
    {
        return $this->id_dsccr;
    }

    /**
     * Set id_dscc
     *
     * @param integer $idDscc
     * @return CommentRating
     */
    public function setIdDscc($idDscc)
    {
        $this->id_dscc = $idDscc;

        return $this;
    }

    /**
     * Get id_dscc
     *
     * @return integer 
     */
    public function getIdDscc()
    {
        return $this->id_dscc;
    }

    /**
     * Set id_u
     *
     * @param integer $idU
     * @return CommentRating
     */
    public function setIdU($idU)
    {
        $this->id_u = $idU;

        return $this;
    }

    /**
     * Get id_u
     *
     * @return integer 
     */
    public function getIdU()
    {
        return $this->id_u;
    }

    /**
     * Set rating
     *
     * @param integer $rating
     * @return CommentRating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return CommentRating
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }
}
