<?php

namespace Acme\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
* Acme\OrganizationBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="organization_users_allocate")
*/

class Allocate
{
    /**
    * @ORM\Id
    * @ORM\Column(name="id_oua", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id_oua;

    /**
    * @ORM\Column(name="id_org", type="integer")
    */
    protected $id_org;
    
    /**
    * @ORM\Column(name="id_u", type="integer")
    */
    protected $id_u;

    /**
    * @ORM\Column(name="position", type="integer")
    */
    protected $position;

    /**
    * @ORM\Column(name="locked", type="integer")
    */
    protected $locked;

    
    /**
    * @ORM\Column(name="creation", type="datetime")
    */
    protected $creation;


    /**
     * Get id_oua
     *
     * @return integer 
     */
    public function getIdOua()
    {
        return $this->id_oua;
    }

    /**
     * Set id_org
     *
     * @param integer $idOrg
     * @return Allocate
     */
    public function setIdOrg($idOrg)
    {
        $this->id_org = $idOrg;

        return $this;
    }

    /**
     * Get id_org
     *
     * @return integer 
     */
    public function getIdOrg()
    {
        return $this->id_org;
    }

    /**
     * Set id_u
     *
     * @param integer $idU
     * @return Allocate
     */
    public function setIdU($idU)
    {
        $this->id_u = $idU;

        return $this;
    }

    /**
     * Get id_u
     *
     * @return integer 
     */
    public function getIdU()
    {
        return $this->id_u;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return Allocate
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set locked
     *
     * @param integer $locked
     * @return Allocate
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;

        return $this;
    }

    /**
     * Get locked
     *
     * @return integer 
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return Allocate
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }
}
