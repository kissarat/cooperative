<?php

namespace Acme\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
* Acme\OrganizationBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="organization_profile")
*/

class OrganizationProfile
{
    /**
    * @ORM\Id
    * @ORM\Column(name="id_org_profile", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id_org_profile;
    
    /**
    * @ORM\Column(name="id_org", type="integer")
    */
    protected $id_org;

    /**
    * @ORM\Column(name="title", type="string")
    */
    protected $title;

    /**
    * @ORM\Column(name="short", type="string")
    */
    protected $short;
    
    /**
    * @ORM\Column(name="description", type="text")
    */
    protected $description;
    
    /**
    * @ORM\Column(name="legal_form", type="integer")
    */
    protected $legal_form;
    
    /**
    * @ORM\Column(name="certificate_vat", type="string")
    */
    protected $certificate_vat;

    /**
    * @ORM\Column(name="itn", type="string")
    */
    protected $itn;

    /**
    * @ORM\Column(name="itn_date", type="date")
    */
    protected $itn_date;

    /**
    * @var integer
    * @ORM\Column(name="activity", type="string")
    */
    protected $activity;

    /**
    * @ORM\Column(name="visibility", type="integer")
    */
    protected $visibility;

    /**
    * @ORM\Column(name="creation", type="datetime")
    */
    protected $creation;


    /**
     * Get id_org_profile
     *
     * @return integer 
     */
    public function getIdOrgProfile()
    {
        return $this->id_org_profile;
    }

    /**
     * Set id_org
     *
     * @param integer $idOrg
     * @return OrganizationProfile
     */
    public function setIdOrg($idOrg)
    {
        $this->id_org = $idOrg;

        return $this;
    }

    /**
     * Get id_org
     *
     * @return integer 
     */
    public function getIdOrg()
    {
        return $this->id_org;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return OrganizationProfile
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set short
     *
     * @param string $short
     * @return OrganizationProfile
     */
    public function setShort($short)
    {
        $this->short = $short;

        return $this;
    }

    /**
     * Get short
     *
     * @return string 
     */
    public function getShort()
    {
        return $this->short;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return OrganizationProfile
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set legal_form
     *
     * @param integer $legalForm
     * @return OrganizationProfile
     */
    public function setLegalForm($legalForm)
    {
        $this->legal_form = $legalForm;

        return $this;
    }

    /**
     * Get legal_form
     *
     * @return integer 
     */
    public function getLegalForm()
    {
        return $this->legal_form;
    }

    /**
     * Set certificate_vat
     *
     * @param string $certificateVat
     * @return OrganizationProfile
     */
    public function setCertificateVat($certificateVat)
    {
        $this->certificate_vat = $certificateVat;

        return $this;
    }

    /**
     * Get certificate_vat
     *
     * @return string 
     */
    public function getCertificateVat()
    {
        return $this->certificate_vat;
    }

    /**
     * Set itn
     *
     * @param string $itn
     * @return OrganizationProfile
     */
    public function setItn($itn)
    {
        $this->itn = $itn;

        return $this;
    }

    /**
     * Get itn
     *
     * @return string 
     */
    public function getItn()
    {
        return $this->itn;
    }

    /**
     * Set itn_date
     *
     * @param \DateTime $itnDate
     * @return OrganizationProfile
     */
    public function setItnDate($itnDate)
    {
        $this->itn_date = $itnDate;

        return $this;
    }

    /**
     * Get itn_date
     *
     * @return \DateTime 
     */
    public function getItnDate()
    {
        return $this->itn_date;
    }

    /**
     * Set activity
     *
     * @param string $activity
     * @return OrganizationProfile
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;

        return $this;
    }

    /**
     * Get activity
     *
     * @return string 
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * Set visibility
     *
     * @param integer $visibility
     * @return OrganizationProfile
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * Get visibility
     *
     * @return integer 
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return OrganizationProfile
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }
}
