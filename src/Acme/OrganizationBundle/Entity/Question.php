<?php

namespace Acme\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
* Acme\OrganizationBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="discussion_question")
*/

class Question
{
    /**
    * @ORM\Id
    * @ORM\Column(name="id_dscq", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id_dscq;

    /**
    * @ORM\Column(name="id_dsc", type="integer")
    */
    protected $id_dsc;
    
    /**
    * @ORM\Column(name="id_u", type="integer")
    */
    protected $id_u;

    /**
    * @ORM\Column(name="title", type="string")
    */
    protected $title;

    /**
    * @ORM\Column(name="description", type="text")
    */
    protected $description;

    /**
    * @ORM\Column(name="status", type="integer")
    */
    protected $status;

    /**
    * @ORM\Column(name="creation", type="datetime")
    */
    protected $creation;


    /**
     * Get id_dscq
     *
     * @return integer 
     */
    public function getIdDscq()
    {
        return $this->id_dscq;
    }

    /**
     * Set id_dsc
     *
     * @param integer $idDsc
     * @return Question
     */
    public function setIdDsc($idDsc)
    {
        $this->id_dsc = $idDsc;

        return $this;
    }

    /**
     * Get id_dsc
     *
     * @return integer 
     */
    public function getIdDsc()
    {
        return $this->id_dsc;
    }

    /**
     * Set id_u
     *
     * @param integer $idU
     * @return Question
     */
    public function setIdU($idU)
    {
        $this->id_u = $idU;

        return $this;
    }

    /**
     * Get id_u
     *
     * @return integer 
     */
    public function getIdU()
    {
        return $this->id_u;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Question
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Question
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Question
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return Question
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }
}
