<?php

namespace Acme\OrganizationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="organization_contact")
*/

class OrganizationContact
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id_org_contact", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_org_contact;

    /**
     * @ORM\Column(name="id_org", type="integer")
     */
    private $id_org;

    /**
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
     * @ORM\Column(name="contact", type="string")
     */
    private $contact;

    /**
    * @ORM\Column(name="creation", type="datetime")
    */
    protected $creation;

    /**
     * Get id_org_contact
     *
     * @return integer 
     */
    public function getIdOrgContact()
    {
        return $this->id_org_contact;
    }

    /**
     * Set id_org
     *
     * @param integer $idOrg
     * @return OrganizationContact
     */
    public function setIdOrg($idOrg)
    {
        $this->id_org = $idOrg;

        return $this;
    }

    /**
     * Get id_org
     *
     * @return integer 
     */
    public function getIdOrg()
    {
        return $this->id_org;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return OrganizationContact
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return OrganizationContact
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set contact
     *
     * @param string $contact
     * @return OrganizationContact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return OrganizationContact
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }
}
