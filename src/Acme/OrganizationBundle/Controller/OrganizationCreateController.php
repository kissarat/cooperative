<?php
namespace Acme\OrganizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\OrganizationBundle\Entity\Organization;
use Acme\OrganizationBundle\Entity\OrganizationProfile;
use Acme\OrganizationBundle\Entity\Allocate;

class OrganizationCreateController extends Controller
{
    public function createOrganizationAction(Request $request)
    {
    
	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$newOrganization = new Organization();
	$newOrganizationProfile = new OrganizationProfile();
	
	$visibility = array('' => 'необхідно обрати', '0' => 'закрита','1' => 'відкрита');

	$form = $this->createFormBuilder($newOrganizationProfile)
		    ->add('title', 'text', array('required' => false))
		    ->add('short', 'text', array('required' => false))
		    ->add('description', 'textarea', array('required' => false))
		    ->add('legal_form', 'entity', array('class' => 'AcmeOrganizationBundle:CommonCOLFM', 'property' => 'title','required' => false))
		    ->add('certificate_vat', 'text', array('required' => false))
		    ->add('itn', 'text', array('required' => false))
		    ->add('itn_date', 'date', array('widget' => 'single_text','format' => 'yyyy-MM-dd', 'required' => false))
		    ->add('activity', 'entity', array('class' => 'AcmeOrganizationBundle:CommonCEA', 'property' => 'title', 'multiple' => 'true','required' => false))
		    ->add('visibility', 'choice', array('choices' => $visibility, 'required' => false))
		    ->add('creation', 'hidden', array('required' => false))
		    ->getForm();

	if ($request->getMethod() == 'POST')
	{
       
            $form->submit($request);
                   
    	    if ($form->isValid())
    	    {
    	    
    	    
		$title = $form->get('title')->getData();
		$short = $form->get('short')->getData();
		$description = $form->get('description')->getData();
		$legal_form = $form->get('legal_form')->getData();
		$legal_form = $legal_form->getColfmId();
		$certificate_vat = $form->get('certificate_vat')->getData();
		$itn = $form->get('itn')->getData();
		$itn_date = $form->get('itn_date')->getData();
		foreach($form->get('activity')->getData() as $a){ $activity[$a->getCeaId()] = $a->getTitle(); }
		    $activity = json_encode($activity);
		$visibility = $form->get('visibility')->getData();
		
		
		$newOrganization->setType(1);
		$newOrganization->setCreation(new \DateTime("now"));
		$em = $this->getDoctrine()->getManager();
		$em->persist($newOrganization);
		$em->flush();
		
		$orgId = $newOrganization->getIdorg();

		
		$newOrganizationProfile->setIdOrg($orgId);
		$newOrganizationProfile->setTitle($title);
                $newOrganizationProfile->setShort($short);
                $newOrganizationProfile->setDescription($description);
                $newOrganizationProfile->setLegalform($legal_form);
                $newOrganizationProfile->setCertificateVat($certificate_vat);
                $newOrganizationProfile->setItn($itn);
                $newOrganizationProfile->setItnDate($itn_date);
                $newOrganizationProfile->setActivity($activity);
                $newOrganizationProfile->setVisibility($visibility);
		$newOrganizationProfile->setCreation(new \DateTime("now"));
		$em = $this->getDoctrine()->getManager();
                $em->persist($newOrganizationProfile);
                $em->flush();
                
                $allocate = new Allocate();
                $allocate->setIdorg($orgId);
                $allocate->setIdu($userId);
                $allocate->setPosition(0);
                $allocate->setLocked(0);
                $allocate->setCreation(new \DateTime("now"));
                $em = $this->getDoctrine()->getManager();
                $em->persist($allocate);
                $em->flush();
                
                return $this->redirect($this->generateUrl('acme_organization_view', array('id' => $orgId)));
    	    }
    	}
    return $this->render('AcmeOrganizationBundle:Default:organization_create_organization.html.twig', array('form' => $form->createView(),'id'=>$userId));

    }

    public function createGroupAction(Request $request)
    {
    
	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$newOrganization = new Organization();
	$newOrganizationProfile = new OrganizationProfile();
	
	$visibility = array('' => 'необхідно обрати', '0' => 'закрита','1' => 'відкрита');

	$form = $this->createFormBuilder($newOrganizationProfile)
		    ->add('title', 'text', array('required' => false))
//		    ->add('short', 'text', array('required' => false))
		    ->add('description', 'textarea', array('required' => false))
//		    ->add('legal_form', 'entity', array('class' => 'AcmeOrganizationBundle:CommonCOLFM', 'property' => 'title','required' => false))
//		    ->add('certificate_vat', 'text', array('required' => false))
//		    ->add('itn', 'text', array('required' => false))
//		    ->add('itn_date', 'date', array('widget' => 'single_text','format' => 'yyyy-MM-dd', 'required' => false))
//		    ->add('activity', 'entity', array('class' => 'AcmeOrganizationBundle:CommonCEA', 'property' => 'title', 'multiple' => 'true','required' => false))
		    ->add('visibility', 'choice', array('choices' => $visibility, 'required' => false))
		    ->add('creation', 'hidden', array('required' => false))
		    ->getForm();

	if ($request->getMethod() == 'POST')
	{
       
            $form->submit($request);
                   
    	    if ($form->isValid())
    	    {
		$title = $form->get('title')->getData();
//		$short = $form->get('short')->getData();
		$description = $form->get('description')->getData();
//		$legal_form = $form->get('legal_form')->getData();
//		$legal_form = $legal_form->getColfmId();
//		$certificate_vat = $form->get('certificate_vat')->getData();
//		$itn = $form->get('itn')->getData();
//		$itn_date = $form->get('itn_date')->getData();
//		foreach($form->get('activity')->getData() as $a){ $activity[$a->getCeaId()] = $a->getTitle(); }
//		    $activity = json_encode($activity);
		$visibility = $form->get('visibility')->getData();
		
		
		$newOrganization->setType(0);
		$newOrganization->setCreation(new \DateTime("now"));
		$em = $this->getDoctrine()->getManager();
		$em->persist($newOrganization);
		$em->flush();
		
		$orgId = $newOrganization->getIdorg();

		
		$newOrganizationProfile->setIdorg($orgId);
		$newOrganizationProfile->setTitle($title);
//                $newOrganizationProfile->setShort($short);
                $newOrganizationProfile->setDescription($description);
//                $newOrganizationProfile->setLegalform($legal_form);
//                $newOrganizationProfile->setCertificateVat($certificate_vat);
//                $newOrganizationProfile->setItn($itn);
//                $newOrganizationProfile->setItnDate($itn_date);
//                $newOrganizationProfile->setActivity($activity);
                $newOrganizationProfile->setVisibility($visibility);
		$newOrganizationProfile->setCreation(new \DateTime("now"));
		$em = $this->getDoctrine()->getManager();
                $em->persist($newOrganizationProfile);
                $em->flush();
                
                $allocate = new Allocate();
                $allocate->setIdorg($orgId);
                $allocate->setIdu($userId);
                $allocate->setPosition(0);
                $allocate->setLocked(0);
                $allocate->setCreation(new \DateTime("now"));
                $em = $this->getDoctrine()->getManager();
                $em->persist($allocate);
                $em->flush();
                
                return $this->redirect($this->generateUrl('acme_group_view', array('id' => $orgId)));
    	    }
    	}
    return $this->render('AcmeOrganizationBundle:Default:organization_create_group.html.twig', array('form' => $form->createView(),'id'=>$userId));

    }
}