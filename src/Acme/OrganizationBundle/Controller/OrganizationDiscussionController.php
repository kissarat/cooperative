<?php
namespace Acme\OrganizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\UserBundle\Entity\Person;
use Acme\OrganizationBundle\Entity\Discussion;
use Acme\OrganizationBundle\Entity\Question;
use Acme\OrganizationBundle\Entity\DiscussionRating;
use Acme\OrganizationBundle\Entity\Comment;
use Acme\OrganizationBundle\Entity\CommentRating;

class OrganizationDiscussionController extends Controller
{
    public function createThemeAction($id_org,Request $request)
    {
    
	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$newDiscussion = new Discussion();
	
	$type = array('' => '- Необхідно обрати -', 1 => 'Необовязкове', 2 => 'Обовязкове, відкрите', 3 => 'За запрошенням');
	$status = array('' => '- Необхідно обрати -', 0 => 'Завершено', 1 => 'Продовжується');
	$visibility = array('' => '- Необхідно обрати -', 1 => 'Видимий', 0 => 'Прихований');
	
	$form = $this->createFormBuilder($newDiscussion)
		    ->add('id_org', 'hidden', array('required' => false))
		    ->add('id_u', 'hidden', array('required' => false))
		    ->add('type', 'choice', array('choices' => $type,'required' => true))
		    ->add('title', 'text', array('required' => false))
		    ->add('description', 'textarea', array('required' => false))
		    ->add('begin_date', 'datetime', array('date_widget' => 'single_text', 'time_widget' => 'single_text'))
		    ->add('end_date', 'datetime', array('date_widget' => 'single_text', 'time_widget' => 'single_text'))
		    ->add('status', 'choice', array('choices' => $status,'required' => true))
		    ->add('visibility', 'choice', array('choices' => $visibility,'required' => true))
		    ->add('creation', 'hidden', array('required' => false))
		    ->getForm();


	if ($request->getMethod() == 'POST')
	{
       
            $form->submit($request);
                   
    	    if ($form->isValid())
    	    {
		$newDiscussion->setIdorg($id_org);
		$newDiscussion->setIdu($userId);
		$newDiscussion->setType($form->get('type')->getData());
		$newDiscussion->setTitle($form->get('title')->getData());
		$newDiscussion->setDescription($form->get('description')->getData());
		$newDiscussion->setBeginDate($form->get('begin_date')->getData());
		$newDiscussion->setEndDate($form->get('end_date')->getData());
		$newDiscussion->setStatus($form->get('status')->getData());
		$newDiscussion->setVisibility($form->get('visibility')->getData());
		$newDiscussion->setCreation(new \DateTime("now"));
                                                                                                                                                                                    
                $em = $this->getDoctrine()->getManager();
                $em->persist($newDiscussion);
                $em->flush();
                
                return $this->redirect($this->generateUrl('acme_organization_view', array('id' => $id_org)));
    	    }
    	}
    return $this->render('AcmeOrganizationBundle:Default:organization_create_discussion.html.twig', array('form' => $form->createView(),'id'=>$userId,'orgId'=>$id_org));

    }

    public function viewThemeAction($id_org,$id_dsc)
    {

	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$discussion = new Discussion();
	$em = $this->getDoctrine()->getManager();

    	    $query = $em->createQuery(
                    'SELECT discussion.id_dsc,discussion.id_org,discussion.id_u,discussion.type,discussion.title,discussion.description,discussion.begin_date,discussion.end_date,discussion.status,discussion.visibility,discussion.creation,person.lastname,person.firstname FROM AcmeOrganizationBundle:Discussion discussion JOIN AcmeUserBundle:Person person WHERE discussion.id_org=:id_org AND discussion.id_u=person.user_id'
                    	    )->setParameters(array(
                        	'id_org' => $id_org,
                                ));
	    $discussion = $query->getResult();
	    return $this->render('AcmeOrganizationBundle:Default:organization_discussion_theme.html.twig', array('discussion' => $discussion,'id' => $userId,'id_org' => $id_org));
    }

    public function viewQuestionAction($id_org,$id_dsc)
    {

	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$discussion = new Discussion();
	$question = new Question();
	
	
	$em = $this->getDoctrine()->getManager();

    	    $query = $em->createQuery(
                    'SELECT discussion FROM AcmeOrganizationBundle:Discussion discussion WHERE discussion.id_org=:id_org AND discussion.id_dsc=:id_dsc'
                    	    )->setParameters(array(
                        	'id_org' => $id_org,
                        	'id_dsc' => $id_dsc
                                ));
	    $discussion = $query->getResult();

    	    $query = $em->createQuery(
                    'SELECT question.id_dscq,question.id_u,question.title,question.description,question.creation,question.status,person.lastname,person.firstname FROM AcmeOrganizationBundle:Question question JOIN AcmeUserBundle:Person person WHERE question.id_dsc=:id_dsc AND question.id_u=person.user_id'
                    	    )->setParameters(array(
                        	'id_dsc' => $id_dsc
                                ));
	    $question = $query->getResult();
	    if (!$question) { $question = array(); }

	    return $this->render('AcmeOrganizationBundle:Default:organization_discussion_question.html.twig', array('discussion' => $discussion,'question' => $question, 'id' => $userId,'id_org' => $id_org, 'id_dsc' => $id_dsc));
    }


    public function createQuestionAction($id_org,$id_dsc,Request $request)
    {
    
	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$question = new Question();

	$form = $this->createFormBuilder($question)
		    ->add('id_dsc','hidden', array('required' => false))
		    ->add('id_u','hidden', array('required' => false))
		    ->add('title','text', array('required' => false))
		    ->add('description', 'textarea', array('required' => false))
		    ->add('status', 'hidden', array('required' => false))
		    ->add('creation', 'hidden', array('required' => false))
		    ->getForm();

	if ($request->getMethod() == 'POST')
	{
            $form->submit($request);
                   
    	    if ($form->isValid())
    	    {
    		$em = $this->getDoctrine()->getManager();
		$question->setIddsc($id_dsc);
		$question->setIdu($userId);
		$question->setTitle($form->get('title')->getData());
		$question->setDescription($form->get('description')->getData());
		$question->setStatus(1);
		$question->setCreation(new \DateTime("now"));

            	$em = $this->getDoctrine()->getManager();
            	$em->persist($question);
            	$em->flush();
                
                return $this->redirect($this->generateUrl('acme_organization_discussion_theme_question', array('id_org' => $id_org, 'id_dsc' => $id_dsc)));

    	    }
    	}
	return $this->render('AcmeOrganizationBundle:Default:organization_discussion_question_create.html.twig', array('form' => $form->createView(), 'id' => $userId,'id_org' => $id_org, 'id_dsc' => $id_dsc));
    }

    public function viewCommentAction($id_org,$id_dsc,$id_dscq)
    {

	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$question = new Question();
	$comment = new Comment();
	
	$em = $this->getDoctrine()->getManager();

    	    $query = $em->createQuery(
                    'SELECT question FROM AcmeOrganizationBundle:Question question WHERE question.id_dsc=:id_dsc AND question.id_dscq=:id_dscq'
                    	    )->setParameters(array(
                        	'id_dsc' => $id_dsc,
                        	'id_dscq' => $id_dscq
                                ));
	    $question = $query->getResult();

    	    $query = $em->createQuery(
                    'SELECT comm.id_dscc,comm.id_u,comm.comment,comm.pid,comm.creation,person.lastname,person.firstname FROM AcmeOrganizationBundle:Comment comm JOIN AcmeUserBundle:Person person WHERE comm.id_dscq=:id_dscq AND comm.id_u=person.user_id'
                    	    )->setParameters(array(
                        	'id_dscq' => $id_dscq
                                ));
	    $comment = $query->getResult();
	    if (!$comment) { $comment = array(); }

	    return $this->render('AcmeOrganizationBundle:Default:organization_discussion_theme_question_comment.html.twig', array('question' => $question, 'comment' => $comment, 'id' => $userId,'id_org' => $id_org, 'id_dsc' => $id_dsc, 'id_dscq' => $id_dscq));
    }

    public function createCommentAction($id_org,$id_dsc,$id_dscq,Request $request)
    {
    
	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$newComment = new Comment();

	$form = $this->createFormBuilder($newComment)
		    ->add('id_dscq','hidden', array('required' => false))
		    ->add('id_u','hidden', array('required' => false))
		    ->add('comment', 'textarea', array('required' => false))
		    ->add('pid', 'hidden', array('required' => false))
		    ->add('visibility', 'hidden', array('required' => false))
		    ->add('creation', 'hidden', array('required' => false))
		    ->getForm();

	if ($request->getMethod() == 'POST')
	{
            $form->submit($request);
                   
    	    if ($form->isValid())
    	    {
    		$em = $this->getDoctrine()->getManager();
		$newComment->setIddscq($id_dscq);
		$newComment->setIdu($userId);
		$newComment->setComment($form->get('comment')->getData());
		$newComment->setPid(0);
		$newComment->setVisibility(1);
		$newComment->setCreation(new \DateTime("now"));

            	$em = $this->getDoctrine()->getManager();
            	$em->persist($newComment);
            	$em->flush();
                
                return $this->redirect($this->generateUrl('acme_organization_discussion_theme_question_comment', array('id_org' => $id_org, 'id_dsc' => $id_dsc, 'id_dscq' => $id_dscq)));

    	    }
    	}
	return $this->render('AcmeOrganizationBundle:Default:organization_discussion_comment_create.html.twig', array('form' => $form->createView(), 'id' => $userId,'id_org' => $id_org, 'id_dsc' => $id_dsc, 'id_dscq' => $id_dscq));
    }

    public function createCommentAnswerAction($id_org,$id_dsc,$id_dscq,$id_dscc,Request $request)
    {
    
	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$newComment = new Comment();
	
	$em = $this->getDoctrine()->getManager();

    	$query = $em->createQuery(
                    'SELECT comment FROM AcmeOrganizationBundle:Comment comment WHERE comment.id_dscc=:id_dscc'
                    	    )->setParameters(array(
                        	'id_dscc' => $id_dscc
                                ));
	$oldComment = $query->getSingleResult();

	$form = $this->createFormBuilder($newComment)
		    ->add('id_dscq','hidden', array('required' => false))
		    ->add('id_u','hidden', array('required' => false))
		    ->add('comment', 'textarea', array('required' => false))
		    ->add('pid', 'hidden', array('required' => false))
		    ->add('visibility', 'hidden', array('required' => false))
		    ->add('creation', 'hidden', array('required' => false))
		    ->getForm();

	if ($request->getMethod() == 'POST')
	{
            $form->submit($request);
                   
    	    if ($form->isValid())
    	    {
    		$answer	= "<comment>".$oldComment->getComment()."</comment>".$form->get('comment')->getData();
    		
		$newComment->setIddscq($id_dscq);
		$newComment->setIdu($userId);
		$newComment->setComment($answer);
		$newComment->setPid($form->get('pid')->getData());
		$newComment->setVisibility(1);
		$newComment->setCreation(new \DateTime("now"));

            	$em = $this->getDoctrine()->getManager();
            	$em->persist($newComment);
            	$em->flush();
                
                return $this->redirect($this->generateUrl('acme_organization_discussion_theme_question_comment', array('id_org' => $id_org, 'id_dsc' => $id_dsc, 'id_dscq' => $id_dscq)));

    	    }
    	}
	return $this->render('AcmeOrganizationBundle:Default:organization_discussion_comment_answer.html.twig', array('form' => $form->createView(), 'oldComment' => $oldComment, 'id' => $userId,'id_org' => $id_org, 'id_dsc' => $id_dsc, 'id_dscq' => $id_dscq, 'id_dscc' => $id_dscc));
    }

}