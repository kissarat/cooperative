<?php

namespace Acme\OrganizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\OrganizationBundle\Entity\Organization;
use Acme\OrganizationBundle\Entity\Allocate;
use Acme\UserBundle\Entity\Person;

class DefaultController extends Controller
{
    public function groupAction($id)
    {
	// SELECT GOODS
	$organization = new Organization();
	$em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("
    			SELECT
    			    organization.id_org,
    			    organization.type,
    			    op.title
    				 FROM AcmeOrganizationBundle:Organization organization
                        		    JOIN AcmeOrganizationBundle:OrganizationProfile op WITH op.id_org=organization.id_org
                            			WHERE organization.type=:type"
                            			    )->setParameters(array(
                            			        'type' => 0,
                            			        ));
	$organization = $query->getResult();

	return $this->render('AcmeOrganizationBundle:Default:group.html.twig', array('id' => $id, 'organization' => $organization));

    }
    public function organizationAction($id)
    {
	// SELECT GOODS
	$organization = new Organization();
	$em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("
    			SELECT
    			    organization.id_org,
    			    organization.type,
    			    op.title,
    			    colfm.title as colfmTitle
    				 FROM AcmeOrganizationBundle:Organization organization
                        		    JOIN AcmeOrganizationBundle:OrganizationProfile op WITH op.id_org=organization.id_org
                            		    JOIN AcmeOrganizationBundle:CommonCOLFM colfm WITH op.legal_form=colfm.colfm_id
                            		    	WHERE organization.type=:type"
                            			    )->setParameters(array(
                            			        'type' => 1,
                            			        ));
	$organization = $query->getResult();

	return $this->render('AcmeOrganizationBundle:Default:organization.html.twig', array('id' => $id, 'organization' => $organization));

    }
}
