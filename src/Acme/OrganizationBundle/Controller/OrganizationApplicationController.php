<?php
namespace Acme\OrganizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\OrganizationBundle\Entity\Application;
use Acme\OrganizationBundle\Entity\ApplicationLegalForm;
use Acme\OrganizationBundle\Entity\Allocate;
use Acme\UserBundle\Entity\Person;


class OrganizationApplicationController extends Controller
{

    public function createAction($id,Request $request)
    {
    
	$userId = $this->get('security.context')->getToken()->getUser()->getId();

        $em = $this->getDoctrine()->getManager();

	// person
        $query = $em->createQuery(
                            'SELECT person FROM AcmeUserBundle:Person person WHERE person.user_id = :user_id'
                                        )->setParameters(array(
                                            	    'user_id' => $userId,
                                                    ));
        $person = $query->getArrayResult();
	// passport
        $query = $em->createQuery(
                            'SELECT passport FROM AcmeUserBundle:PersonalInfoPassport passport WHERE passport.person_id = :person_id'
                                        )->setParameters(array(
                                            	    'person_id' => $person[0]['person_id'],
                                                    ));
        $passport = $query->getArrayResult();
	// itn
        $query = $em->createQuery(
                            'SELECT itn FROM AcmeUserBundle:PersonalInfoItn itn WHERE itn.person_id = :person_id'
                                        )->setParameters(array(
                                            	    'person_id' => $person[0]['person_id'],
                                                    ));
        $itn = $query->getArrayResult();

	// bank
        $query = $em->createQuery(
                            'SELECT bank FROM AcmeUserBundle:PersonalInfoBank bank WHERE bank.person_id = :person_id'
                                        )->setParameters(array(
                                            	    'person_id' => $person[0]['person_id'],
                                                    ));
        $bank = $query->getArrayResult();
        $userBank = array();
        foreach($bank as $b){
    	    $userBank[$b['bank_id']] = "{$b['title']}, МФО: {$b['mfo']}, КОД: {$b['code']}, Р./р.: {$b['rr']}";
    	    }

	// address
	$query = $em->createQuery(
	                   "SELECT a.address_id,country.title,coatsu.unit_name,street.title as stitle,building.building_no,a.address
	                	    FROM AcmeUserBundle:PersonalInfoAddress a
	                                    JOIN AcmeUserBundle:Country country WITH a.country_id=country.country_id
	                                    JOIN AcmeUserBundle:Coatsu coatsu WITH a.administrative_unit_id=coatsu.administrative_unit_id
	                                    JOIN AcmeUserBundle:CommonStreet street WITH a.street_id=street.street_id
	                                    JOIN AcmeUserBundle:CommonBuilding building WITH a.building_id=building.building_id
	                                	    WHERE
	                                                a.person_id=:person_id"
	                                            	    )->setParameters(array(
	                                                    	    'person_id' => $person[0]['person_id'],
	                                                            ));
	$address = $query->getResult();

	// contacts
        $query = $em->createQuery(
                            'SELECT contacts FROM AcmeUserBundle:PersonalInfoContact contacts WHERE contacts.person_id = :person_id'
                                        )->setParameters(array(
                                            	    'person_id' => $person[0]['person_id'],
                                                    ));
        $contacts = $query->getArrayResult();

	// from user
        $query = $em->createQuery(
                            'SELECT person.user_id,person.firstname,person.lastname,person.middlename FROM AcmeOrganizationBundle:Allocate allocate
                        	    JOIN AcmeUserBundle:Person person WITH allocate.id_u=person.user_id
                        		     WHERE allocate.id_org = :id_org'
                                    		    )->setParameters(array(
                                            			'id_org' => $id,
                                                		));
        $allocate = $query->getArrayResult();
        $from = array();
        foreach ($allocate as $a){
    	    $from[$a['user_id']] = $a['lastname'].' '.$a['firstname'].' '.$a['middlename'];
    	    }

	$newApplication = new Application();
	$form = $this->createFormBuilder($newApplication)
		    ->add('id_org','hidden', array('required' => false))
		    ->add('entry_id_u','hidden', array('required' => false))
		    ->add('entry_id_org','hidden', array('required' => false))
		    ->add('from_user', 'choice', array('choices' => $from, 'required' => false))
		    ->add('valid', 'number', array('required' => false))
		    ->add('associated', 'number', array('required' => false))
		    ->add('bank', 'choice', array('choices' => $userBank,'required' => false))
		    ->add('answer', 'hidden', array('required' => false))
		    ->add('viewed', 'hidden', array('required' => false))
		    ->add('confirmation','hidden', array('required' => false))
		    ->add('creation', 'hidden', array('required' => false))
		    ->add('last_modify', 'hidden', array('required' => false))
		    ->getForm();

	if ($request->getMethod() == 'POST')
	{
            $form->submit($request);
    	    if ($form->isValid())
    	    {
		$newApplication->setIdorg($id);
		$newApplication->setEntryIdu($userId);
		$newApplication->setEntryIdorg(0);
		$newApplication->setFromUser($form->get('from_user')->getData());
		$newApplication->setValid($form->get('valid')->getData());
		$newApplication->setAssociated($form->get('associated')->getData());
		$newApplication->setBank($form->get('bank')->getData());
		$newApplication->setCreation(new \DateTime("now"));
		$newApplication->setLastModify(new \DateTime("now"));
                                                                                                                                                                                    
                $em = $this->getDoctrine()->getManager();
                $em->persist($newApplication);
                $em->flush();
                
                return $this->redirect($this->generateUrl('acme_organization_view', array('id' => $id)));
    	    }
    	}
    return $this->render('AcmeOrganizationBundle:Default:organization_create_application.html.twig',
				array(
					'form' => $form->createView(),
					'id'=>$userId,
					'person'=>$person,
					'passport'=>$passport,
					'itn'=>$itn,
					'address'=>$address,
					'contacts'=>$contacts,
					'orgId'=>$id
					));

    }

    public function createLegalAction($id,Request $request)
    {
    
	$userId = $this->get('security.context')->getToken()->getUser()->getId();

        $em = $this->getDoctrine()->getManager();

	// legal
        $query = $em->createQuery('SELECT op.id_org,op.title FROM AcmeOrganizationBundle:Allocate a
                        		JOIN AcmeOrganizationBundle:Organization o WITH a.id_org=o.id_org AND o.type = :type
                        		JOIN AcmeOrganizationBundle:OrganizationProfile op WITH o.id_org=op.id_org
                        			    WHERE a.id_u = :user_id AND a.position = :position'
                                    			    )->setParameters(array(
                                            			    'user_id' => $userId,
                                            			    'position' => 0,
                                            			    'type' => 1
                                                		    ));
        $legal = $query->getArrayResult();
        $userLegal = array(); foreach($legal as $l){ $userLegal[$l['id_org']] = $l['title']; }

	$newApplicationLegalForm = new ApplicationLegalForm();
	//$form = $this->createFormBuilder($newApplicationLegalForm)
	$form1 = $this->get('form.factory')->createNamedBuilder('form1name', 'form', $newApplicationLegalForm)
		    ->add('legal', 'choice', array('choices' => $userLegal, 'required' => false))
		    ->getForm();

	if ($request->getMethod() == 'POST')
	{
	    if ($request->request->has('form2name'))
	    {
		$data = $request->request->all();
		$org = $data['form2name']['org'];
		$from_user = $data['form2name']['from_user'];
		$valid = $data['form2name']['valid'];
		$associated = $data['form2name']['associated'];
		$bank = $data['form2name']['bank'];

		$newApplication2 = new Application();
		$newApplication2->setIdOrg($id);
		$newApplication2->setEntryIdu($userId);
		$newApplication2->setEntryIdorg($org);
		$newApplication2->setFromUser($from_user);
		$newApplication2->setValid($valid);
		$newApplication2->setAssociated($associated);
		$newApplication2->setBank($bank);
		$newApplication2->setCreation(new \DateTime("now"));
		$newApplication2->setLastModify(new \DateTime("now"));

        	$em = $this->getDoctrine()->getManager();
    		$em->persist($newApplication2);
        	$em->flush();

        	return $this->redirect($this->generateUrl('acme_organization_view', array('id' => $id)));

	    
//    		$form2->submit($request);
//		echo "|".$form2->get('bank')->getData()."|";
//    		if ($form2name->isValid())
//    		{
//		    $newApplication2 = new Application();
//		    $newApplication2->setIdOrg($id);
//		    $newApplication2->setEntryIdu($userId);
//		    $newApplication2->setEntryIdorg($form2->get('org')->getData());
//		    $newApplication2->setFromUser($form2->get('from_user')->getData());
//		    $newApplication2->setValid($form2->get('valid')->getData());
//		    $newApplication2->setAssociated($form2->get('associated')->getData());
//		    $newApplication2->setBank($form2->get('bank')->getData());
//		    $newApplication2->setCreation(new \DateTime("now"));
//		    $newApplication2->setLastModify(new \DateTime("now"));
//        	    $em = $this->getDoctrine()->getManager();
//    		    $em->persist($newApplication2);
//        	    $em->flush();
//        	    return $this->redirect($this->generateUrl('acme_organization_view', array('id' => $id)));
//    		}
	    }

	    if ($request->request->has('form1name'))
	    {
    		$form1->submit($request);
    		if ($form1->isValid())
    		{
		    $query = $em->createQuery("SELECT op.title,op.short,op.description,colfm.title as title_colfm,colfm.code as code_colfm,op.certificate_vat,op.itn,op.itn_date,op.activity FROM AcmeOrganizationBundle:OrganizationProfile op
							JOIN AcmeOrganizationBundle:CommonCOLFM colfm WITH op.legal_form=colfm.colfm_id
							    WHERE op.id_org=:id_org"
								)->setParameters(array(
								    'id_org' => $form1->get('legal')->getData(),
								    ));
		    $organization = $query->getResult();
		    $activity = json_decode($organization[0]['activity'],true);
		    $query = $em->createQuery("SELECT a.id_org_address,a.address_type,country.title,coatsu.unit_name,street.title as stitle,building.building_no,a.address FROM AcmeOrganizationBundle:OrganizationAddress a
							JOIN AcmeUserBundle:Country country WITH a.country_id=country.country_id
							JOIN AcmeUserBundle:Coatsu coatsu WITH a.administrative_unit_id=coatsu.administrative_unit_id
							JOIN AcmeUserBundle:CommonStreet street WITH a.street_id=street.street_id
							JOIN AcmeUserBundle:CommonBuilding building WITH a.building_id=building.building_id
							    WHERE
								a.id_org=:id_org"
								    )->setParameters(array(
									'id_org' => $form1->get('legal')->getData(),
									));
		    $address = $query->getResult();
		    $query = $em->createQuery("SELECT contact FROM AcmeOrganizationBundle:OrganizationContact contact
							WHERE contact.id_org=:id_org"
							    )->setParameters(array(
								'id_org' => $form1->get('legal')->getData(),
								));
		    $contact = $query->getResult();
		    $query = $em->createQuery("SELECT bank FROM AcmeOrganizationBundle:OrganizationBank bank
							WHERE bank.id_org=:id_org"
							    )->setParameters(array(
								'id_org' => $form1->get('legal')->getData(),
								));
		    $bank = $query->getArrayResult();
    		    $userBank = array(); foreach($bank as $b){ $userBank[$b['id_org_bank']] = "{$b['title']}, МФО: {$b['mfo']}, КОД: {$b['code']}, Р./р.: {$b['rr']}"; }
		    // from user
    		    $query = $em->createQuery("SELECT person.user_id,person.firstname,person.lastname,person.middlename FROM AcmeOrganizationBundle:Allocate allocate
    							JOIN AcmeUserBundle:Person person WITH allocate.id_u=person.user_id
    							    WHERE allocate.id_org = :id_org"
    								)->setParameters(array(
    								    'id_org' => $id,
    								    ));
    		    $parentUser = $query->getArrayResult();
    		    $from = array(); foreach ($parentUser as $a){ $from[$a['user_id']] = $a['lastname'].' '.$a['firstname'].' '.$a['middlename']; }
    		    $newApplication = new Application();
		    //$form2 = $this->createFormBuilder($newApplication)
		    $form2 = $this->get('form.factory')->createNamedBuilder('form2name', 'form', $newApplication)
			->add('from_user', 'choice', array('choices' => $from, 'required' => false))
			->add('valid', 'number', array('required' => false))
			->add('associated', 'number', array('required' => false))
			->add('bank', 'choice', array('choices' => $userBank,'required' => false))
			->add('org', 'hidden', array('data'=>$form1->get('legal')->getData()))
			->getForm();
//			->handleRequest($request);
		    $next = true;
		}
	    }
	}
    	else
    	{
    	    $next = false;
    	    $organization = NULL;
    	    $activity = NULL;
    	    $address = NULL;
    	    $contact = NULL;
    	    $from = NULL;
    	    $newApplication = new Application();
	    //$form2 = $this->createFormBuilder($newApplication)
	    $form2 = $this->get('form.factory')->createNamedBuilder('form2name', 'form', $newApplication)
		    ->add('from_user', 'hidden', array('required' => false))
		    ->add('valid', 'hidden', array('required' => false))
		    ->add('associated', 'hidden', array('required' => false))
		    ->add('bank', 'hidden', array('required' => false))
		    ->add('org', 'hidden', array('required' => false))
		    ->getForm();
	
    	}

	return $this->render('AcmeOrganizationBundle:Default:organization_create_legal_application.html.twig',
				array(
					'form1' => $form1->createView(),
					'form2' => $form2->createView(),
					'id'=>$userId,
					'orgId'=>$id,
					'next'=>$next,
					'organization'=>$organization,
					'activity'=>$activity,
					'address'=>$address,
					'contact'=>$contact,
					'from'=>$from
					));
    }

    public function viewAction($id,Request $request)
    {
    
	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$person = new Person();
	$Application = new Application();

	$em = $this->getDoctrine()->getManager();

        $query = $em->createQuery("SELECT
    			application.id_oa,
                	application.entry_id_u,
                	application.entry_id_org,
                	application.confirmation,
                	application.creation,
                	person.firstname,
                	person.lastname,
                	person.middlename,
                	person.birthday
                		 FROM AcmeOrganizationBundle:Application application
                			 JOIN AcmeUserBundle:Person person WITH application.entry_id_u=person.user_id
                				WHERE application.id_org=:id_org AND application.entry_id_org=:entry_id_org AND application.entry_id_u=person.user_id"
                    					    )->setParameters(array(
                    							    'entry_id_org' => 0,
                        						    'id_org' => $id,
                            						    ));
	$application = $query->getResult();

        $query = $em->createQuery("SELECT
    			application.id_oa,
                	application.entry_id_u,
                	application.entry_id_org,
                	application.confirmation,
                	application.creation,
                	person.firstname,
                	person.lastname,
                	person.middlename,
                	person.birthday,
                	op.title
                		 FROM AcmeOrganizationBundle:Application application
                			 JOIN AcmeUserBundle:Person person WITH application.entry_id_u=person.user_id
                			 JOIN AcmeOrganizationBundle:OrganizationProfile op WITH application.entry_id_org=op.id_org
                				WHERE application.id_org=:id_org AND application.entry_id_org>:entry_id_org AND application.entry_id_u=person.user_id"
                    					    )->setParameters(array(
                    							    'entry_id_org' => 0,
                        						    'id_org' => $id,
                            						    ));
	$application_legal = $query->getResult();
                                                                                                                                                    

	return $this->render('AcmeOrganizationBundle:Default:organization_view_application.html.twig', array('application' => $application, 'application_legal' => $application_legal, 'id' => $id));

    }

    public function actionAction($id_org,$id_u,Request $request)
    {

	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$newApplication = new Application();

        $confirmation = array(''=>'- оберіть -','0' => 'відмовити','1' => 'прийняти');
	
	$form = $this->createFormBuilder($newApplication)
		    ->add('id_org','hidden', array('required' => false))
		    ->add('id_u','hidden', array('required' => false))
		    ->add('comments','hidden', array('required' => false))
		    ->add('answer', 'textarea', array('required' => false))
		    ->add('viewed', 'hidden', array('required' => false))
		    ->add('confirmation', 'choice', array('choices' => $confirmation,'required' => true))
		    ->add('creation', 'hidden', array('required' => false))
		    ->add('last_modify', 'hidden', array('required' => false))
		    ->getForm();

	if ($request->getMethod() == 'POST')
	{
            $form->submit($request);
                   
    	    if ($form->isValid())
    	    {
    		$em = $this->getDoctrine()->getManager();
		$application = $em->createQuery(
			    "UPDATE AcmeOrganizationBundle:Application application SET
					application.answer=:answer,
					application.confirmation=:confirmation,
					application.last_modify=:modify
					    WHERE
						application.id_org=:id_org AND application.id_u=:id_u"
						    )->setParameters(array(
							    'answer' => $form->get('answer')->getData(),
							    'confirmation' => $form->get('confirmation')->getData(),
							    'id_org' => $id_org,
							    'id_u' => $id_u,
							    'modify' => new \DateTime("now")
							    ));
		$application->getResult();
		$em->flush();

		if($form->get('confirmation')->getData() == 1)
		{
		    $newAllocate = new Allocate();

		    $newAllocate->setIdorg($id_org);
		    $newAllocate->setIdu($id_u);
		    $newAllocate->setPosition(1);
		    $newAllocate->setLocked(0);
		    $newAllocate->setCreation(new \DateTime("now"));

            	    $em = $this->getDoctrine()->getManager();
            	    $em->persist($newAllocate);
            	    $em->flush();
            	}
                
                return $this->redirect($this->generateUrl('acme_organization_application_view', array('id' => $id_org)));

    	    }
    	}

    return $this->render('AcmeOrganizationBundle:Default:organization_view_application_form.html.twig', array('form' => $form->createView(),'id'=>$userId,'id_org'=>$id_org,'id_u'=>$id_u));

    }


}