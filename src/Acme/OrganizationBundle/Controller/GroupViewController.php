<?php

namespace Acme\OrganizationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\OrganizationBundle\Entity\Organization;
use Acme\OrganizationBundle\Entity\Application;
use Acme\OrganizationBundle\Entity\Allocate;
use Acme\OrganizationBundle\Entity\Discussion;
use Acme\UserBundle\Entity\Person;

class GroupViewController extends Controller
{
    public function viewAction($id)
    {
    
	$userId = $this->get('security.context')->getToken()->getUser()->getId();
	
	// SELECT ORGANIZATION
	$organization = new Organization();
	$em = $this->getDoctrine()->getManager();

	$query = $em->createQuery(
		    "SELECT organization.id_org,op.title,op.description FROM AcmeOrganizationBundle:Organization organization
						JOIN AcmeOrganizationBundle:OrganizationProfile op WITH op.id_org=organization.id_org
						    WHERE organization.id_org=:id_org"
							)->setParameters(array(
							    'id_org' => $id,
							    ));
	$organization = $query->getResult();

	$application = new Application();
	$query = $em->createQuery(
		    "SELECT count(application.id_u) FROM AcmeOrganizationBundle:Application application WHERE application.id_org=:id_org AND application.confirmation IS NULL"
			)->setParameters(array(
				'id_org' => $id,
				));
	$cnt_app = $query->getSingleScalarResult();

	$allocate = new Allocate();
	$query = $em->createQuery(
		    "SELECT count(allocate.id_u) FROM AcmeOrganizationBundle:Allocate allocate WHERE allocate.id_org=:id_org"
			)->setParameters(array(
				'id_org' => $id
				));
	$cnt_all = $query->getSingleScalarResult();

	$query = $em->createQuery(
		    "SELECT count(allocate.id_u) FROM AcmeOrganizationBundle:Allocate allocate WHERE allocate.id_org=:id_org AND allocate.id_u=:id_u"
			)->setParameters(array(
				'id_org' => $id,
				'id_u' => $userId
				));
	$cnt = $query->getSingleScalarResult();

	$query = $em->createQuery(
	    "SELECT allocate.id_u,person.lastname,person.firstname,person.middlename FROM AcmeOrganizationBundle:Allocate allocate JOIN AcmeUserBundle:Person person WHERE allocate.id_u=person.user_id AND allocate.id_org = :id_org"
		)->setParameters(array(
			'id_org' => $id,
			));
	$allocate = $query->getResult();

	$discussion = new Discussion();
	$query = $em->createQuery(
		    "SELECT discussion FROM AcmeOrganizationBundle:discussion discussion WHERE discussion.id_org=:id_org"
			)->setParameters(array(
				'id_org' => $id,
				));
	$discussion = $query->getResult();

	return $this->render('AcmeOrganizationBundle:Default:group_view.html.twig',
				array(
				    'id' => $userId,
				    'id_org' => $id,
				    'organization' => $organization,
				    'allocate' => $allocate,
				    'cnt_app' => $cnt_app,
				    'cnt_all'=>$cnt_all,
				    'cnt' => $cnt,
				    'discussion' => $discussion
				    ));

    }
}
