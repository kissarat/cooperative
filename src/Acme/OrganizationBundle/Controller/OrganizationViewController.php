<?php

namespace Acme\OrganizationBundle\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\OrganizationBundle\Entity\Organization;

use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;

use Symfony\Component\HttpFoundation\Cookie;

use Acme\OrganizationBundle\Entity\OrganizationProfile;
use Acme\OrganizationBundle\Entity\OrganizationAddress;
use Acme\OrganizationBundle\Entity\OrganizationBank;
use Acme\OrganizationBundle\Entity\OrganizationContact;
use Acme\OrganizationBundle\Entity\OrganizationDocument;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Acme\OrganizationBundle\Entity\Application;
use Acme\OrganizationBundle\Entity\Allocate;
use Acme\OrganizationBundle\Entity\Discussion;
use Acme\UserBundle\Entity\Person;

class OrganizationViewController extends Controller
{
    public function viewAction($id)
    {
	$userId = $this->get('security.context')->getToken()->getUser()->getId();
	
	// SELECT ORGANIZATION
	$organization = new Organization();
	$em = $this->getDoctrine()->getManager();

	$query = $em->createQuery(
		    "SELECT organization.id_org,organization.type,op.title,op.description,colfm.title as colfmTitle FROM AcmeOrganizationBundle:Organization organization
						JOIN AcmeOrganizationBundle:OrganizationProfile op WITH op.id_org=organization.id_org
						JOIN AcmeOrganizationBundle:CommonCOLFM colfm WITH op.legal_form=colfm.colfm_id
						    WHERE organization.id_org=:id_org"
							)->setParameters(array(
							    'id_org' => $id,
							    ));
	$organization = $query->getResult();

	$application = new Application();
	$query = $em->createQuery(
		    "SELECT count(application.entry_id_u) FROM AcmeOrganizationBundle:Application application WHERE application.id_org=:id_org AND application.confirmation IS NULL"
			)->setParameters(array(
				'id_org' => $id,
				));
	$cnt_app = $query->getSingleScalarResult();

	$allocate = new Allocate();
	$query = $em->createQuery(
		    "SELECT count(allocate.id_u) FROM AcmeOrganizationBundle:Allocate allocate WHERE allocate.id_org=:id_org"
			)->setParameters(array(
				'id_org' => $id
				));
	$cnt_all = $query->getSingleScalarResult();

	$query = $em->createQuery(
		    "SELECT count(allocate.id_u) FROM AcmeOrganizationBundle:Allocate allocate WHERE allocate.id_org=:id_org AND allocate.id_u=:id_u"
			)->setParameters(array(
				'id_org' => $id,
				'id_u' => $userId
				));
	$cnt = $query->getSingleScalarResult();
	if($cnt > 0){
	    $query = $em->createQuery(
		    "SELECT allocate.position FROM AcmeOrganizationBundle:Allocate allocate WHERE allocate.id_org=:id_org AND allocate.id_u=:id_u"
			)->setParameters(array(
				'id_org' => $id,
				'id_u' => $userId
				));
	    $position = $query->getSingleScalarResult();
	    } else {
		$position = -1;
		}

	$query = $em->createQuery(
	    "SELECT allocate.id_u,person.lastname,person.firstname,person.middlename FROM AcmeOrganizationBundle:Allocate allocate JOIN AcmeUserBundle:Person person WHERE allocate.id_u=person.user_id AND allocate.id_org = :id_org"
		)->setParameters(array(
			'id_org' => $id,
			));
	$allocate = $query->getResult();

	$discussion = new Discussion();
	$query = $em->createQuery(
		    "SELECT discussion FROM AcmeOrganizationBundle:discussion discussion WHERE discussion.id_org=:id_org"
			)->setParameters(array(
				'id_org' => $id,
				));
	$discussion = $query->getResult();

	return $this->render('AcmeOrganizationBundle:Default:organization_view.html.twig',
				array(
				    'id' => $userId,
				    'id_org' => $id,
				    'organization' => $organization,
				    'allocate' => $allocate,
				    'cnt_app' => $cnt_app,
				    'cnt_all'=>$cnt_all,
				    'cnt' => $cnt,
				    'position' => $position,
				    'discussion' => $discussion
				    ));

    }

    public function profileAction($id, Request $request)
    {
    
	$userId = $this->get('security.context')->getToken()->getUser()->getId();
	
	// SELECT ORGANIZATION
	$organization = new Organization();
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
		    "SELECT organization.id_org,organization.type,op.title,op.short,op.description,op.certificate_vat,op.itn,op.itn_date,op.visibility,op.legal_form,op.activity FROM AcmeOrganizationBundle:Organization organization
						JOIN AcmeOrganizationBundle:OrganizationProfile op WITH op.id_org=organization.id_org
						    WHERE organization.id_org=:id_org"
							)->setParameters(array(
							    'id_org' => $id,
							    ));
	$organization = $query->getArrayResult();
	
	$visibility = array('' => 'необхідно обрати', '0' => 'закрита','1' => 'відкрита');
	
	$organizationProfile = new OrganizationProfile();
	$organizationProfile->setTitle($organization[0]['title']);
	$organizationProfile->setShort($organization[0]['short']);
	$organizationProfile->setDescription($organization[0]['description']);
	$legal_form = $organization[0]['legal_form'];
	$organizationProfile->setCertificateVat($organization[0]['certificate_vat']);
	$organizationProfile->setItn($organization[0]['itn']);
	$organizationProfile->setItnDate($organization[0]['itn_date']);
	$activity = $organization[0]['activity'];
	$vis = $organization[0]['visibility'];
	
        $form = $this->createFormBuilder($organizationProfile)
        		->add('title', 'text', array('required' => false))
                        ->add('short', 'text', array('required' => false))
                        ->add('description', 'textarea', array('required' => false))
                        ->add('legal_form', 'entity', array('class' => 'AcmeOrganizationBundle:CommonCOLFM', 'property' => 'title', 'data'=>$em->getReference('AcmeOrganizationBundle:CommonCOLFM',$legal_form), 'required' => false))
                        ->add('certificate_vat', 'text', array('required' => false))
                        ->add('itn', 'text', array('required' => false))
                        ->add('itn_date', 'date', array('widget' => 'single_text','format' => 'yyyy-MM-dd', 'required' => false))
                        ->add('activity', 'entity', array('class' => 'AcmeOrganizationBundle:CommonCEA', 'property' => 'title', 'multiple' => 'true', 'required' => false))
                        ->add('visibility', 'choice', array('choices' => $visibility, 'required' => false, 'data'=>$vis))
                        ->add('creation', 'hidden', array('required' => false))
                        ->getForm();
                                                                                                                                                                                                                                            	
	if ($request->getMethod() == 'POST')
	{
	    $form->submit($request);

	    if ($form->isValid())
	    {
		$title = $form->get('title')->getData();
		$short = $form->get('short')->getData();
		$description = $form->get('description')->getData();
		$legal_form = $form->get('legal_form')->getData();
		$legal_form = $legal_form->getColfmId();
		$certificate_vat = $form->get('certificate_vat')->getData();
		$itn = $form->get('itn')->getData();
		$itn_date = $form->get('itn_date')->getData();
		$act = array();
		foreach($form->get('activity')->getData() as $a){ $act[$a->getCeaId()] = $a->getTitle(); }
		    $activity = json_encode($act);
		$visibility = $form->get('visibility')->getData();

		$organizationProfile = $em->createQuery(
		                        "UPDATE AcmeOrganizationBundle:OrganizationProfile op SET
		                                    op.title = :title,
		                                    op.short = :short,
		                                    op.description = :description,
		                                    op.legal_form = :legal_form,
		                                    op.certificate_vat = :certificate_vat,
		                                    op.itn = :itn,
		                                    op.itn_date = :itn_date,
		                                    op.activity = :activity,
		                                    op.visibility = :visibility
						    WHERE op.id_org = :id_org"
							    )->setParameters(array(
								'id_org' => $id,
								'title' => $title,
								'short' => $short,
								'description' => $description,
								'legal_form' => $legal_form,
								'certificate_vat' => $certificate_vat,
								'itn' => $itn,
								'itn_date' => $itn_date,
								'activity' => $activity,
								'visibility' => $visibility
								));
		$organizationProfile->getResult();
                $em->flush();

		return $this->redirect($this->generateUrl('acme_organization_profile', array('id' => $id)));
	    }
	}

	return $this->render('AcmeOrganizationBundle:Default:organization_profile.html.twig',
				array(
				    'form' => $form->createView(),
				    'id' => $userId,
				    'id_org' => $id,
				    'organization' => $organization
				    ));

    }

    public function addressAction(Request $request,$id)
    {
	
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
			"SELECT a.id_org_address,a.address_type,country.title,coatsu.unit_name,street.title as stitle,building.building_no,a.address FROM AcmeOrganizationBundle:OrganizationAddress a
				JOIN AcmeUserBundle:Country country WITH a.country_id=country.country_id
				JOIN AcmeUserBundle:Coatsu coatsu WITH a.administrative_unit_id=coatsu.administrative_unit_id
				JOIN AcmeUserBundle:CommonStreet street WITH a.street_id=street.street_id
				JOIN AcmeUserBundle:CommonBuilding building WITH a.building_id=building.building_id
					WHERE
					    a.id_org=:id_org"
						)->setParameters(array(
							'id_org' => $id
							));
	$address = $query->getResult();
	
	$oAddress = new OrganizationAddress();
	$form = $this->createFormBuilder($oAddress)
	    ->add('address_type', 'choice', array(
	                    'choices' => array('' => '- необхідно обрати -', 0=>'Поштова адреса',1=>'Юридична адреса'),
	                    'required' => true
	                    ))
	    ->getForm();
	if ($request->getMethod() == 'POST')
	{
	    $form->submit($request);
	    if ($form->isValid())
	    {
		$address_type = $form->get('address_type')->getData();
		$country_id = $request->request->get('country_id');
		$administrative_unit_id = $request->request->get('administrative_unit_id');
		$street_id = $request->request->get('street_id');
		$building_id = $request->request->get('building_id');
		$address = $request->request->get('address');

		$oAddress = new OrganizationAddress();
		$oAddress->setIdOrg($id);
		$oAddress->setAddressType($address_type);
		$oAddress->setCountryId($country_id);
		$oAddress->setAdministrativeUnitId($administrative_unit_id);
		$oAddress->setStreetId($street_id);
		$oAddress->setBuildingId($building_id);
		$oAddress->setAddress($address);

		$em = $this->getDoctrine()->getManager();
		$em->persist($oAddress);
		$em->flush();
		return $this->redirect($this->generateUrl('acme_organization_address', array('id'=> $id)));
	    }
	}
	return $this->render('AcmeOrganizationBundle:Default:organization_address.html.twig', array('form' => $form->createView(),'id' => $id, 'address' => $address));
    }

    public function addressDeleteAction($id_org,$id_org_address)
    {
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
			"DELETE FROM AcmeOrganizationBundle:OrganizationAddress oa WHERE oa.id_org_address=:id"
				)->setParameters(array(
					'id' => $id_org_address
					));
	$query->getResult();
	return $this->redirect($this->generateUrl('acme_organization_address', array('id'=> $id_org)));
    }

    public function bankAction(Request $request,$id)
    {
	
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
			"SELECT bank FROM AcmeOrganizationBundle:OrganizationBank bank WHERE bank.id_org=:id_org"
						)->setParameters(array(
							'id_org' => $id
							));
	$bank = $query->getResult();
	
	$oBank = new OrganizationBank();
	$form = $this->createFormBuilder($oBank)
	    ->add('title', 'text', array('required' => true))
	    ->add('mfo', 'text', array('required' => true))
	    ->add('code', 'text', array('required' => true))
	    ->add('rr', 'text', array('required' => true))
	    ->add('description', 'text', array('required' => true))
	    ->getForm();
	if ($request->getMethod() == 'POST')
	{
	    $form->submit($request);
	    if ($form->isValid())
	    {
		$oBank = new OrganizationBank();
		$oBank->setIdOrg($id);
		$oBank->setTitle($form->get('title')->getData());
		$oBank->setMfo($form->get('mfo')->getData());
		$oBank->setCode($form->get('code')->getData());
		$oBank->setRr($form->get('rr')->getData());
		$oBank->setDescription($form->get('description')->getData());

		$em = $this->getDoctrine()->getManager();
		$em->persist($oBank);
		$em->flush();
		return $this->redirect($this->generateUrl('acme_organization_bank', array('id'=> $id)));
	    }
	}
	return $this->render('AcmeOrganizationBundle:Default:organization_bank.html.twig', array('form' => $form->createView(),'id' => $id, 'bank' => $bank));
    }

    public function bankDeleteAction($id_org,$id_org_bank)
    {
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
			"DELETE FROM AcmeOrganizationBundle:OrganizationBank ob WHERE ob.id_org_bank=:id"
				)->setParameters(array(
					'id' => $id_org_bank
					));
	$query->getResult();
	return $this->redirect($this->generateUrl('acme_organization_bank', array('id'=> $id_org)));
    }

    public function contactAction(Request $request,$id)
    {
	
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
			"SELECT contact FROM AcmeOrganizationBundle:OrganizationContact contact WHERE contact.id_org=:id_org"
						)->setParameters(array(
							'id_org' => $id
							));
	$contact = $query->getResult();
	
	$contactArray = array('' => '- необхідно обрати -', '0' => 'Телефон','1' => 'Факс','2' => 'E-mail','3' => 'Url');
	
	$oContact = new OrganizationContact();
	$form = $this->createFormBuilder($oContact)
	    ->add('type', 'choice', array('choices' => $contactArray, 'required' => false))
	    ->add('description', 'text', array('required' => true, 'attr' => array('placeholder' => 'наприклад, телефон: мобільний/робочий/додатковий...')))
	    ->add('contact', 'text', array('required' => true))
	    ->getForm();
	if ($request->getMethod() == 'POST')
	{
	    $form->submit($request);
	    if ($form->isValid())
	    {
		$oContact = new OrganizationContact();
		$oContact->setIdOrg($id);
		$oContact->setType($form->get('type')->getData());
		$oContact->setDescription($form->get('description')->getData());
		$oContact->setContact($form->get('contact')->getData());

		$em = $this->getDoctrine()->getManager();
		$em->persist($oContact);
		$em->flush();
		return $this->redirect($this->generateUrl('acme_organization_contact', array('id'=> $id)));
	    }
	}
	return $this->render('AcmeOrganizationBundle:Default:organization_contact.html.twig', array('form' => $form->createView(),'id' => $id, 'contact' => $contact));
    }

    public function contactDeleteAction($id_org,$id_org_contact)
    {
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
			"DELETE FROM AcmeOrganizationBundle:OrganizationContact contact WHERE contact.id_org_contact=:id"
				)->setParameters(array(
					'id' => $id_org_contact
					));
	$query->getResult();
	return $this->redirect($this->generateUrl('acme_organization_contact', array('id'=> $id_org)));
    }

///////////////////////////////////
    public function documentAction(Request $request,$id)
    {
	if(!is_dir("uploads/organization_documents/$id")){mkdir("uploads/organization_documents/$id", 0755);}
		
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
			"SELECT document FROM AcmeOrganizationBundle:OrganizationDocument document WHERE document.id_org=:id_org"
						)->setParameters(array(
							'id_org' => $id
							));
	$document = $query->getResult();
	
	$oDocument = new OrganizationDocument();
	$form = $this->createFormBuilder($oDocument)
	    ->add('description')
	    ->add('file')
	    ->getForm();
	    
	if ($request->getMethod() == 'POST')
	{
	    $form->submit($request);
	    if ($form->isValid())
	    {
		$em = $this->getDoctrine()->getManager();
		$oDocument->upload($id);
		$oDocument->setIdOrg($id);
		$oDocument->setDescription($form->get('description')->getData());
		$em->persist($oDocument);
		$em->flush();
		return $this->redirect($this->generateUrl('acme_organization_document', array('id'=> $id)));
	    }
	}
	return $this->render('AcmeOrganizationBundle:Default:organization_document.html.twig', array('form' => $form->createView(),'id' => $id, 'document' => $document));
    }

    public function documentDeleteAction($id_org,$id_org_document)
    {
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
			"SELECT document.path FROM AcmeOrganizationBundle:OrganizationDocument document WHERE document.id_org_document=:id_org_document"
						)->setParameters(array(
							'id_org_document' => $id_org_document
							));
	$doc = $query->getSingleResult();
	$query = $em->createQuery(
			"DELETE FROM AcmeOrganizationBundle:OrganizationDocument document WHERE document.id_org_document=:id"
				)->setParameters(array(
					'id' => $id_org_document
					));
	$query->getResult();
	unlink("uploads/organization_documents/$id_org/".$doc['path']);
	return $this->redirect($this->generateUrl('acme_organization_document', array('id'=> $id_org)));
    }

    public function viewOrganizationDocumentAction($id_org,$path)
    {
	$guesser = MimeTypeGuesser::getInstance();
	$mimetype = $guesser->guess("uploads/organization_documents/$id_org/$path");
	$headers = array("Content-Type" => "$mimetype", "Content-Disposition" => "inline; filename='$path'");
	return new Response(file_get_contents("uploads/organization_documents/$id_org/$path"), 200, $headers);
    }

    public function organizationAdminOnAction($id)
    {
	$cookie = new Cookie('admin', "$id::1");
	$response = new Response();
	$response->headers->setCookie($cookie);
	$response->send();
	return $this->redirect($this->generateUrl('acme_organization_view', array('id'=> $id)));
    }

    public function organizationAdminOffAction($id)
    {
	$response = new Response();
	$response->headers->clearCookie('admin');
	$response->send();
	return $this->redirect($this->generateUrl('acme_organization_view', array('id'=> $id)));
    }

}
