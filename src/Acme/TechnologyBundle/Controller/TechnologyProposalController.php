<?php

namespace Acme\TechnologyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\GoodsBundle\Entity\Catalog;
use Acme\GoodsBundle\Entity\Goods;

use Acme\UserBundle\Entity\User;
use Acme\UserBundle\Entity\Person;
use Acme\UserBundle\Entity\PersonalInfonPerson;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TechnologyProposalController extends Controller
{
    public function indexAction($id=NULL)
    {
	// SELECT PATH
	$em = $this->getDoctrine()->getManager();
	$conn = $em->getConnection();
	//$id=5;
	$tree = $conn->prepare("SELECT * FROM getTree($id)");
	$tree->execute();
	$path = $tree->fetchAll();

	// SELECT GOODS CATEGORY
	$Catalog = new Catalog();
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
		    'SELECT catalog FROM AcmeGoodsBundle:Catalog catalog WHERE catalog.pid = :pid'
			)->setParameters(array(
				'pid' => $id,
				));
	$catalog = $query->getResult();
	
	//if (!$goods) {throw $this->createNotFoundException('No users found!');}
	//else {
	    return $this->render('AcmeTechnologyBundle:Default:technology_proposal_view.html.twig', array('id'=>$id,'path' => $path, 'catalog' => $catalog));
	//    }
    }

}
