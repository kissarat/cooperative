<?php 

namespace Acme\NewsBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
* @ORM\Table(name="news")
 */
 class News
 {
/**
* @ORM\Id
 * @ORM\Column(name="id_news", type="integer")
 * @ORM\GeneratedValue(strategy="AUTO")
 */
protected $id_news;
/**
 * @ORM\Column(name="id_u", type="integer")
 */
protected $id_u;

 /**
* @ORM\Column(name="title", type="string")
*/
 protected $title;
 /**
* @ORM\Column(name="brief", type="string")
*/
 protected $brief;
 
  /**
* @ORM\Column(name="fulldescription", type="string")
*/

 protected $fulldescription;
 /**
* @ORM\Column(name="public", type="integer")
*/
 protected $public;
 
/**
* @ORM\Column(name="creation", type="datetime")
*/
 protected $creation;
 
 
 

 

    /**
     * Get id_news
     *
     * @return integer 
     */
    public function getIdNews()
    {
        return $this->id_news;
    }

    /**
     * Set id_u
     *
     * @param integer $idU
     * @return News
     */
    public function setIdU($idU)
    {
        $this->id_u = $idU;

        return $this;
    }

    /**
     * Get id_u
     *
     * @return integer 
     */
    public function getIdU()
    {
        return $this->id_u;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set brief
     *
     * @param string $brief
     * @return News
     */
    public function setBrief($brief)
    {
        $this->brief = $brief;

        return $this;
    }

    /**
     * Get brief
     *
     * @return string 
     */
    public function getBrief()
    {
        return $this->brief;
    }

    /**
     * Set fulldescription
     *
     * @param string $fulldescription
     * @return News
     */
    public function setFulldescription($fulldescription)
    {
        $this->fulldescription = $fulldescription;

        return $this;
    }

    /**
     * Get fulldescription
     *
     * @return string 
     */
    public function getFulldescription()
    {
        return $this->fulldescription;
    }

    /**
     * Set public
     *
     * @param integer $public
     * @return News
     */
    public function setPublic($public)
    {
        $this->public = $public;

        return $this;
    }

    /**
     * Get public
     *
     * @return integer 
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Set creation
     *
    * @param \DateTime $creation                                                                                                                                                                          
    * @return News
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime
     */
    public function getCreation()
    {
        return $this->creation;
    }
}
