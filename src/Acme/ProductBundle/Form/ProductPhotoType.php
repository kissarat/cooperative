<?php

namespace Acme\ProductBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductPhotoType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$builder->add('photo');
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
	$resolver->setDefaults(array(
			'data_class' => 'Acme\ProductBundle\Entity\ProductPhoto',
			));
    }
                        
    public function getName()
    {
	return 'photo';
    }

}