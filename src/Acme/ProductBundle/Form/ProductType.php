<?php

namespace Acme\ProductBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Acme\ProductBundle\Entity\ProductPhoto;

class ProductType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$builder
		->add('name')
		->add('photos', 'collection', array(
		    'type' => new ProductPhotoType(),
		    'allow_add' => true,
		    'allow_delete' => true,
		    'prototype' => true,
		    'label' => 'photos',
		    'options' => array("data_class" => 'Acme\ProductBundle\Entity\ProductPhoto'),
		    ))
		->add('save it', 'submit')
		;
    }
                            
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
	$resolver->setDefaults(array(
			'data_class' => 'Acme\ProductBundle\Entity\Product',
			));
    }
                                    
    public function getName()
    {
	return 'product';
    }

}