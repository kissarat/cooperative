<?php

namespace Acme\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Acme\ProductBundle\Entity\Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id_pr", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_pr;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \Acme\ProductBundle\Entity\ProductPhoto
     *
     * @ORM\OneToMany(targetEntity="Acme\ProductBundle\Entity\Product", mappedBy="product", cascade={"ALL"})
     */
    private $photos;
    
    /**
     * Constructor
     */
    public function __construct()
    {
	$this->photos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add photos
     *
     * @param \Acme\ProductBundle\Entity\ProductPhoto $photos
     * @return Product
     */
    public function addPhoto(\Acme\ProductBundle\Entity\ProductPhoto $photos)
    {
	$this->photos[] = $photos;
	$photo->setProduct($this);
	return $this;
    }
             
    /**
     * Remove photos
     *
     * @param \Acme\ProductBundle\Entity\Product $photos
     */
    public function removePhoto(\Acme\FormBundle\Entity\ProductPhoto $photos)
    {
	$this->photos->removeElement($photos);
    }
                         
    /**
    * Get photos
    *
    * @return \Doctrine\Common\Collections\Collection
    */
    public function getPhotos()
    {
	return $this->photos;
    }

    /**
     * Get id_pr
     *
     * @return integer 
     */
    public function getIdPr()
    {
        return $this->id_pr;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
