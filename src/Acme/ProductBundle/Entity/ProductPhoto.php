<?php

namespace Acme\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Acme\ProductBundle\Entity\ProductPhoto
 *
 * @ORM\Table(name="product_photo")
 * @ORM\Entity
 */
class ProductPhoto
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id_prp", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id_prp;
                            
    /**
     * @ORM\Column(name="photo", type="string")
     */
    private $photo;
      
    /**
    * @var \Acme\ProductBundle\Entity\Product
    * @ORM\ManyToOne(targetEntity="Acme\ProductBundle\Entity\Product", inversedBy="photos")
    * @ORM\JoinColumns({
    *   @ORM\JoinColumn(name="id_pr", referencedColumnName="id_pr")
    * })
    */
    private $product;
             
    /**
    * Set product
    *
    * @param \Acme\ProductBundle\Entity\Product $product
    * @return Photo
    */
    public function setProduct(\Acme\ProductBundle\Entity\Product $product = null)
    {
	$this->product = $product;
	return $this;
    }
                          
    /**
    * Get product
    *
    * @return \Acme\ProductBundle\Entity\Product
    */
    public function getProduct()
    {
	return $this->product;
    }


    /**
     * Get id_prp
     *
     * @return integer 
     */
    public function getIdPrp()
    {
        return $this->id_prp;
    }

    /**
     * Set photo
     *
     * @param string $photo
     * @return ProductPhoto
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string 
     */
    public function getPhoto()
    {
        return $this->photo;
    }
}
