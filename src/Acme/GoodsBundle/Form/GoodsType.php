<?php

namespace Acme\GoodsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class GoodsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

	$avail = array('0' => 'в наявності','1' => 'немає в наявності','2' => 'під замовлення');
	$volume = array('0' => 'в роздріб','1' => 'оптом та в роздріб','2' => 'оптом');
	$currency = array('0' => 'ГРН','1' => 'USD');
	$unit = array('0' => 'ШТ','1' => 'ОД','2' => 'Л','3' => 'УП');
	$courier = array(1=> "Нова пошта",2=> "Міст Експрес",3=> "Авто Люкс",4=> "TNT express",5=> "Гюнсел",6=> "Делівері",7=> "Євроекспрес пошта",8=> "Express moto",9=> "Weltex",10=> "Ін Тайм",11=> "DHL-Експрес",12=> "Арамекс",13=> "Кур'єрська служба доставки",14=> "Обласний вузол спеціального зв’язку",15=> "Меркурій-Експресс",16=> "Міжнародна служба доставки UPS",17=> "Експрес-пошта",18=> "СМС",19=> "Кур'єрський сервіс від А до Я");

        $builder
            ->add('id_u','hidden')
            ->add('type','hidden')
            ->add('title','text')
            ->add('ua_code','text')
            ->add('category','hidden')
            ->add('avail', 'choice', array('choices' => $avail))
            ->add('volume', 'choice', array('choices' => $volume))
            ->add('price')
            ->add('price_currency', 'choice', array('choices' => $currency))
            ->add('price_unit', 'choice', array('choices' => $unit))
            ->add('return')
            ->add('return_currency', 'choice', array('choices' => $currency))
            ->add('return_unit', 'choice', array('choices' => $unit))
            ->add('contribution')
            ->add('contribution_currency', 'choice', array('choices' => $currency))
            ->add('contribution_unit', 'choice', array('choices' => $unit))
            ->add('contribution_account')
            ->add('descriptions')
            ->add('country')
            ->add('region')
            ->add('district')
            ->add('settlement')
            ->add('filter')
            ->add('brand')
            ->add('brand_country','country')
            ->add('goods_code')
            ->add('goods_url','url')
            ->add('frequancy_volume', 'hidden', array('required' => false,))
            ->add('single')
            ->add('pickup')
            ->add('courier')
            ->add('courier', 'choice', array('multiple' => true, 'expanded' => true, 'choices' => $courier))
            ->add('transportation')
            ->add('status')
            ->add('creation')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Acme\GoodsBundle\Entity\Goods'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
//        return 'acme_goodsbundle_goods';
        return 'newgoods';

    }
}
