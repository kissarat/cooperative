CREATE OR REPLACE FUNCTION getTree1(_pid int,num int)
RETURNS SETOF catalog AS $$
DECLARE r catalog;
BEGIN
    FOR r IN
	IF num < 1 THEN
	    SELECT * FROM catalog WHERE id_cat = _pid
            RETURN QUERY SELECT * FROM getTree(r.id_cat,1)
	ELSE
	    SELECT * FROM catalog WHERE id_cat = (SELECT pid FROM catalog WHERE id_cat = _pid)
    	    LOOP
        	RETURN NEXT r; -- return node
        	RETURN QUERY SELECT * FROM getTree(r.id_cat,1);
    	    END LOOP;
    	END IF;
    RETURN;
END;
$$ LANGUAGE plpgsql STRICT;