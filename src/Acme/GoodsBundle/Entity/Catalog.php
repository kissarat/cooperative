<?php
namespace Acme\GoodsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
* Acme\UserBundle\Entity
*
* @ORM\Entity
* @ORM\Table(name="catalog")
*/

class Catalog
{
    /**
    * @ORM\Id
    * @ORM\Column(name="id_cat", type="integer")
    * @ORM\GeneratedValue(strategy="AUTO")
    */
    protected $id_cat;
    
    /**
    * @ORM\Column(name="type", type="integer")
    */
    protected $type;
    
    /**
    * @ORM\Column(name="code", type="integer")
    */
    protected $code;
    
    /**
    * @ORM\Column(name="title", type="string")
    */
    protected $title;
    
    /**
    * @ORM\Column(name="img", type="string")
    */
    protected $img;

    /**
    * @ORM\Column(name="pid", type="integer")
    */
    protected $pid;

    /**
    * @ORM\Column(name="visibility", type="integer")
    */
    protected $visibility;
    
    /**
    * @ORM\Column(name="creation", type="datetime")
    */
    protected $creation;

    /**
    * @ORM\Column(name="ru", type="string")
    */
    protected $ru;

    /**
    * @ORM\Column(name="ua", type="string")
    */
    protected $ua;


    /**
     * Get id_cat
     *
     * @return integer 
     */
    public function getIdCat()
    {
        return $this->id_cat;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Catalog
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set code
     *
     * @param integer $code
     * @return Catalog
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return integer 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Catalog
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set img
     *
     * @param string $img
     * @return Catalog
     */
    public function setImg($img)
    {
        $this->img = $img;

        return $this;
    }

    /**
     * Get img
     *
     * @return string 
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * Set pid
     *
     * @param integer $pid
     * @return Catalog
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Get pid
     *
     * @return integer 
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Set visibility
     *
     * @param integer $visibility
     * @return Catalog
     */
    public function setVisibility($visibility)
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * Get visibility
     *
     * @return integer 
     */
    public function getVisibility()
    {
        return $this->visibility;
    }

    /**
     * Set creation
     *
     * @param \DateTime $creation
     * @return Catalog
     */
    public function setCreation($creation)
    {
        $this->creation = $creation;

        return $this;
    }

    /**
     * Get creation
     *
     * @return \DateTime 
     */
    public function getCreation()
    {
        return $this->creation;
    }

    /**
     * Set ru
     *
     * @param string $ru
     * @return Catalog
     */
    public function setRu($ru)
    {
        $this->ru = $ru;

        return $this;
    }

    /**
     * Get ru
     *
     * @return string 
     */
    public function getRu()
    {
        return $this->ru;
    }

    /**
     * Set ua
     *
     * @param string $ua
     * @return Catalog
     */
    public function setUa($ua)
    {
        $this->ua = $ua;

        return $this;
    }

    /**
     * Get ua
     *
     * @return string 
     */
    public function getUa()
    {
        return $this->ua;
    }
}
