<?php
namespace Acme\GoodsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Acme\GoodsBundle\Entity\Goods;
#use Acme\GoodsBundle\Entity\GoodsForm;

class GoodsProposalCreateController extends Controller
{
    public function createAction(Request $request)
    {
    
	$userId = $this->get('security.context')->getToken()->getUser()->getId();

	$newGoods = new Goods();
	
	$newGoods->setIdu($userId);
	$newGoods->setType(1);
	$newGoods->setStatus(1);
	$newGoods->setCreation('2015-01-15');
	/*
	$newGoods->setTitle("");
	$newGoods->setUa_code("");
	$newGoods->setCategory("");
	$newGoods->setAvail("");
	$newGoods->setVolume("");
	$newGoods->setPrice_min("");
	$newGoods->setPrice_max("");
	$newGoods->setPrice_currency("");
	$newGoods->setPrice_unit("");
	$newGoods->setReturn("");
	$newGoods->setReturn_currency("");
	$newGoods->setReturn_unit("");
	$newGoods->setContribution("");
	$newGoods->setContribution_currency("");
	$newGoods->setContribution_unit("");
	$newGoods->setContribution_account("");
	$newGoods->setDescriptions("");
	$newGoods->setCountry("");
	$newGoods->setRegion("");
	$newGoods->setDistrict("");
	$newGoods->setSettlement("");
	$newGoods->setPhoto("");
	$newGoods->setAddition("");
	$newGoods->setFilter("");
	$newGoods->setBrand("");
	$newGoods->setBrand_country("");
	$newGoods->setGoods_code("");
	$newGoods->setGoods_url("");
	$newGoods->setFrequancy_volume("");
	$newGoods->setSingle("");
	$newGoods->setPickup("");
	$newGoods->setCourier("");
	$newGoods->setTransportation("");
	$newGoods->setStatus("");
	*/
	$avail = array('0' => 'в наявності','1' => 'немає в наявності','2' => 'під замовлення');
	$volume = array('0' => 'в роздріб','1' => 'оптом та в роздріб','2' => 'оптом');
	$currency = array('0' => 'ГРН','1' => 'USD');
	$unit = array('0' => 'ШТ','1' => 'ОД','2' => 'Л','3' => 'УП');
	$courier = array(1=> "Нова пошта",2=> "Міст Експрес",3=> "Авто Люкс",4=> "TNT express",5=> "Гюнсел",6=> "Делівері",7=> "Євроекспрес пошта",8=> "Express moto",9=> "Weltex",10=> "Ін Тайм",11=> "DHL-Експрес",12=> "Арамекс",13=> "Кур'єрська служба доставки",14=> "Обласний вузол спеціального зв’язку",15=> "Меркурій-Експресс",16=> "Міжнародна служба доставки UPS",17=> "Експрес-пошта",18=> "СМС",19=> "Кур'єрський сервіс від А до Я");

	$form = $this->createFormBuilder($newGoods)
		    ->add('id_u','hidden', array('required' => false,))
		    ->add('type','hidden', array('required' => false,))
		    ->add('title','text', array('required' => false,))
		    ->add('ua_code','number', array('required' => false,))
		    ->add('category','hidden', array('required' => false,))
		    ->add('avail', 'choice', array('choices' => $avail))
		    ->add('volume', 'choice', array('choices' => $volume))
		    ->add('price', 'text', array('required' => false,))
		    ->add('price_currency', 'choice', array('choices' => $currency))
		    ->add('price_unit', 'choice', array('choices' => $unit))
		    ->add('return', 'text', array('required' => false,))
		    ->add('return_currency', 'choice', array('choices' => $currency))
		    ->add('return_unit', 'choice', array('choices' => $unit))
		    ->add('contribution', 'text', array('required' => false,))
		    ->add('contribution_currency', 'choice', array('choices' => $currency))
		    ->add('contribution_unit', 'choice', array('choices' => $unit))
		    ->add('contribution_account', 'text', array('required' => false,))
		    ->add('descriptions', 'textarea', array('required' => false,))
		    ->add('country', 'text', array('required' => false,))
		    ->add('region', 'text', array('required' => false,))
		    ->add('district', 'text', array('required' => false,))
		    ->add('settlement', 'text', array('required' => false,))
		    ->add('filter', 'text', array('required' => false,))
		    ->add('brand', 'text', array('required' => false,))
		    ->add('brand_country','country', array('required' => false,))
		    ->add('goods_code', 'text', array('required' => false,))
		    ->add('goods_url','url', array('required' => false,))
		    ->add('frequancy_volume', 'hidden', array('required' => false,))
		    ->add('single', 'text', array('required' => false,))
		    ->add('pickup', 'text', array('required' => false,))
		    ->add('courier', 'choice', array('multiple' => true, 'expanded' => true, 'choices' => $courier))
		    ->add('transportation', 'text', array('required' => false,))
		    ->add('status', 'hidden', array('required' => false,))
		    ->add('creation', 'hidden', array('required' => false,))
		    ->getForm();


	if ($request->getMethod() == 'POST')
	{
       
            $form->submit($request);
                   
    	    if ($form->isValid())
    	    {
		$title = $form->get('title')->getData();
		$ua_code = $form->get('ua_code')->getData();
		$category = $form->get('category')->getData();
		$avail = $form->get('avail')->getData();
		$volume = $form->get('volume')->getData();
		$price = $form->get('price')->getData();
		$price_currency = $form->get('price_currency')->getData();
		$price_unit = $form->get('price_unit')->getData();
		$return = $form->get('return')->getData();
		$return_currency = $form->get('return_currency')->getData();
		$return_unit = $form->get('return_unit')->getData();
		$contribution = $form->get('contribution')->getData();
		$contribution_currency = $form->get('contribution_currency')->getData();
		$contribution_unit = $form->get('contribution_unit')->getData();
		$contribution_account = $form->get('contribution_account')->getData();
		$descriptions = $form->get('descriptions')->getData();
		$country = $form->get('country')->getData();
		$region = $form->get('region')->getData();
		$district = $form->get('district')->getData();
		$settlement = $form->get('settlement')->getData();
		$filter = $form->get('filter')->getData();
		$brand = $form->get('brand')->getData();
		$brand_country = $form->get('brand_country')->getData();
		$goods_code = $form->get('goods_code')->getData();
		$goods_url = $form->get('goods_url')->getData();
		$frequancy_volume = $form->get('frequancy_volume')->getData();
		$single = $form->get('single')->getData();
		$pickup = $form->get('pickup')->getData();
		$courier = $form->get('courier')->getData();
		$transportation = $form->get('transportation')->getData();

                // INSERT
                // $PersonalInfoAddress = new PersonalInfoAddress();

		$newGoods->setIdU($userId);
                $newGoods->setType(1);
                $newGoods->setTitle($title);
                $newGoods->setUaCode($ua_code);
		$newGoods->setCategory($category);
		$newGoods->setAvail($avail);
		$newGoods->setVolume($volume);
		$newGoods->setPriceMin($price);
		$newGoods->setPriceCurrency($price_currency);
		$newGoods->setPriceUnit($price_unit);
		$newGoods->setReturn($return);
		$newGoods->setReturnCurrency($return_currency);
		$newGoods->setReturnUnit($return_unit);
		$newGoods->setContribution($contribution);
		$newGoods->setContributionCurrency($contribution_currency);
		$newGoods->setContributionUnit($contribution_unit);
		$newGoods->setContributionAccount($contribution_account);
		$newGoods->setDescriptions($descriptions);
		$newGoods->setCountry($country);
		$newGoods->setRegion($region);
		$newGoods->setDistrict($district);
		$newGoods->setSettlement($settlement);
		$newGoods->setFilter($filter);
		$newGoods->setBrand($brand);
		$newGoods->setBrandCountry($brand_country);
		$newGoods->setGoodsCode($goods_code);
		$newGoods->setGoodsUrl($goods_url);
		$newGoods->setFrequancyVolume($frequancy_volume);
		$newGoods->setSingle($single);
		$newGoods->setPickup($pickup);
		$newGoods->setCourier(implode(',',$courier));
		$newGoods->setTransportation($transportation);
		
		$newGoods->setCreation(new \DateTime("now"));
                                                                                                                                                                                    
                $em = $this->getDoctrine()->getManager();
                $em->persist($newGoods);
                $em->flush();
                
                return $this->redirect($this->generateUrl('acme_goods_proposal_create'));
    	    }
    	}
    return $this->render('AcmeGoodsBundle:Default:goods_proposal_create.html.twig', array('form' => $form->createView(),'id'=>$userId,'month'=>array('Січ.','Лют.','Бер.','Кві.','Тра.','Чер.','Лип.','Сер.','Вер.','Жов.','Лис.','Гру.')));

    }
}