<?php

namespace Acme\GoodsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\GoodsBundle\Entity\Catalog;
use Acme\GoodsBundle\Entity\Goods;

class UsersGoodsProposalController extends Controller
{
    public function indexAction($id)
    {
	// SELECT GOODS
	$goods = array();
	$Goods = new Goods();
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
		    'SELECT goods FROM AcmeGoodsBundle:Goods goods WHERE goods.id_u =:id_u AND goods.type = :type'
			)->setParameters(array(
				'id_u' => $id,
				'type' => 1,
				));
	$goods = $query->getResult();

	return $this->render('AcmeGoodsBundle:Default:users_goods_proposal_view.html.twig', array('id' =>$id, 'goods' => $goods));

    }
}
