<?php

namespace Acme\GoodsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Acme\GoodsBundle\Entity\Catalog;
use Acme\GoodsBundle\Entity\Goods;

use Acme\UserBundle\Entity\User;
use Acme\UserBundle\Entity\Person;
use Acme\UserBundle\Entity\PersonalInfonPerson;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GoodsProposalController extends Controller
{
    public function indexAction($id=NULL)
    {
	// SELECT PATH
	$em = $this->getDoctrine()->getManager();
	$conn = $em->getConnection();
	//$id=3;
	$tree = $conn->prepare("SELECT * FROM getTree($id)");
	$tree->execute();
	$path = $tree->fetchAll();

	// SELECT GOODS CATEGORY
	$Catalog = new Catalog();
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
		    'SELECT catalog FROM AcmeGoodsBundle:Catalog catalog WHERE catalog.pid = :pid'
			)->setParameters(array(
				'pid' => $id,
				));
	$catalog = $query->getResult();
	
	// SELECT GOODS
	$goods = array();
	$Goods = new Goods();
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
		    'SELECT goods.id_u,goods.title,goods.price,goods.price_currency,goods.creation,inf.firstname,inf.lastname FROM AcmeGoodsBundle:Goods goods
			    JOIN AcmeUserBundle:Person p WITH goods.id_u=p.user_id
			    JOIN AcmeUserBundle:PersonalInfoPerson inf WITH p.person_id=inf.person_id
				    WHERE goods.category =:category AND goods.type = :type'
					)->setParameters(array(
						'category' => $id,
						'type' => 1,
						));
	$goods = $query->getResult();

	//if (!$goods) {throw $this->createNotFoundException('No users found!');}
	//else {
	    return $this->render('AcmeGoodsBundle:Default:goods_proposal_view.html.twig', array('id'=>$id,'path' => $path, 'catalog' => $catalog, 'goods' => $goods, 'search' => ''));
	//    }

    }

    public function searchAction($id=NULL)
    {
	$varSearch = $this->getRequest()->get('search');
	
	// SELECT GOODS
	$goods = array();
	$Goods = new Goods();
	$em = $this->getDoctrine()->getManager();
	                                                                                                                                        
	$query = $em->createQuery(
		    'SELECT goods.id_u,goods.title,goods.price,goods.price_currency,goods.creation,goods.category,inf.firstname,inf.lastname FROM AcmeGoodsBundle:Goods goods
			    JOIN AcmeUserBundle:Person p WITH goods.id_u=p.user_id
			    JOIN AcmeUserBundle:PersonalInfoPerson inf WITH p.person_id=inf.person_id
				    WHERE goods.type=:type AND goods.title LIKE :title OR goods.descriptions LIKE :description'
					)->setParameters(array(
						'title' => '%'.$varSearch.'%',
						'description' => '%'.$varSearch.'%',
						'type' => 1,
						));
	$goods = $query->getResult();
	foreach ($goods as $g){$id = $g['category'];}

	// SELECT PATH
	$em = $this->getDoctrine()->getManager();
	$conn = $em->getConnection();
	$tree = $conn->prepare("SELECT * FROM getTree($id)");
	$tree->execute();
	$path = $tree->fetchAll();

	// SELECT GOODS CATEGORY
	$Catalog = new Catalog();
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
		    'SELECT catalog FROM AcmeGoodsBundle:Catalog catalog WHERE catalog.pid = :pid'
			)->setParameters(array(
				'pid' => $id,
				));
	$catalog = $query->getResult();

	//if (!$goods) {throw $this->createNotFoundException('No users found!');}
	//else {
	    return $this->render('AcmeGoodsBundle:Default:goods_proposal_view.html.twig', array('id'=>$id,'path' => $path, 'catalog' => $catalog, 'goods' => $goods, 'search' => $varSearch));
	//    }

    }

}
