<?php

namespace Acme\GoodsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Acme\OrganizationBundle\Entity\Organization;
use Acme\OrganizationBundle\Entity\OrganizationProfile;

use Acme\GoodsBundle\Entity\Catalog;
use Acme\GoodsBundle\Entity\Goods;

class OrganizationGoodsProposalController extends Controller
{
    public function indexAction($id_org)
    {

	$userId = $this->get('security.context')->getToken()->getUser()->getId();
    
        // SELECT ORGANIZATION
        $organization = new Organization();
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
    			"SELECT organization.id_org,organization.type,op.title,op.description,colfm.title as colfmTitle FROM AcmeOrganizationBundle:Organization organization
    				JOIN AcmeOrganizationBundle:OrganizationProfile op WITH op.id_org=organization.id_org
    				JOIN AcmeOrganizationBundle:CommonCOLFM colfm WITH op.legal_form=colfm.colfm_id
    					WHERE organization.id_org=:id_org"
    					    )->setParameters(array(
    						'id_org' => $id_org,
						));
        $organization = $query->getResult();
            
	// SELECT GOODS
	$goods = array();
	$Goods = new Goods();
	$em = $this->getDoctrine()->getManager();
	$query = $em->createQuery(
		    'SELECT goods FROM AcmeGoodsBundle:Goods goods WHERE goods.id_org =:id_org AND goods.type = :type'
			)->setParameters(array(
				'id_org' => $id_org,
				'type' => 1,
				));
	$goods = $query->getResult();

	return $this->render('AcmeGoodsBundle:Default:organization_goods_proposal_view.html.twig', array('id' => $userId, 'id_org' =>$id_org, 'organization' => $organization, 'goods' => $goods));

    }
}
