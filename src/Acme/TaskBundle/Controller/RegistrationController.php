<?php
namespace Acme\TaskBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Acme\TaskBundle\Entity\Registration;

use Acme\TaskBundle\Entity\Users;
use Symfony\Component\HttpFoundation\Response;

class RegistrationController extends Controller
{
    public function registrationAction(Request $request)
        {

        // создаём задачу и присваиваем ей некоторые начальные данные для примера
        $registration = new Registration();

	$registration->setSurname('');
	$registration->setName('');
	$registration->setEmail('');
	$registration->setPasswd('');
                                        
        $form = $this->createFormBuilder($registration)
            	    ->add('surname', 'text')
            	    ->add('name', 'text')
            	    ->add('email', 'text')
            	    ->add('passwd', 'password')
                    ->getForm();

	if ($request->getMethod() == 'POST') {
            
            $form->submit($request);

            if ($form->isValid()) {
        	$postSurname = $form->get('surname')->getData();
    		$postName = $form->get('name')->getData();
        	$postEmail = $form->get('email')->getData();
		$postPasswd = $form->get('passwd')->getData();

		// INSERT
		$users = new Users();
		$users->setSurname("$postSurname");
    		$users->setName("$postName");
        	$users->setEmail("$postEmail");
        	$users->setPasswd("$postPasswd");
            
                $em = $this->getDoctrine()->getManager();
                $em->persist($users);
                $em->flush();

		// SELECT
//		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery(
			'SELECT users FROM AcmeTaskBundle:Users users WHERE users.email = :email AND users.passwd = :passwd'
			)->setParameters(array(
			    'email' => $postEmail,
			    'passwd' => $postPasswd,
			    ));
		$users = $query->getResult();
		if (!$users) {throw $this->createNotFoundException('No users found!');}
                
		return $this->render('AcmeTaskBundle:Default:view.html.twig', array('users' => $users));
    	    }
        }
                                                                                                                                        
        return $this->render('AcmeTaskBundle:Default:registration.html.twig', array(
        	    'form' => $form->createView(),
//        	    'users' => $users,
                    ));

        }
}