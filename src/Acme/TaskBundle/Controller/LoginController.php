<?php
namespace Acme\TaskBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Acme\TaskBundle\Entity\Task;

use Acme\TaskBundle\Entity\Users;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Controller
{
    public function loginAction(Request $request)
        {

        // создаём задачу и присваиваем ей некоторые начальные данные для примера
        $task = new Task();

        $task->setEmail('');
        $task->setPasswd('');
                                        
        $form = $this->createFormBuilder($task)
            	    ->add('email', 'text')
            	    ->add('passwd', 'password')
                    ->getForm();

	if ($request->getMethod() == 'POST') {
            
            $form->submit($request);

            if ($form->isValid()) {
                // выполняем прочие действие, например, сохраняем задачу в базе данных
        	// $data = $form->getData();
        	$postEmail = $form->get('email')->getData();
                $postPasswd = $form->get('passwd')->getData();
                
                $message = \Swift_Message::newInstance()
                            ->setSubject('Symfony 2: "Login attepmt..."')
                            ->setFrom('d.learn.service@gmail.com')
                            ->setTo('masserg2007@gmail.com')
                            ->setBody($this->renderView('AcmeTaskBundle:Default:contact.html.twig', array('var' => 'hello')), 'text/html');
                $this->get('mailer')->send($message);
                //$this->get('session')->getFlashBag()->add('blogger-notice','Ваш запрос успешно отправлен. Спасибо!');
                
		// $users = $this->getDoctrine()
		//	->getRepository('AcmeTaskBundle:Users')
		//	->findOneByEmail('masserg2007@gmail.com');
		//	->findAll();

		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery(
			'SELECT tmp_users FROM AcmeTaskBundle:Users tmp_users WHERE tmp_users.email = :email OR tmp_users.passwd = :passwd'
			)->setParameters(array(
			    'email' => $postEmail,
			    'passwd' => $postPasswd,
			    ));
		$users = $query->getResult();
		
		if (!$users) {throw $this->createNotFoundException('No users found!');}
		else { return $this->render('AcmeTaskBundle:Default:view.html.twig', array('users' => $users)); }

                //return $this->redirect($this->generateUrl('task_success'));
                //return $this->redirect($this->generateUrl('task'));
    	    }
        }
                                                                                                                                        
        return $this->render('AcmeTaskBundle:Default:login.html.twig', array(
        	    'form' => $form->createView(),
//        	    'users' => $users,
                    ));

        }
}