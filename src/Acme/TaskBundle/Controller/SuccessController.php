<?php

namespace Acme\TaskBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Acme\TaskBundle\Entity\Task;

class SuccessController extends Controller
{
    public function successAction()
    {
        return $this->render('AcmeTaskBundle:Default:success.html.twig');
    }
}
