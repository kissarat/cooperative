<?php
// src/Acme/TaskBundle/Entity/Task.php
namespace Acme\TaskBundle\Entity;

class Registration
{
    protected $surname;
    protected $name;
    protected $email;
    protected $passwd;

    public function getSurname()
        {
        return $this->surname;
        }
    public function setSurname($surname)
        {
        $this->surname = $surname;
        }
    public function getName()
        {
        return $this->name;
        }
    public function setName($name)
        {
        $this->name = $name;
        }        
    public function getEmail()
        {
        return $this->email;
        }
    public function setEmail($email)
        {
        $this->email = $email;
        }
    public function getPasswd()
        {
        return $this->passwd;
        }
    public function setPasswd($passwd)
        {
        $this->passwd = $passwd;
        }

}